//index page related jQuery script
//@monoj
$(document).ready(function () {
    toastr.options = {
        "closeDuration": 300,
        "closeButton": true,
        "closeMethod": "fadeOut",
        "closeEasing": "swing",
        "progressBar": true
    };

    $('#candidateRegistrationBean').validate({
        rules: {
            classId:{
                required: true
            },
            candidateName: {
                minlength: 6,
                required: true
            },
            /* candidateUsername: {
                 nospace: true,
                 minlength: 6,
                 required: true
             },
             candidatePassword: {
                 required: true,
                 minlength: 6
             },
             candidateCpassword: {
                 required: true,
                 equalTo: "#candidatePassword"
             },*/
            candidateEmail: {
                required: true,
                email: true
            },
            candidatePhonenumber: {
                number: true,
                required: true,
                minlength: 10,
                maxlength: 10
            },
            dob: {
                required: true,
                date:true
            }
        },
        messages: {
            candidate_name: "Please specify your name",
            candidate_email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            }
        },

        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element
                .text('OK!').addClass('valid')
                .closest('.control-group').removeClass('error').addClass('success');
        }
    });
    $('#candidateregistration').click(function (e) {
        e.preventDefault();
        if ($("#moduleId").val() === '') {
            toastr.error('Please Select Your Modules.', 'Error!');
            $("#moduleId").focus();
        }else if ($("#classId").val() === '') {
            toastr.error('Please Select Your Classes.', 'Error!');
            $("#classId").focus();
        }else if ($("#candidateName").val() === '') {
            toastr.error('Please Enter Your Name.', 'Error!');
            $("#candidateName").focus();
        } /*else if ($("#candidateUsername").val() === '') {
            toastr.error('(Which you have to use for login)', 'Please Enter Your User Name.', 'Error!');
            $("#candidateUsername").focus();
        } else if ($("#candidatePassword").val() === '') {
            toastr.error('Please Set Your Password.', 'Error!');
            $("#candidatePassword").focus();
        } else if ($("#candidateCpassword").val() === '') {
            toastr.error('Please Confirm Your Password.', 'Error!');
            $("#candidateCpassword").focus();
        } else if ($("#candidateCpassword").val() != $("#candidatePassword").val()) {
            toastr.error('Your Confirm Password Does Not Match.', 'Error!');
            $("#candidateCpassword").focus();
        } else if ($("#candidateInstitute").val() === '') {
            toastr.error('Please Write Your Institute Name.', 'Error!');
            $("#candidateInstitute").focus();
        }*/ else if ($("#candidateEmail").val() === '') {
            toastr.error('We Need Your Email Id For Furthure Communication.', 'Error!');
            $("#candidateEmail").focus();
        } else if ($("#candidatePhonenumber").val() === '') {
            toastr.error('Please Enter Your Mobile Number.', 'Error!');
            $("#candidatePhonenumber").focus();
        } else if ($("#dob").val() === '') {
            toastr.error('Please Enter Your Date of birth.', 'Error!');
            $("#dob").focus();
        } else {
            var job = {};
            job["moduleId"] = $("#moduleId").val();
            job["classId"] = $("#classId").val();
            job["candidateName"] = $("#candidateName").val();
            // job["candidateUsername"] = $("#candidateUsername").val();
            // job["candidatePassword"] = $("#candidatePassword").val();
            // job["candidateInstitute"] = $("#candidateInstitute").val();
            job["candidateEmail"] = $("#candidateEmail").val();
            job["candidatePhonenumber"] = $("#candidatePhonenumber").val();
            job["gender"] = $("input[name='gender']:checked").val();
            job["dob"] = $("#dob").val();
            console.log(job)
            $.ajax({
                url: "./candidateregistration",
                type: "POST",
                data: JSON.stringify(job),
                dataType: "json",
                contentType: "application/json",
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        toastr.success('Thank you for Your Registration.', 'we will get back to you soon!');
                        // $("#candidateRegistrationBean")[0].reset();
                        window.location = "./"
                        /*$("#classId").val("");
                        $("#candidateName").val("");
                        $("#candidateUsername").val("");
                        $("#candidatePassword").val("");
                        // $("#candidateCpassword").val("");
                        // $("#candidateInstitute").val("");
                        $("#candidateEmail").val("");
                        $("#candidatePhonenumber").val("");
                        $("#gender").val("");
                        $("#dob").val("");*/
                    } else {
                        sweetAlert("Oops...", "Something Worong!", "error");
                    }
                }
            });
        }
    });
    /*$("#candidateUsername").keyup(function () {
        var candidateUsername = $("#candidateUsername").val();
        $.ajax({
            url: "./checkStudentUserNameExistOrNot?candidateUsername=" + candidateUsername,
            type: "POST",
            success: function (data) {
                if (data == "SUCCESS") {
                    $("#err_candidateUsername").show();
                    toastr.warning('Please Change your User Name.Someone Allready Used this!');
                    $("#candidateUsername").focus();
                } else {
                    $("#err_candidateUsername").hide();
                }
            }
        });

    });*/
    $("#candidateEmail").keyup(function () {
        var candidateEmail = $("#candidateEmail").val();
        $.ajax({
            url:"./checkCandidateEmailExistOrNot?candidateEmail=" + candidateEmail,
            type:"POST",
            success: function (data) {
                if (data == "SUCCESS") {
                    $("#err_candidateEmail").show();
                    toastr.warning('Your Email Id allready Exist in our Database!');
                    $("#candidateEmail").focus();
                } else {
                    $("#err_candidateEmail").hide();
                }
            }
        })
    });
    $("#candidatePhonenumber").keyup(function () {
        //toastr.warning('Your Mobile No Allready Exist in our Database!');
    });


    $('#candidateLoginBean').validate({
        rules: {
            candidateLoginId: {
                nospace: true,
                minlength: 5,
                required: true
            },
            candidateLoginPassword: {
                required: true,
                minlength: 5
            }
        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element
                    .text('OK!').addClass('valid')
                    .closest('.control-group').removeClass('error').addClass('success');
        }
    });
    $('#candidatelogin').click(function (e) {
        e.preventDefault();
        if ($("#candidateLoginId").val() === '') {
            toastr.error('Please Enter Your User Name.', 'Error!');
            $("#candidateLoginId").focus();
        } else if ($("#candidateLoginPassword").val() === '') {
            toastr.error('Please Enter Your Password.', 'Error!');
            $("#candidateLoginPassword").focus();
        } else {
            var job = {};
            job["candidateLoginId"] = $("#candidateLoginId").val();
            job["candidateLoginPassword"] = $("#candidateLoginPassword").val();
            $.ajax({
                url: "./candidatelogin",
                type: "POST",
                data: JSON.stringify(job),
                dataType: "json",
                contentType: "application/json",
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        toastr.success('you have successfully logged into the Exam Portal.', 'Sacred Heart');
                        $("#candidateLoginId").val("");
                        $("#candidateLoginPassword").val("");
                        window.location = "./candidate/dashboard";
                    } else {
                        sweetAlert("Oops...", "Worong Credential!", "error");
                    }
                }
            });
        }
    });

    $('#teacherLoginBean').validate({
        rules: {
            teacherLoginId: {
                nospace: true,
                minlength: 6,
                required: true
            },
            teacherLoginPassword: {
                required: true,
                minlength: 6
            }
        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element
                    .text('OK!').addClass('valid')
                    .closest('.control-group').removeClass('error').addClass('success');
        }
    });
    $('#teacherlogin').click(function (e) {
        e.preventDefault();
        if ($("#teacherLoginId").val() === '') {
            toastr.error('Please Enter Your User Name.', 'Error!');
            $("#teacherLoginId").focus();
        } else if ($("#teacherLoginPassword").val() === '') {
            toastr.error('Please Enter Your Password.', 'Error!');
            $("#teacherLoginPassword").focus();
        } else {
            var job = {};
            job["teacherLoginId"] = $("#teacherLoginId").val();
            job["teacherLoginPassword"] = $("#teacherLoginPassword").val();
            $.ajax({
                url: "./teacher/teacherlogin",
                type: "POST",
                data: JSON.stringify(job),
                dataType: "json",
                contentType: "application/json",
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        toastr.success('you have successfully logged into the Exam Portal.', 'Sacred Heart');
                        $("#teacherLoginId").val("");
                        $("#teacherLoginPassword").val("");
                        window.location = "./teacher/dashboard";
                    } else {
                        sweetAlert("Oops...", "Worong Credential!", "error");
                    }
                }
            });
        }
    });

    $('#adminLoginBean').validate({
        rules: {
            adminLoginId: {
                nospace: true,
                minlength: 6,
                required: true
            },
            adminLoginPassword: {
                required: true,
                minlength: 6
            }
        },
        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element
                    .text('OK!').addClass('valid')
                    .closest('.control-group').removeClass('error').addClass('success');
        }
    });
    $('#adminlogin').click(function (e) {
        e.preventDefault();
        if ($("#adminLoginId").val() === '') {
            toastr.error('Please Enter Your User Name.', 'Error!');
            $("#adminLoginId").focus();
        } else if ($("#adminLoginPassword").val() === '') {
            toastr.error('Please Enter Your Password.', 'Error!');
            $("#adminLoginPassword").focus();
        } else {
            var job = {};
            job["adminLoginId"] = $("#adminLoginId").val();
            job["adminLoginPassword"] = $("#adminLoginPassword").val();
            $.ajax({
                url: "./admin/adminlogin",
                type: "POST",
                data: JSON.stringify(job),
                dataType: "json",
                contentType: "application/json",
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        toastr.success('you have successfully logged into the Exam Portal.', 'Sacred Heart');
                        $("#adminLoginId").val("");
                        $("#adminLoginPassword").val("");
                        window.location = "./admin/dashboard";
                    } else {
                        sweetAlert("Oops...", "Worong Credential!", "error");
                    }
                }
            });
        }
    });
});