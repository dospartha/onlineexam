//index page related jQuery script
//@monoj



$(document).ready(function () {
    toastr.options = {
        "closeDuration": 300,
        "closeButton": true,
        "closeMethod": "fadeOut",
        "closeEasing": "swing",
        "progressBar": true
    };
    $('#candidateRegistrationBean').validate({
        rules: {
            candidateName: {
                minlength: 6,
                required: true

            },
            candidateUsername: {
                nospace: true,
                minlength: 6,
                required: true
            },
            candidatePassword: {
                required: true,
                minlength: 6
            },
            candidateCpassword: {
                required: true,
                equalTo: "#candidatePassword"
            },
            candidateInstitute: {
                required: true
            },
            candidateEmail: {
                required: true,
                email: true
            },
            candidatePhoneNumber: {
                number: true,
                required: true,
                minlength: 10,
                maxlength: 10
            }
        }, //rules close here
        messages: {
            candidate_name: "Please specify your name",
            candidate_email: {
                required: "We need your email address to contact you",
                email: "Your email address must be in the format of name@domain.com"
            }
        },

        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element
                    .text('OK!').addClass('valid')
                    .closest('.control-group').removeClass('error').addClass('success');
        }
    });
    $('#candidateregistration').click(function (e) {
        e.preventDefault();
        if ($("#candidateName").val() === '') {
            toastr.error('Please Enter Your Name.', 'Error!');
            $("#candidateName").focus();
        } else if ($("#candidateUsername").val() === '') {
            toastr.error('(Which you have to use for login)', 'Please Enter Your User Name.', 'Error!');
            $("#candidateUsername").focus();
        } else if ($("#candidatePassword").val() === '') {
            toastr.error('Please Set Your Password.', 'Error!');
            $("#candidatePassword").focus();
        } else if ($("#candidateCpassword").val() === '') {
            toastr.error('Please Confirm Your Password.', 'Error!');
            $("#candidateCpassword").focus();
        } else if ($("#candidateCpassword").val() != $("#candidatePassword").val()) {
            toastr.error('Your Confirm Password Does Not Match.', 'Error!');
            $("#candidateCpassword").focus();
        } else if ($("#candidateInstitute").val() === '') {
            toastr.error('Please Write Your Institute Name.', 'Error!');
            $("#candidateInstitute").focus();
        } else if ($("#candidateEmail").val() === '') {
            toastr.error('We Need Your Email Id For Furthure Communication.', 'Error!');
            $("#candidateEmail").focus();
        } else if ($("#candidatePhoneNumber").val() === '') {
            toastr.error('Please Enter Your Mobile Number.', 'Error!');
            $("#candidatePhoneNumber").focus();
        } else {
            var l = $(this).ladda();
            l.ladda('start');
            var job = {};
            job["candidateName"] = $("#candidateName").val();
            job["candidateUsername"] = $("#candidateUsername").val();
            job["candidatePassword"] = $("#candidatePassword").val();
            job["candidateInstitute"] = $("#candidateInstitute").val();
            job["candidateEmail"] = $("#candidateEmail").val();
            job["candidatePhoneNumber"] = $("#candidatePhoneNumber").val();
            $.ajax({
                url: "./candidateregistration",
                type: "POST",
                data: JSON.stringify(job),
                dataType: "json",
                contentType: "application/json",
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        toastr.success('Thank you for Your Registration.', 'we will get back to you soon!');
                        $("#candidateName").val("");
                        $("#candidateUsername").val("");
                        $("#candidatePassword").val("");
                        $("#candidateCpassword").val("");
                        $("#candidateInstitute").val("");
                        $("#candidateEmail").val("");
                        $("#candidatePhoneNumber").val("");
                    } else {
                        sweetAlert("Oops...", "Something Worong!", "error");
                    }
                }
            });
        }
        setTimeout(function () {
            l.ladda('stop');
        }, 2000);
    });
    $("#candidateUsername").keyup(function () {
        var candidateUsername = $("#candidateUsername").val();
        $.ajax({
            url: "./checkStudentUserNameExistOrNot?candidateUsername=" + candidateUsername,
            type: "POST",
            success: function (data) {
                if (data == "SUCCESS") {
                    $("#err_candidateUsername").show();
                    toastr.warning('Please Change your User Name.Someone Allready Used this!');
                    $("#candidateUsername").focus();
                } else {
                    $("#err_candidateUsername").hide();
                }
            }
        });

    });
    $("#candidateEmail").keyup(function () {
        //toastr.warning('Your Email Id Allready Exist in our Database!');
    });
    $("#candidatePhoneNumber").keyup(function () {
        //toastr.warning('Your Mobile No Allready Exist in our Database!');
    });

  

    $('#candidateLoginBean').validate({
        rules: {
            candidateLoginId: {
                nospace: true,
                minlength: 6,
                required: true
            },
            candidateLoginPassword: {
                required: true,
                minlength: 6
            }
        }, //rules close here

        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element
                    .text('OK!').addClass('valid')
                    .closest('.control-group').removeClass('error').addClass('success');
        }
    });
    $('#candidatelogin').click(function (e) {
        e.preventDefault();
        if ($("#candidateLoginId").val() === '') {
            toastr.error('Please Enter Your User Name.', 'Error!');
            $("#candidateLoginId").focus();
        } else if ($("#candidateLoginPassword").val() === '') {
            toastr.error('Please Enter Your Password.', 'Error!');
            $("#candidateLoginPassword").focus();
        } else {
            var l = $(this).ladda();
            l.ladda('start');
            var job = {};
            job["candidateLoginId"] = $("#candidateLoginId").val();
            job["candidateLoginPassword"] = $("#candidateLoginPassword").val();
            $.ajax({
                url: "./candidatelogin",
                type: "POST",
                data: JSON.stringify(job),
                dataType: "json",
                contentType: "application/json",
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        toastr.success('you have successfully logged into the Exam Portal.', 'Sacred Heart');
                        $("#candidateLoginId").val("");
                        $("#candidateLoginPassword").val("");
                        window.location = "./candidate/dashboard";
                    } else {
                        sweetAlert("Oops...", "Something Worong!", "error");
                    }
                }
            });
        }
        setTimeout(function () {
            l.ladda('stop');
        }, 2000);
    });


}); // end document.ready