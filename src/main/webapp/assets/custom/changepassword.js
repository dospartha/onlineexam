//changepassword page related jQuery script
//@monoj



$(document).ready(function () {
    toastr.options = {
        "closeDuration": 300,
        "closeButton": true,
        "closeMethod": "fadeOut",
        "closeEasing": "swing",
        "positionClass": "toast-top-center",
        "progressBar": true
    };
    $('#candidateChangePasswordBean').validate({
        rules: {
            candidateOldPassword: {
                required: true,
                minlength: 6
            },
            candidateNewPassword: {
                required: true,
                minlength: 6
            },
            candidateNewConfirmPassword: {
                required: true,
                minlength: 6,
                equalTo: "#candidateNewPassword"
            }
        },

        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element
                    .text('OK!').addClass('valid')
                    .closest('.control-group').removeClass('error').addClass('success');
        }
    });
    $('#candidatepasschange').click(function (e) {
        e.preventDefault();
        if ($("#candidateOldPassword").val() === '') {
            toastr.error('Please Enter Your Current Password.', 'Error!');
            $("#candidateOldPassword").focus();
        } else if ($("#candidateNewPassword").val() === '') {
            toastr.error('Please Enter Your New Password.', 'Error!');
            $("#candidateNewPassword").focus();
        } else if ($("#candidateNewConfirmPassword").val() === '') {
            toastr.error('Please Re Type Your New Password.', 'Error!');
            $("#candidateNewConfirmPassword").focus();
        } else if ($("#candidateNewConfirmPassword").val() != $("#candidateNewPassword").val()) {
            toastr.error('Your New Password Does Not Match.', 'Error!');
            $("#candidateNewConfirmPassword").focus();
        } else {
            var job = {};
            job["candidateOldPassword"] = $("#candidateOldPassword").val();
            job["candidateNewPassword"] = $("#candidateNewPassword").val();
            job["candidateNewConfirmPassword"] = $("#candidateNewConfirmPassword").val();
            job["id"] = $("#candidateUserId").val();
            $.ajax({
                url: "./candidatepasschange",
                type: "POST",
                data: JSON.stringify(job),
                dataType: "json",
                contentType: "application/json",
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        toastr.success('you have successfully change your password.', 'Sacred Heart');
                        $("#candidateOldPassword").val("");
                        $("#candidateNewPassword").val("");
                        $("#candidateNewConfirmPassword").val("");
                    } else {
                        sweetAlert("Oops...", data.responseText, "error");
                    }
                }
            });
        }
    });
    $('#teacherChangePasswordBean').validate({
        rules: {
            teacherOldPassword: {
                required: true,
                minlength: 6
            },
            teacherNewPassword: {
                required: true,
                minlength: 6
            },
            teacherNewConfirmPassword: {
                required: true,
                minlength: 6,
                equalTo: "#teacherNewPassword"
            }
        },

        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element
                    .text('OK!').addClass('valid')
                    .closest('.control-group').removeClass('error').addClass('success');
        }
    });

    $('#teacherpasschange').click(function (e) {
        e.preventDefault();
        if ($("#teacherOldPassword").val() === '') {
            toastr.error('Please Enter Your Current Password.', 'Error!');
            $("#teacherOldPassword").focus();
        } else if ($("#teacherNewPassword").val() === '') {
            toastr.error('Please Enter Your New Password.', 'Error!');
            $("#teacherNewPassword").focus();
        } else if ($("#teacherNewConfirmPassword").val() === '') {
            toastr.error('Please Re Type Your New Password.', 'Error!');
            $("#teacherNewConfirmPassword").focus();
        } else if ($("#teacherNewConfirmPassword").val() != $("#teacherNewPassword").val()) {
            toastr.error('Your New Password Does Not Match.', 'Error!');
            $("#teacherNewConfirmPassword").focus();
        } else {
            var job = {};
            job["teacherOldPassword"] = $("#teacherOldPassword").val();
            job["teacherNewPassword"] = $("#teacherNewPassword").val();
            job["teacherNewConfirmPassword"] = $("#teacherNewConfirmPassword").val();
            job["id"] = $("#teacherUserId").val();
            $.ajax({
                url: "./teacherpasschange",
                type: "POST",
                data: JSON.stringify(job),
                dataType: "json",
                contentType: "application/json",
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        toastr.success('you have successfully change your password.', 'Sacred Heart');
                        $("#teacherOldPassword").val("");
                        $("#teacherNewPassword").val("");
                        $("#teacherNewConfirmPassword").val("");
                    } else {
                        sweetAlert("Oops...", data.responseText, "error");
                    }
                }
            });
        }
    });


}); // end document.ready