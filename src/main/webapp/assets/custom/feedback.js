//feedback page related jQuery script
//@monoj
$(document).ready(function () {
    toastr.options = {
        "closeDuration": 300,
        "closeButton": true,
        "closeMethod": "fadeOut",
        "closeEasing": "swing",
        "positionClass": "toast-top-center",
        "progressBar": true
    };


    $('#feedbackBean').validate({
        rules: {
            fbName: {
                required: true,
                minlength: 6
            },
            fbEmail: {
                required: true,
                email: true
            },
            fbNumber: {
                number: true,
                required: true,
                minlength: 10,
                maxlength: 10
            },
            fbComment: {
                required: true,
                minlength: 20
            }
        },

        highlight: function (element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');
        },
        success: function (element) {
            element
                    .text('OK!').addClass('valid')
                    .closest('.control-group').removeClass('error').addClass('success');
        }
    });

    $('#feedbackSend').click(function (e) {
        e.preventDefault();
        if ($("#fbName").val() === '') {
            toastr.error('Please Enter Your Name.', 'Error!');
            $("#fbName").focus();
        } else if ($("#fbEmail").val() === '') {
            toastr.error('Please Enter Your Email Id.', 'Error!');
            $("#fbEmail").focus();
        } else if ($("#fbNumber").val() === '') {
            toastr.error('Please Enter Your Mobile Number.', 'Error!');
            $("#fbNumber").focus();
        } else if ($("#fbNumber").val().length !== 10) {
            toastr.error('Please Enter Proper Mobile Number.', 'Error!');
            $("#fbNumber").focus();
        } else if ($("#fbComment").val().length < 1) {
            toastr.error('Put Your Comment Please', 'Error!');
            $("#fbComment").focus();
        } else if ($("#fbComment").val().length > 1 && $("#fbComment").val().length < 10) {
            toastr.error('Your Comment is too short', 'Error!');
            $("#fbComment").focus();
        } else {
            var job = {};
            job["fbName"] = $("#fbName").val();
            job["fbEmail"] = $("#fbEmail").val();
            job["fbNumber"] = $("#fbNumber").val();
            job["fbComment"] = $("#fbComment").val();
            $.ajax({
                url: "./feedbackSend",
                type: "POST",
                data: JSON.stringify(job),
                dataType: "json",
                contentType: "application/json",
                complete: function (data) {
                    if (data.responseText === "SUCCESS") {
                        toastr.success('Thanky You for your feedback.', 'Sacred Heart');
                        $("#fbName").val("");
                        $("#fbEmail").val("");
                        $("#fbNumber").val("");
                        $("#fbComment").val("");
                        $("#myModal").modal('hide');
                    } else {
                        sweetAlert("Oops...", data.responseText, "error");
                    }
                }
            });
        }
    });


}); // end document.ready