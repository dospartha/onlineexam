<div class="span2">
    <ul class="nav nav-tabs nav-stacked nav-justified"  style='background-color:white;'>

        <li>
            <a href="./dashboard" >Home</a>
        </li>
        <li>
            <a href="./profile">Profile</a>
        </li>
        <li>
            <a href="./studentRegistration">Add Student</a>
        </li>
        <li>
            <a href="./addquestions">Add Questions</a>
        </li>
        <li>
            <a href="./makepaper">Make Paper</a>
        </li>
        <li>
            <a href="./updatepaper">Update Paper</a>
        </li>
        <li>
            <a href="./viewpaper">View Paper</a>
        </li>
        <li>
            <a href="./viewresult">View Result</a>
        </li>
        <li>
            <a href="./appendpaper">Append Paper</a>
        </li>
        <li>
            <a href="./deletepaper">Delete Paper</a>
        </li>
        <li>
            <a href="./notice">Notice</a>
        </li>
        <li>
            <a href="./changepassword">Change Password</a>
        </li>
    </ul>
</div>