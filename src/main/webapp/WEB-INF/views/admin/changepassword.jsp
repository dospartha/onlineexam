<%
    String adminUserName = (String) session.getAttribute("adminUserName");
    if (adminUserName == null) {
        response.sendRedirect("/admin");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">

        <%@include file="side_menu.jsp" %>
        <div id="maincontent" class="span5 pull-right" >
            <div id="myTabContent" class="tab-content">
                <div id="ChangPassword" class="tab-pane fade in active">
                    <form id="teacherChangePasswordBean" class="form-horizontal" method="post">
                        <div class="control-group">
                            <label class="control-label" for="teacherOldPassword">Current Password</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-book"></i></span>
                                    <input type="password" class="input-large" name="teacherOldPassword" id="teacherOldPassword" required placeholder="Current Password"/>
                                    <span id="err"> </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="teacherNewPassword">New Password</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-barcode"></i></span>
                                    <input type="password" class="input-large" name="teacherNewPassword" id="teacherNewPassword" required placeholder="New Password"/>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="teacherNewConfirmPassword">Re Password</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-barcode"></i></span>
                                    <input type="password" class="input-large" name="teacherNewConfirmPassword" id="teacherNewConfirmPassword" required placeholder="New Password"/>
                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"></label>
                            <div class="controls">
                                <c:if test='${not empty param["Success"]}'>
                                    <p style="color:green;font-weight:bold;">Your Password is changed successfully.</p>
                                </c:if>

                                <c:if test='${not empty param["Failed"]}'>
                                    <p style="color:red;font-weight:bold;">Wrong current Password.</p>

                                </c:if>
                                <button type="submit" class="btn btn-success"  id="teacherpasschange">Submit</button>
                            </div>
                        </div>


                    </form>

                </div>
                <br/>
            </div>

        </div>
    </div>
    <br/><br/>
</div>
<br/><br/><br/><br/>

<%@include file="footer.jsp" %>
</body>
</html>