<%--
    Document   : html
    Created on : Sep 21, 2018, 6:16:47 PM
    Author     : momoj
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="description" content="On-line Exam System is very useful for Educational Institute to prepare an exam, save the time that will take to check the paper and prepare mark sheets. It will help the Institute to testing of students and develop their skills. But the disadvantages for this system, it takes a lot of times when you prepare the exam at the first time for usage. And we are needs number of computers with the same number of students."/>
        <meta name="keywords" content="sacredheart,sacred,career,academy,dosinfotech,dos,shillong,mcqquestions,online,exam,student,teacher,onlineexam" />
        <meta name="author" content="Monoj Majumder" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Admin Profile</title>

        <script type="text/javascript" src="../assets/js/jquery.js"></script>
        <script type="text/javascript" src="../assets/js/jquery-1.7.1.min.js"></script>

        <link rel="shortcut icon" type="image/x-icon" href="../assets/img/favicon.ico"/>
        <link href="../assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/bootstrap_1.css" rel="stylesheet" type="text/css" />
        <link href="../assets/css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />
        <link href="../assets/custom/style.css" rel="stylesheet" type="text/css" />
        <link href="../assets/js/ladda/dist/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/js/toastr/toastr.min.css" rel="stylesheet" type="text/css" />
        <link  href="../assets/css/sweet-alert.min.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            .navbar-inner{
                background:#000;
                border-bottom:5px solid #007AF4;
                height:70px;

            }
            .navbar-inner .brand{color:#FFF}

        </style>
    </head>
    <body>
        <script type="text/javascript" src="../assets/js/bootstrap-button.js"></script>
        <script type="text/javascript" src="../assets/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="../assets/js/bootstrap-tab.js"></script>
        <script type="text/javascript" src="../assets/js/modal.js"></script>
        <script type="text/javascript" src="../assets/custom/candidatedashboard.js"></script>
        <script src="../assets/js/jquery_1.js" type="text/javascript"></script>
        <script src="../assets/js/application_1.js" type="text/javascript"></script>
        <script type="text/javascript" src="../assets/js/bootstrap-button.js"></script>
        <script type="text/javascript" src="../assets/js/toastr/toastr.min.js"></script>
        <script type="text/javascript" src="../assets/js/sweet-alert.min.js"></script>
        <script type="text/javascript" src="../assets/custom/feedback.js"></script>
        <script type="text/javascript" src="../assets/js/ladda/dist/ladda.jquery.min.js"></script>
        <script type="text/javascript" src="../assets/js/ladda/dist/ladda.min.js"></script>
        <script type="text/javascript" src="../assets/js/ladda/dist/spin.min.js"></script>
        <script type="text/javascript" src="../assets/custom/changepassword.js"></script>
        <script type="text/javascript" src="../assets/custom/notice.js"></script>
        <script type="text/javascript" src="../assets/custom/profile.js"></script>
        <script type="text/javascript" src="../assets/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "mceEditor",
                height: "100",
                width: "100",
                menubar: false

            });
        </script>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">

                    <a href="./dashboard" class="brand"> <img src="../assets/img/examshow.png" alt="Online Exam" width="100" height="70"/></a>
                    <br/>
                    <h1 class="brand" style="font-weight:bold;">SacredHeart Career Academy Exam Portal</h1>
                    <h3 class="brand" style="font-weight:bold;"><a href="./dashboard">Home</a></h3>
                    <h3 class="brand" style="font-weight:bold;" align="center"><a href="./upcomingevents">Events<img src="../assets/img/new.gif" border="0" style="margin-top: -10px"/></a></h3>
                    <a href="./logout" class="pull-right">
                        <button class="btn btn-primary" > logout </button>
                    </a>
                    <p class="pull-right" style="color:white;">
                        <br />
                        <%
                            out.println("<b> Welcome , <a href='./dashboard' style='color:white'>" + adminUserName + "</a></b>");
                        %>

                        &nbsp;&nbsp;

                    </p>

                </div>
            </div>
        </div>