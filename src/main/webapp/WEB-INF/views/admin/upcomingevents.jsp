<%
    String adminUserName = (String) session.getAttribute("adminUserName");
    if (adminUserName == null) {
        response.sendRedirect("/admin");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container">
    <div class="row">
        <table id="sortTableExample" class='table zebra-striped'>
            <thead>
                <tr>
                    <th class="header">S No.</th>
                    <th class="red header">Quiz Code</th>
                    <th class="blue header">Start Date</th>
                    <th class="green header headerSortUp">End Date</th>
                    <th class="yellow header">Register</th>
                </tr>
            </thead>
            <tbody>

                <tr>
                    <td>
                        1
                    </td>
                    <td>
                        <a href='quiz.jsp?scode=1'>Math001</a>
                    </td>
                    <td>
                        04/09/2018
                    </td>
                    <td>
                        04/10/2018
                    </td>
                    <td>

                        <form action="reg.jsp" method="post">
                            <input type="hidden" name="scode" value="001" />
                            <button type='submit' class='btn btn-danger'>Register</button>
                        </form>

                        <!--                                <p style="color:green;font-weight:bold;">Registered</p>-->

                    </td>
                </tr>

                <tr>
                    <td>
                        2
                    </td>
                    <td>
                        <a href='quiz.jsp?scode=1'>Math002</a>
                    </td>
                    <td>
                        04/09/2018
                    </td>
                    <td>
                        04/10/2018
                    </td>
                    <td>

                        <form action="reg.jsp" method="post">
                            <input type="hidden" name="scode" value="001" />
                            <button type='submit' class='btn btn-danger'>Register</button>
                        </form>

                        <!--                                <p style="color:green;font-weight:bold;">Registered</p>-->

                    </td>
                </tr>

            </tbody></table>



    </div>
    <br/><br/>
</div>
<br/><br/>
<%@include file="footer.jsp" %>
</body>
</html>