<%
    String adminUserName = (String) session.getAttribute("adminUserName");
    if (adminUserName == null) {
        response.sendRedirect("/admin");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">

        <%@include file="side_menu.jsp" %>
        <form id="myForm" <%--action="./studentRegistration" name="studentsDetailBean" --%>class="form-horizontal" method="post">
            <div id="maincontent" class="span10 pull-right">
                <h1 style='color:#3399FF;align-content: center'> Student Registration : </h1>
                <table class='table table-hover' style='background-color:white;color:#808080;width: 100%'>
                    <tr style="font-weight:bold;">
                        <td><strong>Class : </strong></td>
                        <td>
                            <select name="classId" id="classId">
                                <option value="">Select</option>
                                <c:forEach var="Class" items="${classes}">
                                    <option value="${Class.id}">${Class.name}</option>
                                </c:forEach>
                            </select>
                        </td>
                        <td><strong>Interested for which Exam? : </strong></td>
                        <td>
                            <select name="moduleId" id="moduleId">
                                <option value="">Select</option>
                                <c:forEach var="Modules" items="${modules}">
                                    <option value="${Modules.id}">${Modules.name}</option>
                                </c:forEach>
                            </select>
                        </td>

                    </tr>
                    <tr style="font-weight:bold;">
                        <td>
                            <h2 style='color:#3399FF;align-content: center'> Student Information : </h2>
                            <table class='table table-hover table-bordered'
                                   style='background-color:white;color:#808080;width: 100%'>
                                <tr style="font-weight:bold;">
                                    <td><strong>Name : </strong></td>
                                </tr>
                                <tr style="font-weight:bold;">
                                    <td><input type="text" class="form-control" name="candidateName" id="candidateName" placeholder="Candidate Name"></td>
                                </tr>
                                <tr style="font-weight:bold;">
                                    <td><strong>Date of Birth : </strong></td>
                                </tr>
                                <tr style="font-weight:bold;">
                                    <td><input type="date" class="form-control" id="dob" name="dob" value="2000-01-08"></td>
                                </tr>
                                <tr style="font-weight:bold;">
                                    <td><strong>Phone No : </strong></td>
                                </tr>
                                <tr style="font-weight:bold;">
                                    <td><input type="text" class="form-control" name="candidatePhoneNumber" id="candidatePhoneNumber"
                                               maxlength="10" placeholder="Mobile number"></td>
                                </tr>
                                <tr style="font-weight:bold;">
                                    <td><strong>Email Id : </strong></td>
                                </tr>
                                <tr style="font-weight:bold;">
                                    <td><input type="text" class="form-control" name="candidateEmail" id="candidateEmail"
                                               placeholder="Email address"></td>
                                </tr>
                                <tr style="font-weight:bold;">
                                    <td><strong>Gender : </strong></td>
                                </tr>
                                <tr style="font-weight:bold;">
                                    <td>
                                        <table class="table-bordered">
                                            <tr>
                                                <td><input type="radio" value="male" name="gender" checked style="margin-left: 2px;"></td>
                                                <td><strong>Male : </strong></td>
                                                <td><input type="radio" value="female" name="gender" style="margin-left: 2px;"></td>
                                                <td><strong>Female : </strong></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td colspan="3">
                            <h2 style='color:#3399FF;align-content: center'> Payment Information : </h2>
                            <table class='table table-hover table-bordered'
                                   style='background-color:white;color:#808080;width: 100%'>
                                <tr style="font-weight:bold;">
                                    <td><strong>Registration Fee : </strong></td>
                                    <td><strong>Payment Date : </strong></td>
                                </tr>
                                <tr style="font-weight:bold;">
                                    <td><input type="text" class="form-control" id="registrationFee" name="registrationFee"></td>
                                    <td><input type="date" class="form-control" id="paymentDate" name="paymentDate"></td>
                                </tr>

                                <tr style="font-weight:bold;">
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr style="font-weight:bold;">
                                    <td>
                                        <table class="table-bordered">
                                            <tr style="font-weight:bold;">
                                                <td>
                                                    <table class="table-bordered">
                                                        <tr>
                                                            <td><input type="radio" value="0" class="paymentMode" name="paymentModeName" id="paymentModeCheque"
                                                                       style="margin-left: 2px;"></td>
                                                            <td><strong>Cheque : </strong></td>
                                                            <td><input type="radio" value="1" class="paymentMode" name="paymentModeName" id="paymentModeCash" checked
                                                                       style="margin-left: 2px;"></td>
                                                            <td><strong>Cash : </strong></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr style="font-weight:bold;">
                                                <td>
                                                    <table class="table-bordered" id="checkDetails" style="display: none">
                                                        <tr>
                                                            <td><strong>Cheque No : </strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" class="form-control" id="chequeNo" name="chequeNo"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Bank Name : </strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" class="form-control"
                                                                       id="bankName" name="bankName"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Branch Name : </strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="text" class="form-control"
                                                                       id="bankBranch" name="bankBranch"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Date of Issue : </strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="date" class="form-control"
                                                                       id="chequeIssueDate" name="chequeIssueDate"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>
                                        <table class="table-bordered">
                                            <tr>
                                                <td><strong>Amount : </strong></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" class="form-control" id="netAmount" name="netAmount"></td>
                                            </tr>
                                            <tr>
                                                <td><strong>GST(%) : </strong></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" class="form-control" id="gst" name="gst"></td>
                                            </tr>
                                            <tr>
                                                <td><strong>Payble Amount : </strong></td>
                                            </tr>
                                            <tr>
                                                <td><input type="text" class="form-control" id="grossAmount" name="grossAmount"></td>
                                            </tr>
                                            <tr>
                                                <td><button type="submit" class="btn btn-success" id="studentRegistration">Submit</button></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br/>
            </div>
        </form>
    </div>
    <br/><br/>
</div>
<br/><br/><br/><br/>

<script>

    $(".paymentMode").on("click", function () {
        if ($(this).val() == 0) {
            $("#checkDetails").show();
            $("#paymentModeCash").prop("checked", false);
        } else {
            $("#checkDetails").hide();
            $("#paymentModeCheque").prop("checked", false);

        }
    });

    $("#studentRegistration").on("click",function (e) {
        e.preventDefault();
        var formJSONData = {};
        var arrayData = $("#myForm").serializeArray();
//        console.log(arrayData)
        $.each(arrayData, function() {
            if (formJSONData[this.name]) {
                if (!formJSONData[this.name].push) {
                    formJSONData[this.name] = [formJSONData[this.name]];
                }
                formJSONData[this.name].push(this.value || '');
            } else {
                formJSONData[this.name] = this.value || '';
            }
        });
        console.log(formJSONData)
        $.ajax({
            url: "./studentRegistration",
            type: "POST",
            data:JSON.stringify(formJSONData),
            dataType: "json",
            contentType: "application/json",
            complete: function (data) {
                console.log(data.responseText)
                if (data.responseText === "SUCCESS") {
                    toastr.success('you have successfully added one candidate');
                    $("#myForm").trigger('reset');
//                    window.location = "./dashboard";
                } else {
                    sweetAlert("Oops...", "Worong Credential!", "error");
                }
            }
        });
    });
</script>

<%@include file="footer.jsp" %>
</body>
</html>