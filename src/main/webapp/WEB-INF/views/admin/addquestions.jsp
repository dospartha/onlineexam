<%
    String adminUserName = (String) session.getAttribute("adminUserName");
    if (adminUserName == null) {
        response.sendRedirect("/admin");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">

        <%@include file="side_menu.jsp" %>
        <div id="maincontent" class="span5 pull-right">
            <div id="myTabContent" class="tab-content">
                <div id="addquestions" class="tab-pane active">
                    <form id="teacherAddQuestionBean" class="form-horizontal" method="post">
                        <div class="control-group">
                            <label class="control-label" for="classId">Module</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-dropbox"></i></span>
                                    <select class="form-control" name="moduleId" id="moduleId">
                                        <option value="">Select</option>
                                        <c:forEach var="Module" items="${modules}">
                                            <option value="${Module.id}">${Module.name}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="classId">Class</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-dropbox"></i></span>
                                    <select class="form-control" name="classId" id="classId">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="classId">Subject</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-dropbox"></i></span>
                                    <select class="form-control" name="subjectId" id="subjectId">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="chapterId">Chapter</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-dropbox"></i></span>
                                    <select class="form-control" name="chapterId" id="chapterId">
                                        <option value="">Select</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="questionNo">Question Number:</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <input type="text" class="span5" name="questionNo" id="questionNo"
                                           required/>
                                    <span id="err"> </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="question">Question:</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <textarea <%--class="mceEditor" --%>name="question" id="question" rows="5" cols="5"
                                              placeholder="Question" style="width: 450px"></textarea>
                                </div>
                            </div>
                        </div>
                            <div class="control-group">
                                <label class="control-label" for="option1">Option 1:</label>
                                <div class="controls">
                                    <div class="input-prepend">
                                        <input type="text" class="span4" name="option1" id="option1"
                                               placeholder="Option 1" required/>
                                        <input type="radio" class="span1" <%--value="option1"--%> name="answer">
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="option2">Option 2:</label>
                                <div class="controls">
                                    <div class="input-prepend">
                                        <input type="text" class="span4" name="option2" id="option2"
                                               placeholder="Option 2" required/>
                                        <input type="radio" class="span1" <%--value="option2"--%> name="answer">
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="option3">Option 3:</label>
                                <div class="controls">
                                    <div class="input-prepend">
                                        <input type="text" class="span4" name="option3" id="option3"
                                               placeholder="Option 3" required/>
                                        <input type="radio" class="span1" <%--value="option3"--%> name="answer">
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="option4">Option 4:</label>
                                <div class="controls">
                                    <div class="input-prepend">
                                        <input type="text" class="span4" name="option4" id="option4"
                                               placeholder="Option 4"/>
                                        <input type="radio" class="span1" <%--value="option4"--%> name="answer">
                                    </div>
                                </div>
                            </div>
                        <div class="control-group">
                            <label class="control-label"></label>
                            <div class="controls">
                                <c:if test='${not empty param["Success"]}'>
                                    <p style="color:green;font-weight:bold;">Add Question successfully.</p>
                                </c:if>

                                <c:if test='${not empty param["Failed"]}'>
                                    <p style="color:red;font-weight:bold;">Something went wrong Try again.</p>

                                </c:if>
                                <button type="submit" class="btn btn-success" id="addQuestion">Add Question</button>
                            </div>
                        </div>
                    </form>
                </div>
                <br/>
            </div>
        </div>
    </div>
    <br/><br/>
</div>

<script>
    $('#moduleId').change(function () {
        var moduleId = this.value;
        if (moduleId != '') {
            $('#classId').html("<option value=\"\">Select <\/option>");
            $('#subjectId').html("<option value=\"\">Select <\/option>");
            $('#chapterId').html("<option value=\"\">Select <\/option>");
            $.ajax({
                url: "./getAllClassesByModule?moduleId=" + moduleId/*$('#moduleId:selected').val()*/,
                type: "GET",
                async: false,
                success: function (data) {
                    var strVar = "";
                    for (var i = 0; i < data.length; i++) {
                        strVar += " <option value=\"\">Select <\/option>";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"" + data[i].classes.id + "\" >"
                                + data[i].classes.name
                                + "<\/option>";
                        }
                    }
                    $('#classId').html(strVar);

                }
            });
        }else {
            $('#classId').html("<option value=\"\">Select <\/option>");
            $('#subjectId').html("<option value=\"\">Select <\/option>");
            $('#chapterId').html("<option value=\"\">Select <\/option>");
        }
    });

    $('#classId').change(function () {
        var classId = this.value;
        if (classId != '') {
            $('#subjectId').html("<option value=\"\">Select <\/option>");
            $('#chapterId').html("<option value=\"\">Select <\/option>");
            $.ajax({
                url: "./getAllSubjectsByClassId?classId=" + classId,
                type: "GET",
                async: false,
                success: function (data) {
                    var strVar = "";
                    for (var i = 0; i < data.length; i++) {
                        strVar += " <option value=\"\">Select <\/option>";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"" + data[i].subject.id + "\" >"
                                + data[i].subject.name
                                + "<\/option>";
                        }
                    }
                    $('#subjectId').html(strVar);

                }
            });
        }else {
            $('#subjectId').html("<option value=\"\">Select <\/option>");
            $('#chapterId').html("<option value=\"\">Select <\/option>");
        }
    });
    $('#subjectId').change(function () {
        var subjectId = this.value;
        if (subjectId != '') {
            $('#chapterId').html("<option value=\"\">Select <\/option>");
            $.ajax({
                url: "./getAllChaptersBySubjectId?subjectId=" + subjectId,
                type: "GET",
                async: false,
                success: function (data) {
                    var strVar = "";
                    for (var i = 0; i < data.length; i++) {
                        strVar += " <option value=\"\">Select <\/option>";
                        for (var i = 0; i < data.length; i++) {
                            strVar += " <option value=\"" + data[i].chapters.id + "\" >"
                                + data[i].chapters.name
                                + "<\/option>";
                        }
                    }
                    $('#chapterId').html(strVar);
                }
            });
        }else {
            $('#chapterId').html("<option value=\"\">Select <\/option>");
        }
    });

    $('#chapterId').change(function () {
        var values = {};
        values["moduleId"] = $("#moduleId").val();
        values["classId"] = $("#classId").val();
        values["subjectId"] = $("#subjectId").val();
        values["chapterId"] = $("#chapterId").val();
        $.ajax({
            url: "./getQuestionNumber",
            type: "POST",
            data: JSON.stringify(values),
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
                console.log(data)
                $("#questionNo").val(data + 1);
                $('#questionNo').prop("disabled",true);

            }
        });
    });

    $('#addQuestion').click(function (e) {
        e.preventDefault();
        var formValue = {};
        formValue["moduleId"] = $("#moduleId").val();
        formValue["classId"] = $("#classId").val();
        formValue["subjectId"] = $("#subjectId").val();
        formValue["chapterId"] = $("#chapterId").val();
        formValue["questionNo"] = $("#questionNo").val();
        formValue["question"] = $("#question").val();
        formValue["option1"] = $("#option1").val();
        formValue["option2"] = $("#option2").val();
        formValue["option3"] = $("#option3").val();
        formValue["option4"] = $("#option4").val();
        if ($("input[name='answer']:checked").each(function () {
                formValue["answer"] = ($(this).closest("div.input-prepend").find("input[type=text]").val());
            })) ;
        console.log(formValue)
        $.ajax({
            url: "./addQuestion",
            type: "POST",
            data: JSON.stringify(formValue),
            dataType: "json",
            contentType: "application/json",
            complete: function (data) {
                var question = JSON.parse(data.responseText);
                if (question.id != null) {
                    $("#questionNo").val(question.questionNo + 1);
                    $('#questionNo').prop("disabled",true);
                    $("#question").val("");
                    $("#option1").val("");
                    $("#option2").val("");
                    $("#option3").val("");
                    $("#option4").val("");
                    $('input[type="radio"]').prop("checked", false);
                    toastr.success('Question added successfully');
                }
            }
        });

    });
</script>
<br/><br/><br/><br/>

<%@include file="footer.jsp" %>
