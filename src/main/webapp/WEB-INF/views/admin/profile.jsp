<%
    String adminUserName = (String) session.getAttribute("adminUserName");
    if (adminUserName == null) {
        response.sendRedirect("/admin");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">

        <%@include file="side_menu.jsp" %>
        <div id="maincontent" class="span5 pull-right" >
            <div id="myTabContent" class="tab-content">
                <div id="profile" class="tab-pane active">

                    <h1 style='color:#3399FF;'> Personal Details : </h1>
                    <table class='table table-hover' style='background-color:white;color:#808080;'>
                        <tr style="font-weight:bold;">
                            <td>Name : </td>
                            <td>${teacherdetails.teacherName}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Username : </td>
                            <td>${teacherdetails.teacherUsername}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Password : </td>
                            <td>******</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Email : </td>
                            <td>${teacherdetails.teacherEmail}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Mobile: </td>
                            <td>${teacherdetails.teacherPhoneNumber}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Alternate No : </td>
                            <td>${teacherdetails.alternatePhoneNo}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Permanent Address : </td>
                            <td>${teacherdetails.permanentAddress}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Permanent City : </td>
                            <td>${teacherdetails.permanentCity}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Permanent Pin No : </td>
                            <td>${teacherdetails.permanentPinno}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Permanent State : </td>
                            <td>${teacherdetails.permanentState}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Permanent Country: </td>
                            <td>${teacherdetails.permanentCountry}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Correspondence Address : </td>
                            <td>${teacherdetails.correspondenceAddress}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Correspondence City : </td>
                            <td>${teacherdetails.correspondenceCity}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Correspondence Pin No : </td>
                            <td>${teacherdetails.correspondencePinno}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Correspondence State : </td>
                            <td>${teacherdetails.correspondenceState}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Correspondence Country: </td>
                            <td>${teacherdetails.correspondenceCountry}</td>
                        </tr>
                    </table>

                </div>
                <br/>
            </div>

        </div>
    </div>
    <br/><br/>
</div>
<br/><br/><br/><br/>

<%@include file="footer.jsp" %>
</body>
</html>