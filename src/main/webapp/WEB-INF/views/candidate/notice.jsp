<%
    String candidateUserName = (String) session.getAttribute("candidateEmail");
    if (candidateUserName == null) {
        response.sendRedirect("/");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">

        <div class="span2">
            <ul class="nav nav-tabs nav-stacked nav-justified"  style='background-color:white;'>
                <li>
                    <a href="./dashboard" >Home</a>
                </li>
                <li>
                    <a href="./profile">Profile</a>
                </li>
                <li>
                    <a href="./subjects">View Subjects</a>
                </li>
                <li>
                    <a href="./results">View Results</a>
                </li>
                <li>
                    <a href="./exams">Exam</a>
                </li>
                <li>
                    <a href="./notice">Notification</a>
                </li>
                <li>
                    <a href="./changepassword">Change Password</a>
                </li>
            </ul>
        </div>
        <div id="maincontent" class="span5 pull-right" >
            <div id="myTabContent" class="tab-content">
                <div id="notice" class="tab-pane fade in active">
                    <table id="sortTableExample" class='table zebra-striped'>
                        <thead>
                            <tr>
                                <th class="green header" style="text-align: center">Date</th>
                                <th class="red header" style="text-align: center">Note</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="noticebean" items="${noticebeans}">
                                <tr>
                                    <td>${noticebean.createdDate}</td>
                                    <td><a href='#'>${noticebean.note}</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <br/>
            </div>

        </div>
    </div>
    <br/><br/>
</div>
<br/><br/><br/><br/>

<%@include file="footer.jsp" %>
</body>
</html>