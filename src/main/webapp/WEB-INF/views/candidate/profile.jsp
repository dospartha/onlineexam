<%
    String candidateUserName = (String) session.getAttribute("candidateEmail");
    if (candidateUserName == null) {
        response.sendRedirect("/");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">

        <div class="span2">
            <ul class="nav nav-tabs nav-stacked nav-justified"  style='background-color:white;'>
                <li>
                    <a href="./dashboard" >Home</a>
                </li>
                <li>
                    <a href="./profile">Profile</a>
                </li>
                <li>
                    <a href="./subjects">View Subjects</a>
                </li>
                <li>
                    <a href="./results">View Results</a>
                </li>
                <li>
                    <a href="./exams">Exam</a>
                </li>
                <li>
                    <a href="./notice">Notification</a>
                </li>
                <li>
                    <a href="./changepassword">Change Password</a>
                </li>
            </ul>
        </div>
        <div id="maincontent" class="span5 pull-right" >
            <div id="myTabContent" class="tab-content">
                <div id="profile" class="tab-pane active">

                    <h1 style='color:#3399FF;'> Personal Details : </h1>
                    <table class='table table-hover' style='background-color:white;color:#808080;'>
                        <tr style="font-weight:bold;">
                            <td>Name : </td>
                            <td>${candidatedetails.candidateName}</td>
                        </tr>

                        <tr style="font-weight:bold;">
                            <td>Email : </td>
                            <td>${candidatedetails.candidateEmail}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Mobile: </td>
                            <td>${candidatedetails.candidatePhonenumber}</td>
                        </tr>

                    </table>

                </div>
                <br/>
            </div>

        </div>
    </div>
    <br/><br/>
</div>
<br/><br/><br/><br/>

<%@include file="footer.jsp" %>
</body>
</html>