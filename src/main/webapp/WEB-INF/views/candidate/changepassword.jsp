<%
    String candidateUserName = (String) session.getAttribute("candidateEmail");
    if (candidateUserName == null) {
        response.sendRedirect("/");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">

        <div class="span2">
            <ul class="nav nav-tabs nav-stacked nav-justified"  style='background-color:white;'>
                <li>
                    <a href="./dashboard" >Home</a>
                </li>
                <li>
                    <a href="./profile">Profile</a>
                </li>
                <li>
                    <a href="./subjects">View Subjects</a>
                </li>
                <li>
                    <a href="./results">View Results</a>
                </li>
                <li>
                    <a href="./exams">Exam</a>
                </li>
                <li>
                    <a href="./notice">Notification</a>
                </li>
                <li>
                    <a href="./changepassword">Change Password</a>
                </li>
            </ul>
        </div>
        <div id="maincontent" class="span5 pull-right" >
            <div id="myTabContent" class="tab-content">
                <div id="ChangPassword" class="tab-pane fade in active">
                    <form id="candidateChangePasswordBean" class="form-horizontal" method="post">
                        <div class="control-group">
                            <label class="control-label" for="candidateOldPassword">Current Password</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-book"></i></span>
                                    <input type="password" class="input-large" name="candidateOldPassword" id="candidateOldPassword" required="true" placeholder="Current Password"/>
                                    <span id="err"> </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="candidateNewPassword">New Password</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-barcode"></i></span>
                                    <input type="password" class="input-large" name="candidateNewPassword" id="candidateNewPassword" required="true" placeholder="New Password"/>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="candidateNewConfirmPassword">Re Password</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-barcode"></i></span>
                                    <input type="password" class="input-large" name="candidateNewConfirmPassword" id="candidateNewConfirmPassword" required="true" placeholder="New Password"/>
                                </div>
                            </div>
                        </div>

                        <div class="control-group">
                            <label class="control-label"></label>
                            <div class="controls">
                                <c:if test='${not empty param["Success"]}'>
                                    <p style="color:green;font-weight:bold;">Your Password is changed successfully.</p>
                                </c:if>

                                <c:if test='${not empty param["Failed"]}'>
                                    <p style="color:red;font-weight:bold;">Wrong current Password.</p>

                                </c:if>
                                <button type="submit" class="btn btn-success"  id="candidatepasschange">Submit</button>
                            </div>
                        </div>


                    </form>

                </div>
                <br/>
            </div>

        </div>
    </div>
    <br/><br/>
</div>
<br/><br/><br/><br/>

<%@include file="footer.jsp" %>
</body>
</html>