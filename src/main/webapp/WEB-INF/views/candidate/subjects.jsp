<%
    String candidateUserName = (String) session.getAttribute("candidateEmail");
    if (candidateUserName == null) {
        response.sendRedirect("/");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">

        <div class="span2">
            <ul class="nav nav-tabs nav-stacked nav-justified"  style='background-color:white;'>
                <li>
                    <a href="./dashboard" >Home</a>
                </li>
                <li>
                    <a href="./profile">Profile</a>
                </li>
                <li>
                    <a href="./subjects">View Subjects</a>
                </li>
                <li>
                    <a href="./results">View Results</a>
                </li>
                <li>
                    <a href="./exams">Exam</a>
                </li>
                <li>
                    <a href="./notice">Notification</a>
                </li>
                <li>
                    <a href="./changepassword">Change Password</a>
                </li>
            </ul>
        </div>
        <div id="maincontent" class="span5 pull-right" >
            <div id="myTabContent" class="tab-content">
                <div id="viewsub" class="tab-pane active">

                    <table id="sortTableExample" class='table zebra-striped'>
                        <thead>
                            <tr>
                                <th class="header">S No.</th>
                                <th class="red header">Subject Name</th>
                                <th class="blue header">Subject Code</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>fdgd</td>
                                <td>fdg</td>
                            </tr>
                        </tbody>
                    </table>
                    <br/>
                </div>
                <br/>
            </div>

        </div>
    </div>
    <br/><br/>
</div>
<br/><br/><br/><br/>

<%@include file="footer.jsp" %>
</body>
</html>