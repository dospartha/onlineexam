<%
    String candidateUserName = (String) session.getAttribute("candidateEmail");
    if (candidateUserName == null) {
        response.sendRedirect("/");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">

        <div class="span2">
            <ul class="nav nav-tabs nav-stacked nav-justified"  style='background-color:white;'>
                <li>
                    <a href="./dashboard" >Home</a>
                </li>
                <li>
                    <a href="./profile">Profile</a>
                </li>
                <li>
                    <a href="./subjects">View Subjects</a>
                </li>
                <li>
                    <a href="./results">View Results</a>
                </li>
                <li>
                    <a href="./exams">Exam</a>
                </li>
                <li>
                    <a href="./notice">Notification</a>
                </li>
                <li>
                    <a href="./changepassword">Change Password</a>
                </li>
            </ul>
        </div>
        <div id="maincontent" class="span5 pull-right" >
            <div id="myTabContent" class="tab-content">
                <div id="exam" class="tab-pane active" >

                    <form action="showpaper_servlet" id="contact-form" class="form-horizontal" method="post">


                        <div class="control-group">
                            <label class="control-label" for="password">Subject Code</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-barcode"></i></span>
                                    <input type="text" class="input-large" name="scode" id="scode" required="true" placeholder="Subject Code"/>
                                </div>
                                <c:if test='${not empty param["alreadygiven"]}'>
                                    <p style='font-weight:bold;color:red'>You have already Given this Paper..</p>
                                </c:if>

                                <c:if test='${not empty param["error"]}'>
                                    <p style='font-weight:bold;color:red'>Wrong Subject Code</p>
                                </c:if>

                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"></label>
                            <div class="controls">
                                <button type="submit" class="btn btn-primary">Start Exam</button>
                            </div>
                        </div>
                    </form>
                </div>
                <br/>

            </div>

        </div>
    </div>
    <br/><br/>
</div>
<br/><br/><br/><br/>

<%@include file="footer.jsp" %>
</body>
</html>