<%-- 
    Document   : index
    Created on : Aug 29, 2018, 6:16:47 PM
    Author     : monoj
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="description" content="On-line Exam System is very useful for Educational Institute to prepare an exam, save the time that will take to check the paper and prepare mark sheets. It will help the Institute to testing of students and develop their skills. But the disadvantages for this system, it takes a lot of times when you prepare the exam at the first time for usage. And we are needs number of computers with the same number of students."/>
        <meta name="keywords" content="sacredheart,sacred,career,academy,dosinfotech,dos,shillong,mcqquestions,online,exam,student,teacher,onlineexam" />
        <meta name="author" content="Monoj Majumder" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Sacred Heart Career Academy</title>
        <link rel="shortcut icon" type="image/x-icon" href="${context}/assets/img/favicon.ico"/>
        <script type="text/javascript" src="${context}/assets/js/jquery.js"></script>
        <script type="text/javascript" src="${context}/assets/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="${context}/assets/js/jquery.validate.js"></script>
        <script type="text/javascript" src="${context}/assets/js/jquery.validate.min1.js"></script>

        <link href="${context}/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="${context}/assets/css/bootstrap.icon-large.min.css" rel="stylesheet" type="text/css" />
        <link href="${context}/assets/css/bootstrap-responsive.css" rel="stylesheet" type="text/css" />
        <link href="${context}/assets/custom/style.css" rel="stylesheet" type="text/css" />
        <link href="${context}/assets/css/sweet-alert.min.css" rel="stylesheet" type="text/css" />
        <link href="${context}/assets/js/toastr/toastr.min.css" rel="stylesheet" type="text/css" />
        <link href="${context}/assets/js/ladda/dist/ladda-themeless.min.css" rel="stylesheet" type="text/css" />
        <style type="text/css">
            .navbar-inner{
                background:#000;
                border-bottom:5px solid #007AF4;
                height:70px;

            }
            .navbar-inner .brand{color:#FFF}

        </style>
    </head>
    <body>
        <script type="text/javascript" src="${context}/assets/js/bootstrap-tab.js"></script>
        <script type="text/javascript" src="${context}/assets/js/bootstrap-carousel.js"></script>
        <script type="text/javascript" src="${context}/assets/js/bootstrap-dropdown.js"></script>
        <script type="text/javascript" src="${context}/assets/js/bootstrap-collapse.js"></script>
        <script type="text/javascript" src="${context}/assets/js/bootstrap-button.js"></script>
        <script type="text/javascript" src="${context}/assets/js/bootstrap.js"></script>
        <script type="text/javascript" src="${context}/assets/custom/index.js"></script>
        <script type="text/javascript" src="${context}/assets/js/sweet-alert.min.js"></script>
        <script type="text/javascript" src="${context}/assets/js/toastr/toastr.min.js"></script>
        <script type="text/javascript" src="${context}/assets/js/ladda/dist/spin.min.js"></script>
        <script type="text/javascript" src="${context}/assets/js/ladda/dist/ladda.min.js"></script>
        <script type="text/javascript" src="${context}/assets/js/ladda/dist/ladda.jquery.min.js"></script>
        <script type="text/javascript" src="${context}/assets/custom/feedback.js"></script>
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container">

                    <a href="/" class="brand"> <img src="${context}/assets/img/examshow.png" alt="Online Exam" width="100" height="70"/></a>
                    <br/>

                    <h1 class="brand" style="font-weight:bold;">SacredHeart Career Academy Exam Portal</h1>

                    <h3 class="brand" style="font-weight:bold;" align="center"><a href="/">Home</a></h3>
                    <h3 class="brand" style="font-weight:bold;" align="center"><a href="./upcomingevents">Events<img src="${context}/assets/img/new.gif" border="0" style="margin-top: -10px"/></a></h3>
                </div>
            </div>
        </div>
