<%@include file="../header.jsp" %>
<div class="container well">
    <div class="row">
        <div class="span7" >

            <div class="bs-docs-example">
                <div id="myCarousel" class="carousel slide" style="height:350px; width:600px;">
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item" style="height:350px; width:600px;">
                            <img src="assets/img/exam3.jpg" height="350" width="600" alt="ExamShow" />
                        </div>
                        <div class="item">

                            <img src="assets/img/exam3.jpg" height="350" width="600" alt="ExamShow"/>
                        </div>
                        <div class="item">

                            <img src="assets/img/exam3.jpg" height="350" width="600" alt="ExamShow"/>
                        </div>
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                </div>
            </div>

        </div>

        <div id="maincontent" class="span5 pull-right ">
            <ul class="nav nav-tabs nav-justified">
                <li class="active"><a href="#teacherLogin" data-toggle="tab">Teacher LogIn</a></li>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div  id="teacherLogin" class="tab-pane active">
                    <form id="teacherLoginBean" class="form-horizontal" method="post">
                        <div class="control-group">
                            <label class="control-label" for="teacherLoginId">Username</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-user"></i></span>
                                    <input type="text" class="input-large" name="teacherLoginId" id="teacherLoginId" placeholder="User Name"/>
                                    <span id="err"> </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="teacherLoginPassword">Password</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-lock"></i></span>
                                    <input type="password" class="input-large" name="teacherLoginPassword" id="teacherLoginPassword" placeholder="******"/>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"></label>
                            <div class="controls">
                                <button type="submit" class="btn btn-success" data-loading-text="Loading..." id="teacherlogin">Log In</button>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"></label>
                            <div class="controls">
                                <a href="#">Forgotten account?</a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div><br/><br/><br/><br/>
<%@include file="../footer.jsp" %>