<%
    String teacherUserName = (String) session.getAttribute("teacherEmail");
    if (teacherUserName == null) {
        response.sendRedirect("/teacher");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">

        <%@include file="side_menu.jsp" %>
        <div id="maincontent" class="span5 pull-right" >
            <div id="myTabContent" class="tab-content">
                <div id="notice" class="tab-pane fade in active">
                    <table id="sortTableExample" class='table zebra-striped'>
                        <thead>
                            <tr>
                                <th class="green header" style="text-align: center">Date</th>
                                <th class="red header" style="text-align: center">Note</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="noticebean" items="${noticebeans}">
                                <tr>
                                    <td>${noticebean.createdDate}</td>
                                    <td><a href='#'>${noticebean.note}</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <br/>
            </div>

        </div>
    </div>
    <br/><br/>
</div>
<br/><br/><br/><br/>

<%@include file="footer.jsp" %>
</body>
</html>