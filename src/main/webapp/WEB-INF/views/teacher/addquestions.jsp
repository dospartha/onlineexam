<%
    String teacherUserName = (String) session.getAttribute("teacherEmail");
    if (teacherUserName == null) {
        response.sendRedirect("/teacher");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">

        <%@include file="side_menu.jsp" %>
        <div id="maincontent" class="span5 pull-right" >
            <div id="myTabContent" class="tab-content">
                <div id="addquestions" class="tab-pane active">
                    <form id="teacherAddQuestionBean" class="form-horizontal" method="post">
                        <div class="control-group">
                            <label class="control-label" for="questionNo">Question Number:</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <input type="text" class="span5" name="questionNo" id="questionNo" value="1" required/>
                                    <span id="err"> </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="question">Question:</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <textarea class="mceEditor"  name="question" rows="15" cols="50" placeholder="Question" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="option1">Option 1:</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <input type="text" class="span5" name="option1" id="option1" placeholder="Option 1" required/>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="option2">Option 2:</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <input type="text" class="span5" name="option2" id="option2" placeholder="Option 2" required/>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="option3">Option 3:</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <input type="text" class="span5" name="option3" id="option3" placeholder="Option 3" required/>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="option4">Option 4:</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <input type="text" class="span5" name="option4" id="option4" placeholder="Option 4"/>
                                </div>
                            </div>
                        </div>


                        <div class="control-group">
                            <label class="control-label"></label>
                            <div class="controls">
                                <c:if test='${not empty param["Success"]}'>
                                    <p style="color:green;font-weight:bold;">Add Question successfully.</p>
                                </c:if>

                                <c:if test='${not empty param["Failed"]}'>
                                    <p style="color:red;font-weight:bold;">Something went wrong Try again.</p>

                                </c:if>
                                <button type="submit" class="btn btn-success"  id="addQuestion">Add Question</button>
                            </div>
                        </div>
                    </form>

                </div>
                <br/>
            </div>

        </div>
    </div>
    <br/><br/>
</div>
<br/><br/><br/><br/>

<%@include file="footer.jsp" %>
</body>
</html>