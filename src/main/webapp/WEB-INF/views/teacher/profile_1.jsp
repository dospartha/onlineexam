<%
    String teacherUserName = (String) session.getAttribute("teacherEmail");
    if (teacherUserName == null) {
        response.sendRedirect("/teacher");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">

        <%@include file="side_menu.jsp" %>
        <div id="maincontent" class="span5 pull-right" >
            <div id="myTabContent" class="tab-content">
                <div id="profile" class="tab-pane active">

                    <h1 style='color:#3399FF;'> Personal Details : </h1>
                    <table class='table table-hover' style='background-color:white;color:#808080;'>
                           <tr style="font-weight:bold;">
                            <td>Name : </td>
                            <td>${candidatedetails.candidateName}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Institute : </td>
                            <td>${candidatedetails.candidateInstitute}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Email : </td>
                            <td>${candidatedetails.candidateEmail}</td>
                        </tr>
                        <tr style="font-weight:bold;">
                            <td>Mobile: </td>
                            <td>${candidatedetails.candidatePhoneNumber}</td>
                        </tr>

                    </table>

                </div>
                <br/>
            </div>

        </div>
    </div>
    <br/><br/>
</div>
<br/><br/><br/><br/>

<%@include file="footer.jsp" %>
</body>
</html>