<%
    String teacherUserName = (String) session.getAttribute("teacherEmail");
    if (teacherUserName == null) {
        response.sendRedirect("/teacher");
    } else {
%>
<%@include file="header.jsp" %>
<% }%>
<div class="container well">
    <div class="row">
        <%@include file="side_menu.jsp" %>

        <div id="maincontent" class="span5 pull-right" >
            <div id="myTabContent" class="tab-content">
                <div id="home" class="tab-pane active">
                    <div class="span10">
                        <center><img src="../assets/img/back.png" alt="Online Exam System" height="300" width="400"/></center>
                        <p style="font-weight: bold;font-size:20px;color:#808080;line-height: 25px;">
                        <bold style="color:black;">On-line Exam System</bold> is very useful for Educational Institute to prepare an exam, safe the time that will take to check the paper and prepare mark sheets. It will help the Institute to testing of students and develop their skills. But the disadvantages for this system, it takes a lot of times when you prepare the exam at the first time for usage. And we are needs number of computers with the same number of students.
                        </p>
                        <p style="font-weight: bold;font-size:20px;color:#808080;line-height: 25px;"  >
                            The effective use of "On-line Exam System", any Educational Institute or training centers can be use it to develop their strategy for putting the exams, and for getting better results in less time.
                        </p>
                    </div>
                </div>
                <br/>
            </div>
        </div>
    </div>
    <br/><br/>
</div>
<br/><br/><br/><br/>

<%@include file="footer.jsp" %>
</body>
</html>