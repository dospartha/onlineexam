<%@include file="header.jsp" %>
<div class="container well">
    <div class="row">
        <div class="span7" >

            <div class="bs-docs-example">
                <div id="myCarousel" class="carousel slide" style="height:350px; width:600px;">
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>
                    <!-- Carousel items -->
                    <div class="carousel-inner">
                        <div class="active item" style="height:350px; width:600px;">
                            <img src="assets/img/exam3.jpg" height="350px" width="600px" alt="ExamShow" />
                        </div>
                        <div class="item">

                            <img src="assets/img/exam3.jpg" height="350px" width="600px" alt="ExamShow"/>
                        </div>
                        <div class="item">

                            <img src="assets/img/exam3.jpg" height="350px" width="600px" alt="ExamShow"/>
                        </div>
                    </div>
                    <!-- Carousel nav -->
                    <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                    <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
                </div>
            </div>

        </div>

        <div id="maincontent" class="span5 pull-right ">
            <ul class="nav nav-tabs nav-justified">
                <li class="active"><a href="#candidateLogin" data-toggle="tab">Log In</a></li>
                <%--<li><a href="#candidateRegistration" data-toggle="tab">Sign Up</a></li>--%>
            </ul>
            <div id="myTabContent" class="tab-content">
                <div  id="candidateLogin" class="tab-pane active">
                    <form id="candidateLoginBean" class="form-horizontal" method="post">
                        <div class="control-group">
                            <label class="control-label" for="candidateLoginId">Email or Phone</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-user"></i></span>
                                    <input type="text" class="input-large" name="candidateLoginId" id="candidateLoginId" placeholder="Email or Phone"/>
                                    <span id="err"> </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="candidateLoginPassword">Password</label>
                            <div class="controls">
                                <div class="input-prepend">
                                    <span class="add-on"><i class="icon-lock"></i></span>
                                    <input type="password" class="input-large" name="candidateLoginPassword" id="candidateLoginPassword" placeholder="******"/>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label"></label>
                            <div class="controls">
                                <button type="submit" class="btn btn-success" data-loading-text="Loading..." id="candidatelogin">Log In</button>
                            </div>
                        </div>
                        <div class="control-group">
                            <%--<label class="control-label"></label>--%>
                            <div class="controls">
                                <a class="text-white" href="forgotPassword">
                                    <i class="fa fa-sign-in text-white"></i> forgotPassword?
                                </a>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <a class="text-white" href="registration">
                                    <i class="fa fa-sign-in text-white"></i> Register
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><br/><br/><br/><br/>
<%@include file="footer.jsp" %>