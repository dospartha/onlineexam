<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="height: 800px; width: 700px">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
        <h3 id="myModalLabel">Feedback form</h3>
    </div>
    <div class="modal-body">
        <form id="feedbackBean" class="form-horizontal" method="post">

            <div class="control-group">
                <label class="control-label" for="fbName">Name</label>
                <div class="controls">

                    <input type="text" class="input-large span5" name="fbName" id="fbName" placeholder="Name"/>
                    <span id="err"> </span>

                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fbEmail">Email Address</label>
                <div class="controls">
                    <input type="text" class="input-large span5" name="fbEmail" id="fbEmail" placeholder="Email address"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fbNumber">Mobile No.</label>
                <div class="controls">
                    <input type="text" class="input-large span5" name="fbNumber" id="fbNumber" maxlength="10" placeholder="Mobile number"/>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="fbComment">Comment</label>
                <div class="controls">
                    <textarea class="form-control span5" name="fbComment" id="fbComment" rows="8" placeholder="Put youe Comment......" required="">
                    </textarea>
                    <span id="err"> </span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"></label>
                <div class="controls">
                    <button type="submit" class="btn btn-success btn-large" id="feedbackSend" data-loading-text="Loading...">Send</button>
                </div>
            </div>
        </form>


    </div>
</div>