<%--
  Created by IntelliJ IDEA.
  User: dos-49
  Date: 31/10/18
  Time: 5:25 PM
  To change this template use File | Settings | File Templates.
--%>
<%@include file="header.jsp" %>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-push-3">
                <div id="success-alert">

                    <div class="space-6"></div>

                </div>
                <div class="border-1px p-25">
                    <h4 class="text-theme-colored text-uppercase m-0"> Forgot Password   </h4>
                    <div class="line-bottom mb-30"></div>
                    <form id="" name="" class="form-transparent mt-30" method="post" action="#">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-10">
                                    <input id="form_email" name="email" class="form-control" type="text" required="" placeholder="Enter Registered Email ID" aria-required="true" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-10">
                                    <span class="pull-left">I have Password..! <a class="text-theme-colored text-underline"     href="login.html"        > Login here..  </a></span>
                                    <span class="pull-right">You have no account.Go to <a class="text-theme-colored text-underline"       href="register.html"          > Register</a></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-0 mt-30 text-center">
                            <input type="submit" name="forgot" class="btn btn-dark btn-colored btn-theme-colored" value="Submit" />
                        </div>
                    </form>
                    <!-- Appointment Form Validation-->
                </div>
            </div>
        </div>
    </div>
</section>
<%@include file="footer.jsp" %>