<%--
  Created by IntelliJ IDEA.
  User: dos-49
  Date: 29/10/18
  Time: 6:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@include file="header.jsp" %>
<div class="container well">
    <div class="row">
        <div class="span3"><img src="assets/img/shlogo.png" alt="Online Exam" width="100"
                                height="70" <%--style="display: none"--%>></div>
        <div id="maincontent" class="span9">
            <form id="candidateRegistrationBean" class="form-horizontal" method="post">
                <div class="control-group">
                    <label class="control-label" for="moduleId">Register for exam</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-dropbox"></i></span>
                            <select class="form-control" name="moduleId" id="moduleId">
                                <option value="">Select</option>
                                <c:forEach var="Modules" items="${modules}">
                                    <option value="${Modules.id}">${Modules.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="classId">Class</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-dropbox"></i></span>
                            <select class="form-control" name="classId" id="classId">
                                <option value="">Select</option>
                                <c:forEach var="Class" items="${classes}">
                                    <option value="${Class.id}">${Class.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="candidateName">Candidate Name</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user"></i></span>
                            <input type="text" class="input-large" name="candidateName" id="candidateName"
                                   placeholder="Candidate Name"/>
                            <span id="err"> </span>
                        </div>
                    </div>
                </div>
                <%--<div class="control-group">
                    <label class="control-label" for="candidateUsername">User Name</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user"></i></span>
                            <input type="text" class="input-large" name="candidateUsername" id="candidateUsername"
                                   placeholder="User Name"/>
                            <span id="err_candidateUsername" style="color:red;font-weight:bold;display: none">Username already exists.</span>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="candidatePassword">Password</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-lock"></i></span>
                            <input type="password" class="input-large" name="candidatePassword" id="candidatePassword"
                                   placeholder="******"/>
                        </div>
                    </div>
                </div>--%>
                <div class="control-group">
                    <label class="control-label" for="candidateEmail">Email Address</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span>
                            <input type="text" class="input-large" name="candidateEmail" id="candidateEmail"
                                   placeholder="Email address"/>
                            <span id="err_candidateEmail"></span>
                            <%--<span id="err_candidateEmail" style="color:red;font-weight:bold;display: none">Candidate Email already exists.</span>--%>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="candidatePhoneNumber">Mobile No.</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-book"></i></span>
                            <input type="text" class="input-large" name="candidatePhonenumber" id="candidatePhonenumber"
                                   maxlength="10" placeholder="Mobile number"/>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Gender</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <input type="radio" value="male" name="gender"> Male
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" value="female" name="gender"> Female
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label for="dob" class="control-label">Date of Birth</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-calendar"></i></span>
                            <input type="date" class="form-control" id="dob" name="dob" value="2000-01-08">
                        </div>
                    </div>
                </div>

                <div id="paymentDetailsDiv">
                    <legend>Payment Information</legend>
                </div>
                <div class="control-group">
                    <label class="control-label" for="registrationfee">Registration Fee</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-lock"></i></span>
                            <input type="text" name="registrationfee" id="registrationfee" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label for="paymentDateForXi" class="control-label">Payment Date</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-calendar"></i></span>
                            <input type="date" class="form-control" id="paymentDateForXi" name="paymentDateForXi">
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label for="paymentModeCash" class="control-label">Payment Type</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <input type="radio" class="paymentMode" id="paymentModeCheque" name="paymentTypenameForXI" value="0">Cheque
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" class="paymentMode" id="paymentModeCash" name="paymentTypenameForXI" checked value="1"> Cash
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label for="courseAmount" class="control-label">Amount</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <input type="text" class="form-control" id="courseAmount" <%--disabled--%>>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label for="gstAomunt" class="control-label">GST(%)</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <input type="text" class="form-control" id="gstAomunt" <%--disabled--%>>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label for="amountTotal" class="control-label">Payble Amount</label>
                    <div class="controls">
                        <div class="input-prepend">
                            <input type="text" class="form-control" id="amountTotal" <%--disabled--%>>
                        </div>
                    </div>
                </div>
                <div id="checkDetailsDiv" style="display: none">
                    <legend>Cheque Details</legend>
                    <div class="control-group">
                        <label for="checkNo" class="control-label">Cheque No</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <input type="text" value="" id="checkNo" class="form-control" name="check_no">
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="bankName" class="control-label">Bank Name</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <input type="text" value="" id="bankName" class="form-control" name="bank_Name">
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="bankBranch" class="control-label">Branch Name</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <input type="text" vlabelalue="" id="bankBranch" class="form-control"
                                       name="bank_Branch">
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="dateofCheck" class="control-label">Date of Issue</label>
                        <div class="controls">
                            <div class="input-prepend">
                                <input type="date" id="dateofCheck" class="form-control" name="dateof_Check">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label"></label>
                    <div class="controls">
                        <button type="submit" class="btn btn-success" id="candidateregistration">Create My Account
                        </button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
        <%--<div class="span3">
        </div>--%>
    </div>
</div>

<script>

    $(".paymentMode").on("click", function () {
        if ($(this).val() == 0) {
            $("#checkDetailsDiv").show();
            $("#paymentModeCash").prop("checked", false);
        } else {
            $("#checkDetailsDiv").css('display', "none");
            $("#paymentModeCheque").prop("checked", false);

        }
    });
</script>
<br/><br/><br/><br/>
<%@include file="footer.jsp" %>


