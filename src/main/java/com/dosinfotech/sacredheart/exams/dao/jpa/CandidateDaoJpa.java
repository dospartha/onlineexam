package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.CandidateDao;
import com.dosinfotech.sacredheart.exams.domain.Candidates;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;

@Repository
public class CandidateDaoJpa extends BaseDaoJpa<Candidates> implements CandidateDao {

    public CandidateDaoJpa() {
        super(Candidates.class, "Candidates");
        // TODO Auto-generated constructor stub
    }

    @Override
    public Candidates checkCandidateEmailExistOrNot(String candidateEmail) {
        try {
            Query query = getEntityManager().createQuery("SELECT c FROM Candidates AS c WHERE c.candidateEmail=:candidateEmail");
            query.setParameter("candidateEmail", candidateEmail);
            return (Candidates) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Candidates checkStudentUserNameExistOrNot(String candidateUsername) {
        try {
            Query query = getEntityManager().createQuery("SELECT c FROM Candidates AS c WHERE c.candidateUsername=:candidateUsername");
            query.setParameter("candidateUsername", candidateUsername);
            return (Candidates) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Candidates candidatelogin(String candidateLoginId, String candidateLoginPassword) {
        try {
            Query query = getEntityManager().createQuery("SELECT c FROM Candidates AS c WHERE c.candidateEmail=:candidateEmail OR c.candidatePhonenumber=:candidatePhonenumber AND c.candidatePassword=:candidatePassword");
            query.setParameter("candidateEmail", candidateLoginId);
            query.setParameter("candidatePhonenumber", candidateLoginId);
            query.setParameter("candidatePassword", candidateLoginPassword);
            return (Candidates) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
