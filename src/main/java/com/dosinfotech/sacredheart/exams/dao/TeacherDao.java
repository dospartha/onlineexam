package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.Teachers;

public interface TeacherDao extends BaseDao<Teachers> {

    public Teachers teacherlogin(String teacherLoginId, String teacherLoginPassword);

}
