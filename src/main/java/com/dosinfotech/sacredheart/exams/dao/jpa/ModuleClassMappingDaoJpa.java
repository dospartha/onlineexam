package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.ModuleClassMappingDao;
import com.dosinfotech.sacredheart.exams.domain.ModuleClassMappings;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class ModuleClassMappingDaoJpa extends BaseDaoJpa<ModuleClassMappings> implements ModuleClassMappingDao {

    public ModuleClassMappingDaoJpa() {
        super(ModuleClassMappings.class, "ModuleClassMappings");
        // TODO Auto-generated constructor stub
    }

    @Override
    public List<ModuleClassMappings> getAllClassesByModuleId(long moduleId) {
        try {
            Query query = getEntityManager().createQuery("select mcm from ModuleClassMappings AS mcm where mcm.modules.id=:moduleId");
            query.setParameter("moduleId", moduleId);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }
}
