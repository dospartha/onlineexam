package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.QuestionsDao;
import com.dosinfotech.sacredheart.exams.domain.Questions;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;

/**
 * @author Partha Sarothi Banerjee
 */

@Repository
public class QuestionsDaoJpa extends BaseDaoJpa<Questions> implements QuestionsDao{


    public QuestionsDaoJpa() {
        super(Questions.class, "Questions");
    }

    @Override
    public Object[] getQuestionNumber(long moduleId, long classId, long subjectId, long chapterId) {
        try {
           Query query = getEntityManager().createNativeQuery("SELECT * FROM questions WHERE id = (SELECT MAX(id) FROM questions  where module_id=:module_id and class_id=:class_id and subject_id=:subject_id and chapter_id=:chapter_id)");
            query.setParameter("module_id", moduleId);
            query.setParameter("class_id", classId);
            query.setParameter("subject_id", subjectId);
            query.setParameter("chapter_id", chapterId);
            return (Object[]) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }
}
