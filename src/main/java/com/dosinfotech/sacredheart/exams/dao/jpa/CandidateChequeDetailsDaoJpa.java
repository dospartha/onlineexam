package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.CandidateChequeDetailsDao;
import com.dosinfotech.sacredheart.exams.domain.CandidateChequeDetails;
import org.springframework.stereotype.Repository;

/**
 * @author Partha Sarothi Banerjee
 */
@Repository
public class CandidateChequeDetailsDaoJpa extends BaseDaoJpa<CandidateChequeDetails> implements CandidateChequeDetailsDao{

    public CandidateChequeDetailsDaoJpa() {
        super(CandidateChequeDetails.class, "CandidateChequeDetails");
    }

}
