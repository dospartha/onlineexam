package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.FeedbackDao;
import com.dosinfotech.sacredheart.exams.domain.Feedback;
import org.springframework.stereotype.Repository;

@Repository
public class FeedbackDaoJpa extends BaseDaoJpa<Feedback> implements FeedbackDao {

    public FeedbackDaoJpa() {
        super(Feedback.class, "Feedback");
        // TODO Auto-generated constructor stub
    }

}
