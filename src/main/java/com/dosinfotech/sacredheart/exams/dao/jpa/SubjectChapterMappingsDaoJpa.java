package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.SubjectChapterMappingsDao;
import com.dosinfotech.sacredheart.exams.domain.SubjectChapterMappings;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class SubjectChapterMappingsDaoJpa extends BaseDaoJpa<SubjectChapterMappings> implements SubjectChapterMappingsDao{

    public SubjectChapterMappingsDaoJpa(){
        super(SubjectChapterMappings.class,"SubjectChapterMappings");
    }

    @Override
    public List<SubjectChapterMappings> getAllChaptersBySubjectId(long subjectId) {
        try {
            Query query = getEntityManager().createQuery("select scm from SubjectChapterMappings AS scm where scm.subjects.id=:subjectId");
            query.setParameter("subjectId", subjectId);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }
}
