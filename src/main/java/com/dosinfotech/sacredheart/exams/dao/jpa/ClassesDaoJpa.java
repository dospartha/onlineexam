package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.ClassesDao;
import com.dosinfotech.sacredheart.exams.domain.Classes;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class ClassesDaoJpa extends BaseDaoJpa<Classes> implements ClassesDao {

    public ClassesDaoJpa() {
        super(Classes.class, "Classes");
        // TODO Auto-generated constructor stub
    }
}
