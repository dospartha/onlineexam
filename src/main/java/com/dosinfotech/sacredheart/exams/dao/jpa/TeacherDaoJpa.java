package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.domain.Teachers;
import javax.persistence.Query;
import com.dosinfotech.sacredheart.exams.dao.TeacherDao;
import org.springframework.stereotype.Repository;

@Repository
public class TeacherDaoJpa extends BaseDaoJpa<Teachers> implements TeacherDao {

    public TeacherDaoJpa() {
        super(Teachers.class, "Teachers");
        // TODO Auto-generated constructor stub
    }

    @Override
    public Teachers teacherlogin(String teacherLoginId, String teacherLoginPassword) {
        try {
            Query query = getEntityManager().createQuery("SELECT t FROM Teachers AS t WHERE t.teacherUsername=:teacherLoginId AND t.teacherPassword=:teacherLoginPassword");
            query.setParameter("teacherLoginId", teacherLoginId);
            query.setParameter("teacherLoginPassword", teacherLoginPassword);
            return (Teachers) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
