package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.SubjectDao;
import com.dosinfotech.sacredheart.exams.domain.Subjects;
import org.springframework.stereotype.Repository;

/**
 * @author Partha Sarothi Banerjee
 */

@Repository
public class SubjectDaoJpa extends BaseDaoJpa<Subjects> implements SubjectDao{

    public SubjectDaoJpa() {
        super(Subjects.class, "Subjects");
    }
}
