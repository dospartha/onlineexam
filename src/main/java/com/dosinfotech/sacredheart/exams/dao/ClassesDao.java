package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.Classes;

import java.util.List;

public interface ClassesDao extends BaseDao<Classes> {

}
