package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.ChapterDao;
import com.dosinfotech.sacredheart.exams.domain.Chapters;
import org.springframework.stereotype.Repository;

/**
 * @author Partha Sarothi Banerjee
 */
@Repository
public class ChapterDaoJpa extends BaseDaoJpa<Chapters> implements ChapterDao{

    public ChapterDaoJpa() {
        super(Chapters.class, "Chapters");
    }
}
