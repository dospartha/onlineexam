package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.ClassSubjectMappings;

import java.util.List;

public interface ClassSubjectMappingsDao extends BaseDao<ClassSubjectMappings> {
    List<ClassSubjectMappings> getAllSubjectsByClassId(long classId);
}
