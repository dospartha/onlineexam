package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.ClassSubjectMappingsDao;
import com.dosinfotech.sacredheart.exams.domain.ClassSubjectMappings;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class ClassSubjectMappingsDaoJpa extends BaseDaoJpa<ClassSubjectMappings> implements ClassSubjectMappingsDao{

    public ClassSubjectMappingsDaoJpa() {
        super(ClassSubjectMappings.class, "ClassSubjectMappings");
        // TODO Auto-generated constructor stub
    }

    @Override
    public List<ClassSubjectMappings> getAllSubjectsByClassId(long classId) {
        try {
            Query query = getEntityManager().createQuery("select csm from ClassSubjectMappings AS csm where csm.classes.id=:classid");
            query.setParameter("classid", classId);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }
}
