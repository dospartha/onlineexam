package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.Subjects;

/**
 * @author Partha Sarothi Banerjee
 */
public interface SubjectDao extends BaseDao<Subjects>{
}
