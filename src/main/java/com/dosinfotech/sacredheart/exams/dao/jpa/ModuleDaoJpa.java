package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.ModuleDao;
import com.dosinfotech.sacredheart.exams.domain.Modules;
import org.springframework.stereotype.Repository;

@Repository
public class ModuleDaoJpa extends BaseDaoJpa<Modules> implements ModuleDao {

    public ModuleDaoJpa() {
        super(Modules.class, "Modules");
        // TODO Auto-generated constructor stub
    }
}
