package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.Notice;
import java.util.List;

public interface NoticeDao extends BaseDao<Notice> {

    public List<Notice> getNoticeDetails(int i);

}
