package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.CandidatePaymentDetailsDao;
import com.dosinfotech.sacredheart.exams.domain.CandidatePaymentDetails;
import org.springframework.stereotype.Repository;

/**
 * @author Partha Sarothi Banerjee
 */
@Repository
public class CandidatePaymentDetailsDaoJpa extends BaseDaoJpa<CandidatePaymentDetails> implements CandidatePaymentDetailsDao{

    public CandidatePaymentDetailsDaoJpa() {
        super(CandidatePaymentDetails.class, "CandidatePaymentDetails");
    }
}
