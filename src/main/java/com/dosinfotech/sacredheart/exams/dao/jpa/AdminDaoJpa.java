package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.domain.Admin;
import javax.persistence.Query;
import com.dosinfotech.sacredheart.exams.dao.AdminDao;
import org.springframework.stereotype.Repository;

@Repository
public class AdminDaoJpa extends BaseDaoJpa<Admin> implements AdminDao {

    public AdminDaoJpa() {
        super(Admin.class, "Admin");
        // TODO Auto-generated constructor stub
    }

    @Override
    public Admin adminlogin(String adminLoginId, String adminLoginPassword) {
        try {
            Query query = getEntityManager().createQuery("SELECT a FROM Admin AS a WHERE a.username=:adminLoginId AND a.password=:adminLoginPassword");
            query.setParameter("adminLoginId", adminLoginId);
            query.setParameter("adminLoginPassword", adminLoginPassword);
            return (Admin) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
