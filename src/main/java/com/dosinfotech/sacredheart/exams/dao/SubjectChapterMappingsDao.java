package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.SubjectChapterMappings;

import java.util.List;

public interface SubjectChapterMappingsDao extends BaseDao<SubjectChapterMappings>{
    List<SubjectChapterMappings>  getAllChaptersBySubjectId(long subjectId);
}
