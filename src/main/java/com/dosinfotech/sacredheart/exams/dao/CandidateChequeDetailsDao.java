package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.CandidateChequeDetails;

/**
 * @author Partha Sarothi Banerjee
 */
public interface CandidateChequeDetailsDao extends BaseDao<CandidateChequeDetails> {
}
