package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.Candidates;

public interface CandidateDao extends BaseDao<Candidates> {

    Candidates checkCandidateEmailExistOrNot(String candidateEmail);

    Candidates checkStudentUserNameExistOrNot(String candidateUsername);

    Candidates candidatelogin(String candidateLoginId, String candidateLoginPassword);
}
