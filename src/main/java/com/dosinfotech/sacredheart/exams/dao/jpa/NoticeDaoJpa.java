package com.dosinfotech.sacredheart.exams.dao.jpa;

import com.dosinfotech.sacredheart.exams.dao.NoticeDao;
import com.dosinfotech.sacredheart.exams.domain.Notice;
import org.springframework.stereotype.Repository;

import java.util.List;
import javax.persistence.Query;

@Repository
public class NoticeDaoJpa extends BaseDaoJpa<Notice> implements NoticeDao {

    public NoticeDaoJpa() {
        super(Notice.class, "Notice");
        // TODO Auto-generated constructor stub
    }

    @Override
    public List<Notice> getNoticeDetails(int i) {
        try {
            Query query = getEntityManager().createQuery("SELECT n FROM Notice AS n WHERE n.status=:status");
            query.setParameter("status", i);
            return query.getResultList();
        } catch (Exception e) {
            return null;
        }
    }

}
