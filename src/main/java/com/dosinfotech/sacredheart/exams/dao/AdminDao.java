package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.Admin;

public interface AdminDao extends BaseDao<Admin> {

    public Admin adminlogin(String adminLoginId, String adminLoginPassword);

}
