package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.Questions;

/**
 * @author Partha Sarothi Banerjee
 */
public interface QuestionsDao extends BaseDao<Questions>  {
    Object[] getQuestionNumber(long moduleId, long classId, long subjectId, long chapterId);
}
