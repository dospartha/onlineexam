package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.Feedback;

public interface FeedbackDao extends BaseDao<Feedback> {

}
