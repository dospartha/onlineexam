package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.CandidatePaymentDetails;

/**
 * @author Partha Sarothi Banerjee
 */
public interface CandidatePaymentDetailsDao extends BaseDao<CandidatePaymentDetails> {
}
