package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.ModuleClassMappings;

import java.util.List;

public interface ModuleClassMappingDao extends BaseDao<ModuleClassMappings> {
    public List<ModuleClassMappings> getAllClassesByModuleId(long moduleId);
}
