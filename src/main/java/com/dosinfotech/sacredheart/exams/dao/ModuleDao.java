package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.Modules;

public interface ModuleDao extends BaseDao<Modules> {
}
