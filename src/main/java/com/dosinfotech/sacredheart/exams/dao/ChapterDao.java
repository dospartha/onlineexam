package com.dosinfotech.sacredheart.exams.dao;

import com.dosinfotech.sacredheart.exams.domain.Chapters;

/**
 * @author Partha Sarothi Banerjee
 */
public interface ChapterDao extends BaseDao<Chapters>{
}
