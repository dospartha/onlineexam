package com.dosinfotech.sacredheart.exams.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Module_class_mappings")
public class ModuleClassMappings extends BaseDomain{

  private static final long serialVersionUID = 1L;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "module_id")
  private Modules modules;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "class_id")
  private Classes classes;
  @Column(name = "created_date")
  private Date createdDate = new Date();

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public Modules getModules() {
    return modules;
  }

  public void setModules(Modules modules) {
    this.modules = modules;
  }

  public Classes getClasses() {
    return classes;
  }

  public void setClasses(Classes classes) {
    this.classes = classes;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }
}
