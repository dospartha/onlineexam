package com.dosinfotech.sacredheart.exams.domain;

import java.util.Date;
import javax.persistence.*;

/**
 * The persistent class for the feedback table.
 *
 */
@Entity
@Table(name = "feedback")
@NamedQuery(name = "Feedback.findAll", query = "SELECT f FROM Feedback f")
public class Feedback extends BaseDomain {

    private static final long serialVersionUID = 1L;

    @Column(name = "fb_name")
    private String fbName;
    @Column(name = "fb_email")
    private String fbEmail;
    @Column(name = "fb_number")
    private String fbNumber;
    @Column(name = "fb_comment")
    private String fbComment;
    @Basic(optional = false)
    @Column(name = "fb_date")
    @Temporal(TemporalType.DATE)
    private Date fbDate;

    public Feedback() {
    }

    public String getFbName() {
        return fbName;
    }

    public void setFbName(String fbName) {
        this.fbName = fbName;
    }

    public String getFbEmail() {
        return fbEmail;
    }

    public void setFbEmail(String fbEmail) {
        this.fbEmail = fbEmail;
    }

    public String getFbNumber() {
        return fbNumber;
    }

    public void setFbNumber(String fbNumber) {
        this.fbNumber = fbNumber;
    }

    public String getFbComment() {
        return fbComment;
    }

    public void setFbComment(String fbComment) {
        this.fbComment = fbComment;
    }

    public Date getFbDate() {
        return fbDate;
    }

    public void setFbDate(Date fbDate) {
        this.fbDate = fbDate;
    }
}
