package com.dosinfotech.sacredheart.exams.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "exam_packages")
public class ExamPackages extends BaseDomain{

  private static final long serialVersionUID = 1L;
  @Column(name = "package_name")
  private String packageName;
  @Column(name = "created_date")
  private Date createdDate = new Date();
  @Column(name = "update_date")
  private Date updateDate;
  @Column(name = "status")
  private Long status;
  @Column(name = "no_of_exam")
  private Long noOfExam;
  @Column(name = "amount")
  private Double amount;
  @Column(name = "discrepancy_amount")
  private Double discrepancyAmount;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public String getPackageName() {
    return packageName;
  }

  public void setPackageName(String packageName) {
    this.packageName = packageName;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

  public Long getNoOfExam() {
    return noOfExam;
  }

  public void setNoOfExam(Long noOfExam) {
    this.noOfExam = noOfExam;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Double getDiscrepancyAmount() {
    return discrepancyAmount;
  }

  public void setDiscrepancyAmount(Double discrepancyAmount) {
    this.discrepancyAmount = discrepancyAmount;
  }
}
