package com.dosinfotech.sacredheart.exams.domain;

import java.util.Date;
import javax.persistence.*;

/**
 * The persistent class for the teachers table.
 *
 */
@Entity
@Table(name = "teachers")
@NamedQuery(name = "Teachers.findAll", query = "SELECT t FROM Teachers t")
public class Teachers extends BaseDomain {

    private static final long serialVersionUID = 1L;

    @Column(name = "teacher_name")
    private String teacherName;
    @Column(name = "teacher_email")
    private String teacherEmail;
    @Column(name = "teacher_phonenumber")
    private String teacherPhoneNumber;
    @Column(name = "alternate_phone_no")
    private String alternatePhoneNo;
    @Column(name = "permanent_address")
    private String permanentAddress;
    @Column(name = "permanent_city")
    private String permanentCity;
    @Column(name = "permanent_pinno")
    private String permanentPinno;
    @Column(name = "permanent_state")
    private String permanentState;
    @Column(name = "permanent_country")
    private String permanentCountry;
    @Column(name = "correspondence_address")
    private String correspondenceAddress;
    @Column(name = "correspondence_city")
    private String correspondenceCity;
    @Column(name = "correspondence_pinno")
    private String correspondencePinno;
    @Column(name = "correspondence_state")
    private String correspondenceState;
    @Column(name = "correspondence_country")
    private String correspondenceCountry;
    @Column(name = "status")
    private int status;
    @Column(name = "image_name")
    private String imageName;
    @Column(name = "teacher_username")
    private String teacherUsername;
    @Column(name = "teacher_password")
    private String teacherPassword;
    @Column(name = "created_by")
    private String createdBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "created_date")
    private Date createdDate;
    @Column(name = "updated_by")
    private String updatedBy;
    @Temporal(TemporalType.DATE)
    @Column(name = "updated_date")
    private Date updatedDate;
    @Column(name = "internal_teacher")
    private int internalTeacher;

    public Teachers() {
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getTeacherPhoneNumber() {
        return teacherPhoneNumber;
    }

    public void setTeacherPhoneNumber(String teacherPhoneNumber) {
        this.teacherPhoneNumber = teacherPhoneNumber;
    }

    public String getAlternatePhoneNo() {
        return alternatePhoneNo;
    }

    public void setAlternatePhoneNo(String alternatePhoneNo) {
        this.alternatePhoneNo = alternatePhoneNo;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPermanentCity() {
        return permanentCity;
    }

    public void setPermanentCity(String permanentCity) {
        this.permanentCity = permanentCity;
    }

    public String getPermanentPinno() {
        return permanentPinno;
    }

    public void setPermanentPinno(String permanentPinno) {
        this.permanentPinno = permanentPinno;
    }

    public String getPermanentState() {
        return permanentState;
    }

    public void setPermanentState(String permanentState) {
        this.permanentState = permanentState;
    }

    public String getPermanentCountry() {
        return permanentCountry;
    }

    public void setPermanentCountry(String permanentCountry) {
        this.permanentCountry = permanentCountry;
    }

    public String getCorrespondenceAddress() {
        return correspondenceAddress;
    }

    public void setCorrespondenceAddress(String correspondenceAddress) {
        this.correspondenceAddress = correspondenceAddress;
    }

    public String getCorrespondenceCity() {
        return correspondenceCity;
    }

    public void setCorrespondenceCity(String correspondenceCity) {
        this.correspondenceCity = correspondenceCity;
    }

    public String getCorrespondencePinno() {
        return correspondencePinno;
    }

    public void setCorrespondencePinno(String correspondencePinno) {
        this.correspondencePinno = correspondencePinno;
    }

    public String getCorrespondenceState() {
        return correspondenceState;
    }

    public void setCorrespondenceState(String correspondenceState) {
        this.correspondenceState = correspondenceState;
    }

    public String getCorrespondenceCountry() {
        return correspondenceCountry;
    }

    public void setCorrespondenceCountry(String correspondenceCountry) {
        this.correspondenceCountry = correspondenceCountry;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getTeacherUsername() {
        return teacherUsername;
    }

    public void setTeacherUsername(String teacherUsername) {
        this.teacherUsername = teacherUsername;
    }

    public String getTeacherPassword() {
        return teacherPassword;
    }

    public void setTeacherPassword(String teacherPassword) {
        this.teacherPassword = teacherPassword;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getInternalTeacher() {
        return internalTeacher;
    }

    public void setInternalTeacher(int internalTeacher) {
        this.internalTeacher = internalTeacher;
    }

}
