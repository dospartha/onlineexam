package com.dosinfotech.sacredheart.exams.domain;


import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "candidate_payment_details")
public class CandidatePaymentDetails extends BaseDomain{

  private static final long serialVersionUID = 1L;

  @Column(name = "invoice_id")
  private String invoiceId;
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "candidate_id")
  private Candidates candidates;
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "package_id")
  private ExamPackages examPackages;
  @Column(name = "payment_for")
  private String paymentFor;
  @Column(name = "amount")
  private Double amount;
  @Column(name = "tax_amount")
  private Double taxAmount;
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "cheque_id")
  private CandidateChequeDetails candidateChequeDetails;
  @Column(name = "payment_date")
  private Date paymentDate = new Date();
  @Column(name = "due_amount")
  private Double dueAmount = 0.0;
  @Column(name = "due_date")
  private Date dueDate;
  @Column(name = "discrepancy_amount")
  private Double discrepancyAmount = 0.0;
  @Column(name = "extra_amount")
  private Double extraAmount=0.0;
  @Column(name = "create_date")
  private Date createDate = new Date();
  @Column(name = "update_date")
  private Date updateDate;
  @Column(name = "status")
  private int status = 0;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public String getInvoiceId() {
    return invoiceId;
  }

  public void setInvoiceId(String invoiceId) {
    this.invoiceId = invoiceId;
  }

  public Candidates getCandidates() {
    return candidates;
  }

  public void setCandidates(Candidates candidates) {
    this.candidates = candidates;
  }

  public ExamPackages getExamPackages() {
    return examPackages;
  }

  public void setExamPackages(ExamPackages examPackages) {
    this.examPackages = examPackages;
  }

  public String getPaymentFor() {
    return paymentFor;
  }

  public void setPaymentFor(String paymentFor) {
    this.paymentFor = paymentFor;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Double getTaxAmount() {
    return taxAmount;
  }

  public void setTaxAmount(Double taxAmount) {
    this.taxAmount = taxAmount;
  }

  public CandidateChequeDetails getCandidateChequeDetails() {
    return candidateChequeDetails;
  }

  public void setCandidateChequeDetails(CandidateChequeDetails candidateChequeDetails) {
    this.candidateChequeDetails = candidateChequeDetails;
  }

  public Date getPaymentDate() {
    return paymentDate;
  }

  public void setPaymentDate(Date paymentDate) {
    this.paymentDate = paymentDate;
  }

  public Double getDueAmount() {
    return dueAmount;
  }

  public void setDueAmount(Double dueAmount) {
    this.dueAmount = dueAmount;
  }

  public Date getDueDate() {
    return dueDate;
  }

  public void setDueDate(Date dueDate) {
    this.dueDate = dueDate;
  }

  public Double getDiscrepancyAmount() {
    return discrepancyAmount;
  }

  public void setDiscrepancyAmount(Double discrepancyAmount) {
    this.discrepancyAmount = discrepancyAmount;
  }

  public Double getExtraAmount() {
    return extraAmount;
  }

  public void setExtraAmount(Double extraAmount) {
    this.extraAmount = extraAmount;
  }

  public Date getCreateDate() {
    return createDate;
  }

  public void setCreateDate(Date createDate) {
    this.createDate = createDate;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }
}
