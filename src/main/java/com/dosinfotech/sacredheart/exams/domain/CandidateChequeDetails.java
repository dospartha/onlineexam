package com.dosinfotech.sacredheart.exams.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "candidate_cheque_details")
public class CandidateChequeDetails extends BaseDomain{

  private static final long serialVersionUID = 1L;
  @Column(name = "bank_name")
  private String bankName;
  @Column(name ="bank_branch_name")
  private String bankBranchName;
  @Column(name = "cheque_no")
  private String chequeNo;
  @Column(name = "cheque_date")
  private Date chequeDate = new Date();
  @Column(name = "cheque_status")
  private int chequeStatus;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public String getBankName() {
    return bankName;
  }

  public void setBankName(String bankName) {
    this.bankName = bankName;
  }

  public String getBankBranchName() {
    return bankBranchName;
  }

  public void setBankBranchName(String bankBranchName) {
    this.bankBranchName = bankBranchName;
  }

  public String getChequeNo() {
    return chequeNo;
  }

  public void setChequeNo(String chequeNo) {
    this.chequeNo = chequeNo;
  }

  public Date getChequeDate() {
    return chequeDate;
  }

  public void setChequeDate(Date chequeDate) {
    this.chequeDate = chequeDate;
  }

  public int getChequeStatus() {
    return chequeStatus;
  }

  public void setChequeStatus(int chequeStatus) {
    this.chequeStatus = chequeStatus;
  }
}
