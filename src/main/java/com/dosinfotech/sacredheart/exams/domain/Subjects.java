package com.dosinfotech.sacredheart.exams.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "subjects")
public class Subjects extends BaseDomain {

  private static final long serialVersionUID = 1L;
  @Column(name = "name")
  private String name;
  @Column(name = "created_date")
  private Date createdDate = new Date();
  @Column(name = "updated_date")
  private Date updatedDate;
  @Column(name = "status")
  private int status;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getUpdatedDate() {
    return updatedDate;
  }

  public void setUpdatedDate(Date updatedDate) {
    this.updatedDate = updatedDate;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }
}
