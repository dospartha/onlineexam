package com.dosinfotech.sacredheart.exams.domain;

import java.util.Date;
import javax.persistence.*;

/**
 * The persistent class for the notice database table.
 *
 */
@Entity
@Table(name = "notice")
@NamedQuery(name = "Notice.findAll", query = "SELECT n FROM Notice n")
public class Notice extends BaseDomain {

    private static final long serialVersionUID = 1L;

    @Column(name = "note")
    private String note;
    @Basic(optional = false)
    @Column(name = "created_date")
    @Temporal(TemporalType.DATE)
    private Date createdDate;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "status")
    private int status;

    public Notice() {
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
