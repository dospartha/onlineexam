package com.dosinfotech.sacredheart.exams.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Class_subject_mappings")
public class ClassSubjectMappings extends BaseDomain{

  private static final long serialVersionUID = 1L;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "class_id")
  private Classes classes;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "subject_id")
  private Subjects subject;

  @Column(name = "created_date")
  private Date createdDate = new Date();

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public Classes getClasses() {
    return classes;
  }

  public void setClasses(Classes classes) {
    this.classes = classes;
  }

  public Subjects getSubject() {
    return subject;
  }

  public void setSubject(Subjects subject) {
    this.subject = subject;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }
}
