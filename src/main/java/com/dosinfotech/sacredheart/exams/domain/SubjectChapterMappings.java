package com.dosinfotech.sacredheart.exams.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SubjectChapterMappings")
public class SubjectChapterMappings extends BaseDomain {

  private static final long serialVersionUID = 1L;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "subject_id")
  private Subjects subjects;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "chapter_id")
  private Chapters chapters;

  @Column(name = "created_date")
  private Date createdDate = new Date();

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public Subjects getSubjects() {
    return subjects;
  }

  public void setSubjects(Subjects subjects) {
    this.subjects = subjects;
  }

  public Chapters getChapters() {
    return chapters;
  }

  public void setChapters(Chapters chapters) {
    this.chapters = chapters;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }
}
