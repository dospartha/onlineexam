package com.dosinfotech.sacredheart.exams.domain;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

/**
 * The persistent class for the user database table.
 *
 */
@Entity
@Table(name = "candidates")
@NamedQuery(name = "Candidates.findAll", query = "SELECT c FROM Candidates c")
public class Candidates extends BaseDomain {

    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "module_id")
    private Modules modules;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "class_id")
    private Classes classes;

    @Column(name = "candidate_name")
    private String candidateName;
    @Column(name = "father_name")
    private String fatherName;
    @Column(name = "mother_name")
    private String mother_name;
    @Column(name = "dob")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Temporal(TemporalType.DATE)
    private Date dob;
    @Column(name = "nationality")
    private static final String nationality = "Indian";
    @Column(name = "permanent_address")
    private String permanentAddress;
    @Column(name = "permanent_city")
    private String permanentCity;
    @Column(name = "permanent_pinno")
    private String permanentPinno;
    @Column(name = "permanent_state")
    private String permanentState;
    @Column(name = "permanent_country")
    private String permanentCountry;
    @Column(name = "correspondence_address")
    private String correspondenceAddress;
    @Column(name = "correspondence_city")
    private String correspondenceCity;
    @Column(name = "correspondence_pinno")
    private String correspondencePinno;
    @Column(name = "correspondence_state")
    private String correspondenceState;
    @Column(name = "correspondence_country")
    private String correspondenceCountry;
    @Column(name = "candidate_phonenumber")
    private String candidatePhonenumber;
    @Column(name = "another_candidate_phonenumber")
    private String anotherCandidatePhonenumber;
    @Column(name = "candidate_email")
    private String candidateEmail;
    @Column(name = "father_occupation")
    private String fatherOccupation;
    @Column(name = "mother_occupation")
    private String motherOccupation;
    @Column(name = "father_contact_no")
    private String fatherContactNo;
    @Column(name = "mother_contact_no")
    private String motherContactNo;
    @Column(name = "gender")
    private String gender;
    @Column(name = "category")
    private String category;
    @Column(name = "image_name")
    private String imageName;
    @Column(name = "status")
    private int status;
    /*@Column(name = "candidate_username")
    private String candidateUsername;*/
    @Column(name = "candidate_password")
    private String candidatePassword;
    @Column(name = "created_by")
    private String createdBy;
    @Column(name = "created_date")
    private Date createdDate = new Date();
    @Column(name = "updated_by")
    private String updatedBy;
    @Column(name = "updated_date")
    private Date updatedDate;
    @Column(name = "internal_candidate", columnDefinition = "tinyint(1) default 0")
    private boolean internalCandidate = false;

    public Candidates() {
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Modules getModules() {
        return modules;
    }

    public void setModules(Modules modules) {
        this.modules = modules;
    }

    public Classes getClasses() {
        return classes;
    }

    public void setClasses(Classes classes) {
        this.classes = classes;
    }

    public boolean isInternalCandidate() {
        return internalCandidate;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMother_name() {
        return mother_name;
    }

    public void setMother_name(String mother_name) {
        this.mother_name = mother_name;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getNationality() {
        return nationality;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPermanentCity() {
        return permanentCity;
    }

    public void setPermanentCity(String permanentCity) {
        this.permanentCity = permanentCity;
    }

    public String getPermanentPinno() {
        return permanentPinno;
    }

    public void setPermanentPinno(String permanentPinno) {
        this.permanentPinno = permanentPinno;
    }

    public String getPermanentState() {
        return permanentState;
    }

    public void setPermanentState(String permanentState) {
        this.permanentState = permanentState;
    }

    public String getPermanentCountry() {
        return permanentCountry;
    }

    public void setPermanentCountry(String permanentCountry) {
        this.permanentCountry = permanentCountry;
    }

    public String getCorrespondenceAddress() {
        return correspondenceAddress;
    }

    public void setCorrespondenceAddress(String correspondenceAddress) {
        this.correspondenceAddress = correspondenceAddress;
    }

    public String getCorrespondenceCity() {
        return correspondenceCity;
    }

    public void setCorrespondenceCity(String correspondenceCity) {
        this.correspondenceCity = correspondenceCity;
    }

    public String getCorrespondencePinno() {
        return correspondencePinno;
    }

    public void setCorrespondencePinno(String correspondencePinno) {
        this.correspondencePinno = correspondencePinno;
    }

    public String getCorrespondenceState() {
        return correspondenceState;
    }

    public void setCorrespondenceState(String correspondenceState) {
        this.correspondenceState = correspondenceState;
    }

    public String getCorrespondenceCountry() {
        return correspondenceCountry;
    }

    public void setCorrespondenceCountry(String correspondenceCountry) {
        this.correspondenceCountry = correspondenceCountry;
    }

    public String getCandidatePhonenumber() {
        return candidatePhonenumber;
    }

    public void setCandidatePhonenumber(String candidatePhonenumber) {
        this.candidatePhonenumber = candidatePhonenumber;
    }

    public String getAnotherCandidatePhonenumber() {
        return anotherCandidatePhonenumber;
    }

    public void setAnotherCandidatePhonenumber(String anotherCandidatePhonenumber) {
        this.anotherCandidatePhonenumber = anotherCandidatePhonenumber;
    }

    public String getCandidateEmail() {
        return candidateEmail;
    }

    public void setCandidateEmail(String candidateEmail) {
        this.candidateEmail = candidateEmail;
    }

    public String getFatherOccupation() {
        return fatherOccupation;
    }

    public void setFatherOccupation(String fatherOccupation) {
        this.fatherOccupation = fatherOccupation;
    }

    public String getMotherOccupation() {
        return motherOccupation;
    }

    public void setMotherOccupation(String motherOccupation) {
        this.motherOccupation = motherOccupation;
    }

    public String getFatherContactNo() {
        return fatherContactNo;
    }

    public void setFatherContactNo(String fatherContactNo) {
        this.fatherContactNo = fatherContactNo;
    }

    public String getMotherContactNo() {
        return motherContactNo;
    }

    public void setMotherContactNo(String motherContactNo) {
        this.motherContactNo = motherContactNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    /*public String getCandidateUsername() {
        return candidateUsername;
    }

    public void setCandidateUsername(String candidateUsername) {
        this.candidateUsername = candidateUsername;
    }*/

    public String getCandidatePassword() {
        return candidatePassword;
    }

    public void setCandidatePassword(String candidatePassword) {
        this.candidatePassword = candidatePassword;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public boolean getInternalCandidate() {
        return internalCandidate;
    }

    public void setInternalCandidate(boolean internalCandidate) {
        this.internalCandidate = internalCandidate;
    }
}
