package com.dosinfotech.sacredheart.exams.domain;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "questions")
public class Questions extends BaseDomain{

  private static final long serialVersionUID = 1L;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "module_id")
  private Modules modules;
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "class_id")
  private Classes classes;
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "subject_id")
  private Subjects subjects;
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "chapter_id")
  private Chapters chapters;
  @Column(name = "qno")
  private int questionNo;
  @Column(name = "question")
  private String question;
  @Column(name = "opt1")
  private String opt1;
  @Column(name = "opt2")
  private String opt2;
  @Column(name = "opt3")
  private String opt3;
  @Column(name = "opt4")
  private String opt4;
  @Column(name = "ans")
  private String answer;
  @Column(name = "created_date")
  private Date createdDate = new Date();
  @Column(name = "update_date")
  private Date updateDate;
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "teacher_id")
  private Teachers teachers;
  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "admin_id")
  private Admin admin;

  @Column(name = "status")
  private int status;

  /*@Column(name = "teacher_id")
  private int teacherId;
  @Column(name = "admin_id")
  private int adminId;*/

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public Modules getModules() {
    return modules;
  }

  public void setModules(Modules modules) {
    this.modules = modules;
  }

  public Classes getClasses() {
    return classes;
  }

  public void setClasses(Classes classes) {
    this.classes = classes;
  }

  public Subjects getSubjects() {
    return subjects;
  }

  public void setSubjects(Subjects subjects) {
    this.subjects = subjects;
  }

  public Chapters getChapters() {
    return chapters;
  }

  public void setChapters(Chapters chapters) {
    this.chapters = chapters;
  }

  public int getQuestionNo() {
    return questionNo;
  }

  public void setQuestionNo(int questionNo) {
    this.questionNo = questionNo;
  }

  public String getQuestion() {
    return question;
  }

  public void setQuestion(String question) {
    this.question = question;
  }

  public String getOpt1() {
    return opt1;
  }

  public void setOpt1(String opt1) {
    this.opt1 = opt1;
  }

  public String getOpt2() {
    return opt2;
  }

  public void setOpt2(String opt2) {
    this.opt2 = opt2;
  }

  public String getOpt3() {
    return opt3;
  }

  public void setOpt3(String opt3) {
    this.opt3 = opt3;
  }

  public String getOpt4() {
    return opt4;
  }

  public void setOpt4(String opt4) {
    this.opt4 = opt4;
  }

  public String getAnswer() {
    return answer;
  }

  public void setAnswer(String answer) {
    this.answer = answer;
  }

  public Date getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Date createdDate) {
    this.createdDate = createdDate;
  }

  public Date getUpdateDate() {
    return updateDate;
  }

  public void setUpdateDate(Date updateDate) {
    this.updateDate = updateDate;
  }

  public Teachers getTeachers() {
    return teachers;
  }

  public void setTeachers(Teachers teachers) {
    this.teachers = teachers;
  }

  public Admin getAdmin() {
    return admin;
  }

  public void setAdmin(Admin admin) {
    this.admin = admin;
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }
}
