package com.dosinfotech.sacredheart.exams.business.impl;

import org.springframework.beans.factory.annotation.Autowired;
import com.dosinfotech.sacredheart.exams.business.TeacherService;
import com.dosinfotech.sacredheart.exams.util.DCEncryptDecrypt;
import javax.servlet.http.HttpSession;
import com.dosinfotech.sacredheart.exams.dao.TeacherDao;
import com.dosinfotech.sacredheart.exams.domain.Candidates;
import com.dosinfotech.sacredheart.exams.domain.Teachers;
import com.dosinfotech.sacredheart.exams.dto.CandidateLoginBean;
import com.dosinfotech.sacredheart.exams.dto.TeacherLoginBean;
import com.dosinfotech.sacredheart.exams.dto.TeacherRegistrationBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;

@Service
@Transactional
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    TeacherDao teacherDao;

    @Override
    public String teacherlogin(TeacherLoginBean teacherloginBean, HttpSession session, HttpServletRequest request) {
        String loginTeacher;
        DCEncryptDecrypt dCEncryptDecrypt = new DCEncryptDecrypt();
        Teachers teacher = teacherDao.teacherlogin(teacherloginBean.getTeacherLoginId(),
                dCEncryptDecrypt.encrypt(teacherloginBean.getTeacherLoginPassword()));
        if (teacher != null) {
            session.invalidate();
            loginTeacher = "SUCCESS";
            HttpSession newSession = request.getSession(true);
            HttpSession session1 = request.getSession(true);
            newSession.setAttribute("teacherUser", teacher);
            session1.setAttribute("teacherUserName", teacher.getTeacherUsername());
        } else {
            loginTeacher = "FAILURE";
        }
        return loginTeacher;
    }

    @Override
    public TeacherRegistrationBean getTeacherDetails(HttpSession session) {
        Teachers teacher = (Teachers) session.getAttribute("teacherUser");
        TeacherRegistrationBean bean = new TeacherRegistrationBean();
        bean.setId(teacher.getId());
        bean.setTeacherName(teacher.getTeacherName());
        bean.setTeacherEmail(teacher.getTeacherEmail());
        bean.setTeacherPhoneNumber(teacher.getTeacherPhoneNumber());
        bean.setAlternatePhoneNo(teacher.getAlternatePhoneNo());
        bean.setAlternatePhoneNo(teacher.getAlternatePhoneNo());
        bean.setPermanentAddress(teacher.getPermanentAddress());
        bean.setPermanentCity(teacher.getPermanentCity());
        bean.setPermanentPinno(teacher.getPermanentPinno());
        bean.setPermanentState(teacher.getPermanentState());
        bean.setPermanentCountry(teacher.getPermanentCountry());
        bean.setCorrespondenceAddress(teacher.getCorrespondenceAddress());
        bean.setCorrespondenceCity(teacher.getCorrespondenceCity());
        bean.setCorrespondencePinno(teacher.getCorrespondencePinno());
        bean.setCorrespondenceState(teacher.getCorrespondenceState());
        bean.setCorrespondenceCountry(teacher.getCorrespondenceCountry());
        bean.setStatus(teacher.getStatus());
        bean.setImageName(teacher.getImageName());
        bean.setTeacherUsername(teacher.getTeacherUsername());
        bean.setTeacherPassword(teacher.getTeacherPassword());
        bean.setCreatedBy(teacher.getCreatedBy());
        bean.setCreatedDate(teacher.getCreatedDate());
        bean.setUpdatedBy(teacher.getUpdatedBy());
        bean.setUpdatedDate(teacher.getUpdatedDate());
        bean.setInternalTeacher(teacher.getInternalTeacher());
        return bean;
    }

    @Override
    @Transactional
    public String teacherpasschange(TeacherLoginBean teacherChangePasswordBean, HttpSession session) {
        String status = "ERROR";
        Teachers teacher = (Teachers) session.getAttribute("teacherUser");
        DCEncryptDecrypt dCEncryptDecrypt = new DCEncryptDecrypt();
        if (teacher.getTeacherPassword().equals(dCEncryptDecrypt.encrypt(teacherChangePasswordBean.getTeacherOldPassword()))) {
            if (teacherChangePasswordBean.getTeacherNewPassword().equals(teacherChangePasswordBean.getTeacherNewConfirmPassword())) {
                teacher.setTeacherPassword(dCEncryptDecrypt.encrypt(teacherChangePasswordBean.getTeacherNewPassword()));
                teacherDao.save(teacher);
                status = "SUCCESS";
            } else {
                status = "New Password Mismatched";
            }
        } else {
            status = "Current Password Does Not Match";
        }
        return status;
    }

}
