package com.dosinfotech.sacredheart.exams.business.impl;

import com.dosinfotech.sacredheart.exams.dao.*;
import com.dosinfotech.sacredheart.exams.domain.*;
import com.dosinfotech.sacredheart.exams.dto.QuestionsBean;
import com.dosinfotech.sacredheart.exams.dto.CandidateDetailsBean;
import org.springframework.beans.factory.annotation.Autowired;
import com.dosinfotech.sacredheart.exams.business.AdminService;
import com.dosinfotech.sacredheart.exams.util.DCEncryptDecrypt;

import javax.servlet.http.HttpSession;

import com.dosinfotech.sacredheart.exams.dto.AdminLoginBean;
import com.dosinfotech.sacredheart.exams.dto.AdminRegistrationBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.List;

@Service
@Transactional
public class AdminServiceImpl implements AdminService {

    @Autowired
    AdminDao adminDao;
    @Autowired
    ModuleDao moduleDao;
    @Autowired
    ClassesDao classesDao;
    @Autowired
    SubjectDao subjectDao;
    @Autowired
    ChapterDao chapterDao;
    @Autowired
    CandidateDao candidateDao;
    @Autowired
    CandidateChequeDetailsDao candidateChequeDetailsDao;
    @Autowired
    CandidatePaymentDetailsDao candidatePaymentDetailsDao;
    @Autowired
    ModuleClassMappingDao moduleClassMappingDao;
    @Autowired
    ClassSubjectMappingsDao classSubjectMappingsDao;
    @Autowired
    SubjectChapterMappingsDao subjectChapterMappingsDao;
    @Autowired
    QuestionsDao questionsDao;

    @Override
    public String adminlogin(AdminLoginBean adminloginBean, HttpSession session, HttpServletRequest request) {
        String loginAdmin;
        DCEncryptDecrypt dCEncryptDecrypt = new DCEncryptDecrypt();
        Admin admin = adminDao.adminlogin(adminloginBean.getAdminLoginId(),
                dCEncryptDecrypt.encrypt(adminloginBean.getAdminLoginPassword()));
        if (admin != null) {
            session.invalidate();
            loginAdmin = "SUCCESS";
            HttpSession newSession = request.getSession(true);
            HttpSession session1 = request.getSession(true);
            newSession.setAttribute("adminUser", admin);
            session1.setAttribute("adminUserName", admin.getUsername());
        } else {
            loginAdmin = "FAILURE";
        }
        return loginAdmin;
    }

    @Override
    public AdminRegistrationBean getAdminDetails(HttpSession session) {
        Admin admin = (Admin) session.getAttribute("adminUser");
        AdminRegistrationBean bean = new AdminRegistrationBean();
        bean.setId(admin.getId());
        bean.setName(admin.getName());
        bean.setPhoneNo(admin.getPhoneNo());
        bean.setUsername(admin.getUsername());
        bean.setPassword(admin.getPassword());
        bean.setRole(admin.getRole());
        bean.setStatus(admin.getStatus());
        bean.setCreatedBy(admin.getCreatedBy());
        bean.setCreatedDate(admin.getCreatedDate());
        bean.setUpdatedBy(admin.getUpdatedBy());
        bean.setUpdatedDate(admin.getUpdatedDate());
        return bean;
    }

    @Override
    @Transactional
    public String adminpasschange(AdminLoginBean adminChangePasswordBean, HttpSession session) {
        String status = "ERROR";
        Admin admin = (Admin) session.getAttribute("adminUser");
        DCEncryptDecrypt dCEncryptDecrypt = new DCEncryptDecrypt();
        if (admin.getPassword().equals(dCEncryptDecrypt.encrypt(adminChangePasswordBean.getAdminOldPassword()))) {
            if (adminChangePasswordBean.getAdminNewPassword().equals(adminChangePasswordBean.getAdminNewConfirmPassword())) {
                admin.setPassword(dCEncryptDecrypt.encrypt(adminChangePasswordBean.getAdminNewPassword()));
                adminDao.save(admin);
                status = "SUCCESS";
            } else {
                status = "New Password Mismatched";
            }
        } else {
            status = "Current Password Does Not Match";
        }
        return status;
    }

    @Override
    public String studentRegistration(CandidateDetailsBean candidateDetailsBean) {
        String status = "ERROR";
        Candidates candidates = new Candidates();
        DCEncryptDecrypt dcEncrypt = new DCEncryptDecrypt();
        CandidateChequeDetails candidateChequeDetails = new CandidateChequeDetails();
        CandidatePaymentDetails candidatePaymentDetails= new CandidatePaymentDetails();
        candidates.setClasses(classesDao.loadById(candidateDetailsBean.getClassId()));
        candidates.setModules(moduleDao.loadById(candidateDetailsBean.getModuleId()));
        candidates.setCandidateName(candidateDetailsBean.getCandidateName());
        candidates.setCandidateEmail(candidateDetailsBean.getCandidateEmail());
        candidates.setCandidatePhonenumber(candidateDetailsBean.getCandidatePhoneNumber());
        candidates.setDob(candidateDetailsBean.getDob());
        candidates.setGender(candidateDetailsBean.getGender());
        Calendar calendar = Calendar.getInstance();
        String password = String.valueOf(calendar.get(Calendar.DATE)< 10 ? ("0"+ calendar.get(Calendar.DATE)):calendar.get(Calendar.DATE));
        if (calendar.get(Calendar.MONTH)<9){
            password += "0";
        }
        password = password + (calendar.get(Calendar.MONTH)+1);
        password = password + calendar.get(Calendar.YEAR);
        System.out.println("password = " + password);

        candidates.setCandidatePassword(dcEncrypt.encrypt(password));
        candidates.setStatus(1);
        candidates = candidateDao.save(candidates);
        if (candidates!= null){
            candidatePaymentDetails.setCandidates(candidates);
        }
        if (!candidateDetailsBean.getChequeNo().isEmpty()){
            candidateChequeDetails.setChequeNo(candidateDetailsBean.getChequeNo());
            candidateChequeDetails.setBankBranchName(candidateDetailsBean.getBankBranch());
            candidateChequeDetails.setBankName(candidateDetailsBean.getBankName());
            candidateChequeDetails.setChequeStatus(0);
            candidateChequeDetails = candidateChequeDetailsDao.save(candidateChequeDetails);

            if (candidateChequeDetails!= null){
                candidatePaymentDetails.setCandidateChequeDetails(candidateChequeDetails);
            }
        }
        candidatePaymentDetails.setAmount(candidateDetailsBean.getNetAmount());
        candidatePaymentDetails.setTaxAmount(candidateDetailsBean.getGst());
        candidatePaymentDetails = candidatePaymentDetailsDao.save(candidatePaymentDetails);
        if (candidatePaymentDetails!=null){
            status = "SUCCESS";
        }
        return status;
    }

    @Override
    public List<Modules> getAllModules() {
        return moduleDao.loadAll();
    }

    @Override
    public List<ModuleClassMappings> getAllClassesByModule(long moduleId) {
        List<ModuleClassMappings> classesList = moduleClassMappingDao.getAllClassesByModuleId(moduleId);
        return classesList;
    }

    @Override
    public List<ClassSubjectMappings> getAllSubjectsByClassId(long classId) {
        List<ClassSubjectMappings> subjectList = classSubjectMappingsDao.getAllSubjectsByClassId(classId);
        return subjectList;
    }

    @Override
    public List<SubjectChapterMappings> getAllChaptersBySubjectId(long subjectId) {
        return subjectChapterMappingsDao.getAllChaptersBySubjectId(subjectId);
    }

    @Override
    public int getQuestionNumber(QuestionsBean questionBean) {
        long moduleId = questionBean.getModuleId();
        long classId = questionBean.getClassId();
        long subjectId = questionBean.getSubjectId();
        long chapterId = questionBean.getChapterId();
        Object[] questions = questionsDao.getQuestionNumber(moduleId, classId, subjectId, chapterId);
        if (questions != null) {
            return Integer.parseInt(questions[5].toString());
        } else {
            return 0;
        }
    }

    @Override
    public Questions addQuestion(QuestionsBean questionBean) {
        Questions questions = new Questions();
        Modules modules = moduleDao.loadById(questionBean.getModuleId());
        questions.setModules(modules);
        Classes classes = classesDao.loadById(questionBean.getClassId());
        questions.setClasses(classes);
        Subjects subjects = subjectDao.loadById(questionBean.getSubjectId());
        questions.setSubjects(subjects);
        Chapters chapters = chapterDao.loadById(questionBean.getChapterId());
        questions.setChapters(chapters);
        questions.setQuestionNo(questionBean.getQuestionNo());
        questions.setQuestion(questionBean.getQuestion());
        questions.setOpt1(questionBean.getOption1());
        questions.setOpt2(questionBean.getOption2());
        questions.setOpt3(questionBean.getOption3());
        questions.setOpt4(questionBean.getOption4());
        questions.setAnswer(questionBean.getAnswer());
//        questions.setAdminId(1);
        questions.setStatus(1);
        questions = questionsDao.save(questions);
        return questions;
    }

}
