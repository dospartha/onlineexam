package com.dosinfotech.sacredheart.exams.business;

import com.dosinfotech.sacredheart.exams.domain.Classes;
import com.dosinfotech.sacredheart.exams.dto.CandidateLoginBean;
import com.dosinfotech.sacredheart.exams.dto.CandidateRegistrationBean;
import com.dosinfotech.sacredheart.exams.dto.FeedbackBean;
import com.dosinfotech.sacredheart.exams.dto.NoticeBean;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public interface HomeService {

    List<Classes> getAllClasses();

    String candidateRegistration(CandidateRegistrationBean candidateRegistrationBean);

    String checkCandidateEmailExistOrNot(String candidateEmail);

    String checkStudentUserNameExistOrNot(String candidateUsername);

    String candidateLogin(CandidateLoginBean candidateloginBean, HttpSession session, HttpServletRequest request);

    String feedbackSend(FeedbackBean feedbackBean);

    List<NoticeBean> getNoticeDetails();

}
