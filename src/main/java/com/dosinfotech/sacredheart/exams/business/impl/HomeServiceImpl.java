package com.dosinfotech.sacredheart.exams.business.impl;

import com.dosinfotech.sacredheart.exams.dao.*;
import com.dosinfotech.sacredheart.exams.domain.*;
import com.dosinfotech.sacredheart.exams.dto.CandidateRegistrationBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.dosinfotech.sacredheart.exams.business.HomeService;
import com.dosinfotech.sacredheart.exams.dto.CandidateLoginBean;
import com.dosinfotech.sacredheart.exams.dto.FeedbackBean;
import com.dosinfotech.sacredheart.exams.dto.NoticeBean;
import com.dosinfotech.sacredheart.exams.util.DCEncryptDecrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Service
@Transactional
public class HomeServiceImpl implements HomeService {

    @Autowired
    ModuleDao moduleDao;
    @Autowired
    ClassesDao classesDao;
    @Autowired
    CandidateDao candidateDao;
    @Autowired
    NoticeDao noticeDao;
    @Autowired
    FeedbackDao feedbackDao;

    @Override
    public List<Classes> getAllClasses() {
        return classesDao.loadAll();
    }

    @Override
    @Transactional
    public String candidateRegistration(CandidateRegistrationBean candidateRegistrationBean) {
        String registerCandidate = null;
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        DCEncryptDecrypt dcEncrypt = new DCEncryptDecrypt();
        Candidates candidates = new Candidates();
        Modules modules = moduleDao.loadById(candidateRegistrationBean.getModuleId());
        candidates.setModules(modules);
        Classes classes = classesDao.loadById(candidateRegistrationBean.getClassId());
        candidates.setClasses(classes);
        candidates.setCandidateName(candidateRegistrationBean.getCandidateName());
//        candidates.setCandidateUsername(candidateRegistrationBean.getCandidateUsername());
        String strDate= formatter.format(candidateRegistrationBean.getDob());
        System.out.println("strDate = " + strDate);
        String[] dob = strDate.split("/");
        String password = "";
        for (String s : dob) {
            password+=s;
        }
        System.out.println("password = " + password);

        candidates.setCandidatePassword(dcEncrypt.encrypt(password));
        candidates.setCandidateEmail(candidateRegistrationBean.getCandidateEmail());
        candidates.setCandidatePhonenumber(candidateRegistrationBean.getCandidatePhonenumber());
        candidates.setGender(candidateRegistrationBean.getGender());
        candidates.setDob(candidateRegistrationBean.getDob());
        candidates.setStatus(1);

        Candidates candidateSave = candidateDao.save(candidates);
        if (candidateSave != null) {
            registerCandidate = "SUCCESS";
        }
        return registerCandidate;
    }

    @Override
    public String checkCandidateEmailExistOrNot(String candidateEmail) {
        Candidates candidateExist = candidateDao.checkCandidateEmailExistOrNot(candidateEmail);
        if (candidateExist != null) {
            return "SUCCESS";
        } else {
            return "FAILURE";
        }
    }

    @Override
    public String checkStudentUserNameExistOrNot(String candidateUsername) {
        Candidates candidateExist = candidateDao.checkStudentUserNameExistOrNot(candidateUsername);
        if (candidateExist != null) {
            return "SUCCESS";
        } else {
            return "FAILURE";
        }
    }

    @Override
    public String candidateLogin(CandidateLoginBean candidateloginBean, HttpSession session, HttpServletRequest request) {
        String loginCandidate;
        DCEncryptDecrypt dCEncryptDecrypt = new DCEncryptDecrypt();
        Candidates candidate = candidateDao.candidatelogin(candidateloginBean.getCandidateLoginId(),
                dCEncryptDecrypt.encrypt(candidateloginBean.getCandidateLoginPassword()));
        if (candidate != null) {
            session.invalidate();
            loginCandidate = "SUCCESS";
            HttpSession newSession = request.getSession(true);
            HttpSession session1 = request.getSession(true);
            newSession.setAttribute("candidateUser", candidate);
            session1.setAttribute("candidateEmail", candidate.getCandidateEmail());
        } else {
            loginCandidate = "FAILURE";
        }
        return loginCandidate;
    }

    @Override
    @Transactional
    public String feedbackSend(FeedbackBean feedbackBean) {
        String status = "Try Later";
        Feedback feedback = new Feedback();
        feedback.setFbName(feedbackBean.getFbName());
        feedback.setFbEmail(feedbackBean.getFbEmail());
        feedback.setFbNumber(feedbackBean.getFbNumber());
        feedback.setFbComment(feedbackBean.getFbComment());
        //SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
        Date date = new Date();
        feedback.setFbDate(date);
        Feedback feedbacksave = feedbackDao.save(feedback);
        if (feedbacksave != null) {
            status = "SUCCESS";
        }
        return status;
    }

    @Override
    public List<NoticeBean> getNoticeDetails() {
        List<NoticeBean> beans = new ArrayList<NoticeBean>();
        List<Notice> notices = noticeDao.loadAll();
        for (Notice notice : notices) {
            NoticeBean noticeBean = new NoticeBean();
            noticeBean.setId(notice.getId());
            noticeBean.setNote(notice.getNote());
            noticeBean.setCreatedDate(notice.getCreatedDate());
            noticeBean.setStatus(notice.getStatus());
            beans.add(noticeBean);
        }
        return beans;
    }

}
