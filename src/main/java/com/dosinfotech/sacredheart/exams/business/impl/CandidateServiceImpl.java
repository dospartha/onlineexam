package com.dosinfotech.sacredheart.exams.business.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.dosinfotech.sacredheart.exams.business.CandidateService;
import com.dosinfotech.sacredheart.exams.dao.CandidateDao;
import com.dosinfotech.sacredheart.exams.dao.NoticeDao;
import com.dosinfotech.sacredheart.exams.domain.Candidates;
import com.dosinfotech.sacredheart.exams.dto.CandidateLoginBean;
import com.dosinfotech.sacredheart.exams.dto.CandidateRegistrationBean;
import com.dosinfotech.sacredheart.exams.util.DCEncryptDecrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;

@Service
@Transactional
public class CandidateServiceImpl implements CandidateService {

    @Autowired
    CandidateDao candidateDao;
    @Autowired
    NoticeDao noticeDao;

    @Override
    @Transactional
    public String candidatepasschange(CandidateLoginBean candidateChangePasswordBean, HttpSession session) {
        String status = "ERROR";
        Candidates candidate = (Candidates) session.getAttribute("candidateUser");
        DCEncryptDecrypt dCEncryptDecrypt = new DCEncryptDecrypt();
        if (candidate.getCandidatePassword().equals(dCEncryptDecrypt.encrypt(candidateChangePasswordBean.getCandidateOldPassword()))) {
            if (candidateChangePasswordBean.getCandidateNewPassword().equals(candidateChangePasswordBean.getCandidateNewConfirmPassword())) {
                candidate.setCandidatePassword(dCEncryptDecrypt.encrypt(candidateChangePasswordBean.getCandidateNewPassword()));
                candidateDao.save(candidate);
                status = "SUCCESS";
            } else {
                status = "New Password Mismatched";
            }
        } else {
            status = "Current Password Does Not Match";
        }
        return status;
    }

    @Override
    public CandidateRegistrationBean getCandidateDetails(HttpSession session) {
        Candidates candidate = (Candidates) session.getAttribute("candidateUser");
        CandidateRegistrationBean bean = new CandidateRegistrationBean();
        bean.setId(candidate.getId());
        bean.setCandidateName(candidate.getCandidateName());
//        bean.setCandidateUsername(candidate.getCandidateUsername());
        bean.setCandidatePassword(candidate.getCandidatePassword());
        bean.setCandidateEmail(candidate.getCandidateEmail());
        bean.setCandidatePhonenumber(candidate.getCandidatePhonenumber());
        bean.setStatus(candidate.getStatus());
        return bean;
    }

}
