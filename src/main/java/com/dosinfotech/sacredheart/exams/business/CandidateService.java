package com.dosinfotech.sacredheart.exams.business;

import com.dosinfotech.sacredheart.exams.dto.CandidateLoginBean;
import com.dosinfotech.sacredheart.exams.dto.CandidateRegistrationBean;
import javax.servlet.http.HttpSession;

public interface CandidateService {

    public String candidatepasschange(CandidateLoginBean candidateChangePasswordBean, HttpSession session);

    public CandidateRegistrationBean getCandidateDetails(HttpSession session);

}
