package com.dosinfotech.sacredheart.exams.business;

import com.dosinfotech.sacredheart.exams.dto.TeacherLoginBean;
import com.dosinfotech.sacredheart.exams.dto.TeacherRegistrationBean;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public interface TeacherService {

    public String teacherlogin(TeacherLoginBean teacherloginBean, HttpSession session, HttpServletRequest request);

    public TeacherRegistrationBean getTeacherDetails(HttpSession session);

    public String teacherpasschange(TeacherLoginBean teacherChangePasswordBean, HttpSession session);

}
