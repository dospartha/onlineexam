package com.dosinfotech.sacredheart.exams.business;

import com.dosinfotech.sacredheart.exams.domain.*;
import com.dosinfotech.sacredheart.exams.dto.AdminLoginBean;
import com.dosinfotech.sacredheart.exams.dto.AdminRegistrationBean;
import com.dosinfotech.sacredheart.exams.dto.QuestionsBean;
import com.dosinfotech.sacredheart.exams.dto.CandidateDetailsBean;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

public interface AdminService {

    public String adminlogin(AdminLoginBean adminloginBean, HttpSession session, HttpServletRequest request);

    public AdminRegistrationBean getAdminDetails(HttpSession session);

    public String adminpasschange(AdminLoginBean adminChangePasswordBean, HttpSession session);

    String studentRegistration(CandidateDetailsBean candidateDetailsBean);

    List<Modules> getAllModules();

    List<ModuleClassMappings> getAllClassesByModule(long moduleId);

    List<ClassSubjectMappings> getAllSubjectsByClassId(long classId);

    List<SubjectChapterMappings> getAllChaptersBySubjectId(long subjectId);

    int getQuestionNumber(QuestionsBean questionBean);

    Questions addQuestion(QuestionsBean questionBean);
}

