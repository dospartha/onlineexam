package com.dosinfotech.sacredheart.exams.controller;

import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.dosinfotech.sacredheart.exams.business.HomeService;
import com.dosinfotech.sacredheart.exams.business.TeacherService;
import com.dosinfotech.sacredheart.exams.dto.NoticeBean;
import com.dosinfotech.sacredheart.exams.dto.TeacherLoginBean;
import com.dosinfotech.sacredheart.exams.dto.TeacherRegistrationBean;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Handles requests for the teacher home page.
 */
@CrossOrigin
@Controller
@RequestMapping(value = "/teacher/**")
public class TeacherController {

    @Autowired
    HomeService homeService;
    @Autowired
    TeacherService teacherService;

    @RequestMapping("/")
    public String home() {
        return "teacher/index";
    }

    @RequestMapping(value = "/teacherlogin", method = RequestMethod.POST)
    @ResponseBody
    public String teacherlogin(@RequestBody TeacherLoginBean teacherloginBean, HttpSession session, HttpServletRequest request) {

        return teacherService.teacherlogin(teacherloginBean, session, request);
    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String dashboard(ModelMap model, HttpSession session) {

        return "teacher/teacherdashboard";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String Logout(HttpSession session, HttpServletResponse response) {
        session.invalidate();
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP
        // 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0);
        return "redirect:/teacher";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(ModelMap model, HttpSession session) {
        TeacherRegistrationBean teacherDetails = teacherService.getTeacherDetails(session);
        model.addAttribute("teacherdetails", teacherDetails);
        return "teacher/profile";
    }

    @RequestMapping(value = "/notice", method = RequestMethod.GET)
    public String notice(ModelMap model, HttpSession session) {
        List<NoticeBean> noticeBeans = homeService.getNoticeDetails();
        model.addAttribute("noticebeans", noticeBeans);
        return "teacher/notice";
    }

    @RequestMapping(value = "/changepassword", method = RequestMethod.GET)
    public String changepassword(ModelMap model, HttpSession session) {

        return "teacher/changepassword";
    }

    @RequestMapping(value = "/teacherpasschange", method = RequestMethod.POST)
    @ResponseBody
    private String teacherpasschange(@RequestBody TeacherLoginBean teacherChangePasswordBean, HttpSession session) {
        return teacherService.teacherpasschange(teacherChangePasswordBean, session);
    }

    @RequestMapping(value = "/addquestions", method = RequestMethod.GET)
    public String addquestions(ModelMap model, HttpSession session) {

        return "teacher/addquestions";
    }

}
