package com.dosinfotech.sacredheart.exams.controller;

import javax.servlet.http.HttpSession;

import com.dosinfotech.sacredheart.exams.domain.*;
import com.dosinfotech.sacredheart.exams.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.dosinfotech.sacredheart.exams.business.HomeService;
import com.dosinfotech.sacredheart.exams.business.AdminService;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.ui.ModelMap;

/**
 * Handles requests for the admin home page.
 */
@CrossOrigin
@Controller
@RequestMapping(value = "admin")
public class AdminController {

    @Autowired
    HomeService homeService;
    @Autowired
    AdminService adminService;

    @RequestMapping()
    public String home() {
        return "admin/index";
    }

    @RequestMapping(value = "adminlogin", method = RequestMethod.POST)
    @ResponseBody
    public String adminlogin(@RequestBody AdminLoginBean adminloginBean, HttpSession session, HttpServletRequest request) {

        return adminService.adminlogin(adminloginBean, session, request);
    }

    @RequestMapping(value = "dashboard", method = RequestMethod.GET)
    public String dashboard(ModelMap model, HttpSession session) {

        return "admin/admindashboard";
    }

    @RequestMapping(value = "logout", method = RequestMethod.GET)
    public String Logout(HttpSession session, HttpServletResponse response) {
        session.invalidate();
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP
        // 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0);
        return "/admin";
    }

    @RequestMapping(value = "profile", method = RequestMethod.GET)
    public String profile(ModelMap model, HttpSession session) {
        AdminRegistrationBean adminDetails = adminService.getAdminDetails(session);
        model.addAttribute("admindetails", adminDetails);
        return "admin/profile";
    }

    @RequestMapping(value = "studentRegistration",method = RequestMethod.GET)
    public String registration(ModelMap modelMap) {
        modelMap.addAttribute("classes",homeService.getAllClasses());
        modelMap.addAttribute("modules",adminService.getAllModules());
        return "admin/studentregistration";
    }

    @RequestMapping(value = "studentRegistration", method = RequestMethod.POST)
    @ResponseBody
    public String studentRegistration(@RequestBody CandidateDetailsBean candidateDetailsBean) {
        return adminService.studentRegistration(candidateDetailsBean);
    }

    @RequestMapping(value = "notice", method = RequestMethod.GET)
    public String notice(ModelMap model, HttpSession session) {
        List<NoticeBean> noticeBeans = homeService.getNoticeDetails();
        model.addAttribute("noticebeans", noticeBeans);
        return "admin/notice";
    }

    @RequestMapping(value = "changepassword", method = RequestMethod.GET)
    public String changepassword(ModelMap model, HttpSession session) {

        return "admin/changepassword";
    }

    @RequestMapping(value = "adminpasschange", method = RequestMethod.POST)
    @ResponseBody
    private String adminpasschange(@RequestBody AdminLoginBean adminChangePasswordBean, HttpSession session) {
        return adminService.adminpasschange(adminChangePasswordBean, session);
    }

    @RequestMapping(value = "addquestions", method = RequestMethod.GET)
    public String addquestions(ModelMap modelMap, HttpSession session) {
        modelMap.addAttribute("modules",adminService.getAllModules());
        return "admin/addquestions";
    }

    @RequestMapping(value = "getAllClassesByModule", method = RequestMethod.GET)
    @ResponseBody
    public List<ModuleClassMappings> getAllClassesByModule(@RequestParam(required = true) long moduleId) {
        return adminService.getAllClassesByModule(moduleId);
    }

    @RequestMapping(value = "getAllSubjectsByClassId", method = RequestMethod.GET)
    @ResponseBody
    public List<ClassSubjectMappings> getAllSubjectsByClassId(@RequestParam(required = true) long classId) {
        return adminService.getAllSubjectsByClassId(classId);
    }

    @RequestMapping(value = "getAllChaptersBySubjectId", method = RequestMethod.GET)
    @ResponseBody
    public List<SubjectChapterMappings> getAllChaptersBySubjectId(@RequestParam(required = true) long subjectId) {
        return adminService.getAllChaptersBySubjectId(subjectId);
    }

    @RequestMapping(value = "getQuestionNumber", method = RequestMethod.POST)
    public @ResponseBody
    int getQuestionNumber(@RequestBody QuestionsBean questionBean) {

        return adminService.getQuestionNumber(questionBean);
    }

    @RequestMapping(value = "addQuestion", method = RequestMethod.POST)
    public @ResponseBody
    Questions addQuestion(@RequestBody QuestionsBean questionBean, ModelMap model, HttpSession session) {
        Questions questions=adminService.addQuestion(questionBean);
        if(questions!=null){
            return questions;
        }else{
            return new Questions();
        }
    }
}
