package com.dosinfotech.sacredheart.exams.controller;

import javax.servlet.http.HttpSession;

import com.dosinfotech.sacredheart.exams.business.AdminService;
import com.dosinfotech.sacredheart.exams.dto.CandidateRegistrationBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.dosinfotech.sacredheart.exams.business.HomeService;
import com.dosinfotech.sacredheart.exams.dto.CandidateLoginBean;
import javax.servlet.http.HttpServletRequest;

/**
 * Handles requests for the application home page.
 */
@CrossOrigin
@Controller
public class HomeController {

    @Autowired
    HomeService homeService;
    @Autowired
    AdminService adminService;

    @RequestMapping("/")
    public String home() {
        return "index";
    }

    @RequestMapping("registration")
    public String registration(ModelMap modelMap) {
        modelMap.addAttribute("classes",homeService.getAllClasses());
        modelMap.addAttribute("modules",adminService.getAllModules());
        return "registration";
    }

    @RequestMapping("forgotPassword")
    public String forgotPassword() {
        return "forgotPassword";
    }

    @RequestMapping(value = "/candidateregistration", method = RequestMethod.POST)
    public @ResponseBody
    String candidateRegistration(@RequestBody CandidateRegistrationBean candidateRegistrationBean, ModelMap model, HttpSession session) {
        return homeService.candidateRegistration(candidateRegistrationBean);
    }

    @ResponseBody
    @RequestMapping(value = "/checkStudentUserNameExistOrNot", method = RequestMethod.POST)
    public String checkStudentUserNameExistOrNot(@RequestParam String candidateUsername) {
        return homeService.checkStudentUserNameExistOrNot(candidateUsername);
    }

    @ResponseBody
    @RequestMapping(value = "/checkCandidateEmailExistOrNot", method = RequestMethod.POST)
    public String checkCandidateEmailExistOrNot(@RequestParam String candidateEmail) {
        return homeService.checkCandidateEmailExistOrNot(candidateEmail);
    }

    @RequestMapping(value = "/candidatelogin", method = RequestMethod.POST)
    @ResponseBody
    public String candidateLogin(@RequestBody CandidateLoginBean candidateloginBean, HttpSession session, HttpServletRequest request) {
        return homeService.candidateLogin(candidateloginBean, session, request);
    }

}
