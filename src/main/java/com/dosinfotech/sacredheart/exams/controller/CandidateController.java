package com.dosinfotech.sacredheart.exams.controller;

import com.dosinfotech.sacredheart.exams.business.CandidateService;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.dosinfotech.sacredheart.exams.business.HomeService;
import com.dosinfotech.sacredheart.exams.dto.CandidateLoginBean;
import com.dosinfotech.sacredheart.exams.dto.CandidateRegistrationBean;
import com.dosinfotech.sacredheart.exams.dto.FeedbackBean;
import com.dosinfotech.sacredheart.exams.dto.NoticeBean;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Handles requests for the candidate home page.
 */
@CrossOrigin
@Controller
@RequestMapping(value = "/candidate/**")
public class CandidateController {

    @Autowired
    HomeService homeService;
    @Autowired
    CandidateService candidateService;

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String dashboard(ModelMap model, HttpSession session) {

        return "candidate/candidatedashboard";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(ModelMap model, HttpSession session) {
        CandidateRegistrationBean candidateDetails = candidateService.getCandidateDetails(session);
         model.addAttribute("candidatedetails", candidateDetails);
        return "candidate/profile";
    }

    @RequestMapping(value = "/subjects", method = RequestMethod.GET)
    public String subjects(ModelMap model, HttpSession session) {

        return "candidate/subjects";
    }

    @RequestMapping(value = "/results", method = RequestMethod.GET)
    public String results(ModelMap model, HttpSession session) {

        return "candidate/results";
    }

    @RequestMapping(value = "/exams", method = RequestMethod.GET)
    public String exams(ModelMap model, HttpSession session) {

        return "candidate/exams";
    }

    @RequestMapping(value = "/notice", method = RequestMethod.GET)
    public String notice(ModelMap model, HttpSession session) {
        List<NoticeBean> noticeBeans = homeService.getNoticeDetails();
        model.addAttribute("noticebeans", noticeBeans);
        return "candidate/notice";
    }

    @RequestMapping(value = "/changepassword", method = RequestMethod.GET)
    public String changepassword(ModelMap model, HttpSession session) {

        return "candidate/changepassword";
    }

    @RequestMapping(value = "/candidatepasschange", method = RequestMethod.POST)
    @ResponseBody
    private String candidatepasschange(@RequestBody CandidateLoginBean candidateChangePasswordBean, HttpSession session) {
        return candidateService.candidatepasschange(candidateChangePasswordBean, session);
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String Logout(HttpSession session, HttpServletResponse response) {
        session.invalidate();
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP
        // 1.1.
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        response.setDateHeader("Expires", 0);
        return "redirect:/";
    }

    @RequestMapping(value = "/upcomingevents", method = RequestMethod.GET)
    public String upcomingevents(ModelMap model, HttpSession session) {

        return "candidate/upcomingevents";
    }

    @RequestMapping(value = "/feedbackSend", method = RequestMethod.POST)
    public @ResponseBody
    String feedbackSend(@RequestBody FeedbackBean feedbackBean, ModelMap model, HttpSession session) {
        return homeService.feedbackSend(feedbackBean);
    }

}
