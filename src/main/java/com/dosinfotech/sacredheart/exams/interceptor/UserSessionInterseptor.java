/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dosinfotech.sacredheart.exams.interceptor;

//import in.myproject.test.dto.Status;
//import com.dosinfotech.sacredheart.exams.domain.User;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 *
 * @author @sayan saha
 */
public class UserSessionInterseptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        HttpSession session = request.getSession();
        // User user = (User) session.getAttribute("superUser");

        //if (user == null) {
        String context = request.getContextPath();
        response.sendRedirect(context + "/login");
        return false;

        //} else {
        // return true;
        //}
    }
}
