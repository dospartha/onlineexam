package com.dosinfotech.sacredheart.exams;

import java.util.Calendar;
import java.util.Date;

/**
 * @author Partha Sarothi Banerjee
 */
public class Test {
    public static void main(String[] args) {
        Date date = new Date();
        System.out.println("date = " + date);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        String password = String.valueOf(calendar.get(Calendar.DATE)< 10 ? ("0"+ calendar.get(Calendar.DATE)):calendar.get(Calendar.DATE));
        /*if (calendar.get(Calendar.DATE)<10){
            password = "0"+ calendar.get(Calendar.DATE);
        }else {
            password += calendar.get(Calendar.DATE);
        }*/
        if (calendar.get(Calendar.MONTH)<9){
            password+="0";
        }
        password = password + (calendar.get(Calendar.MONTH)+1);
        password = password + calendar.get(Calendar.YEAR);
        System.out.println("password = " + password);
        System.out.println("calendar Date = " + calendar.get(Calendar.DATE));
        System.out.println("calendar Month = " + calendar.get(Calendar.MONTH));
        System.out.println("calendar Year = " + calendar.get(Calendar.YEAR));
    }
}
