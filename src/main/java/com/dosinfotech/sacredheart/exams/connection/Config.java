/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dosinfotech.sacredheart.exams.connection;

/**
 *
 * @author aqfaridi
 */
import java.sql.*;

public class Config {

    Connection con = null;
    String url = "jdbc:mysql://localhost:3306/onlineexam";
    String username = "root";
    String password = "";

    public Connection getcon() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url, username, password);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return con;
    }

}
