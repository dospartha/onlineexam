/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dosinfotech.sacredheart.exams.dto;

/**
 *
 * @author Monoj
 */
public class CandidateLoginBean {

    private long id;
    private String candidateLoginId;
    private String candidateLoginPassword;
    private String candidateOldPassword;
    private String candidateNewPassword;
    private String candidateNewConfirmPassword;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCandidateLoginId() {
        return candidateLoginId;
    }

    public void setCandidateLoginId(String candidateLoginId) {
        this.candidateLoginId = candidateLoginId;
    }

    public String getCandidateLoginPassword() {
        return candidateLoginPassword;
    }

    public void setCandidateLoginPassword(String candidateLoginPassword) {
        this.candidateLoginPassword = candidateLoginPassword;
    }

    public String getCandidateOldPassword() {
        return candidateOldPassword;
    }

    public void setCandidateOldPassword(String candidateOldPassword) {
        this.candidateOldPassword = candidateOldPassword;
    }

    public String getCandidateNewPassword() {
        return candidateNewPassword;
    }

    public void setCandidateNewPassword(String candidateNewPassword) {
        this.candidateNewPassword = candidateNewPassword;
    }

    public String getCandidateNewConfirmPassword() {
        return candidateNewConfirmPassword;
    }

    public void setCandidateNewConfirmPassword(String candidateNewConfirmPassword) {
        this.candidateNewConfirmPassword = candidateNewConfirmPassword;
    }

}
