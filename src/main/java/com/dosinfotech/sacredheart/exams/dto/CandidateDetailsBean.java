package com.dosinfotech.sacredheart.exams.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author Partha Sarothi Banerjee
 */
public class CandidateDetailsBean {

    private long classId;
    private long moduleId;
    private String candidateName;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dob;
    private String candidatePhoneNumber;
    private String candidateEmail;
    private String gender;

    private double registrationFee;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date paymentDate;
    private String paymentModeName;

    private String chequeNo;
    private String bankName;
    private String bankBranch;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date chequeIssueDate;

    private double  netAmount;
    private double  gst;
    private double grossAmount;

    public long getClassId() {
        return classId;
    }

    public void setClassId(long classId) {
        this.classId = classId;
    }

    public long getModuleId() {
        return moduleId;
    }

    public void setModuleId(long moduleId) {
        this.moduleId = moduleId;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getCandidateEmail() {
        return candidateEmail;
    }

    public void setCandidateEmail(String candidateEmail) {
        this.candidateEmail = candidateEmail;
    }

    public String getCandidatePhoneNumber() {
        return candidatePhoneNumber;
    }

    public void setCandidatePhoneNumber(String candidatePhoneNumber) {
        this.candidatePhoneNumber = candidatePhoneNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPaymentModeName() {
        return paymentModeName;
    }

    public void setPaymentModeName(String paymentModeName) {
        this.paymentModeName = paymentModeName;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public Date getChequeIssueDate() {
        return chequeIssueDate;
    }

    public void setChequeIssueDate(Date chequeIssueDate) {
        this.chequeIssueDate = chequeIssueDate;
    }

    public double getRegistrationFee() {
        return registrationFee;
    }

    public void setRegistrationFee(double registrationFee) {
        this.registrationFee = registrationFee;
    }

    public double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(double netAmount) {
        this.netAmount = netAmount;
    }

    public double getGst() {
        return gst;
    }

    public void setGst(double gst) {
        this.gst = gst;
    }

    public double getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(double grossAmount) {
        this.grossAmount = grossAmount;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }
}
