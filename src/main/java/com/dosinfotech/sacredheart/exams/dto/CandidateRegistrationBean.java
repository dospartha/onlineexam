/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dosinfotech.sacredheart.exams.dto;

import java.util.Date;

/**
 *
 * @author Monoj
 */
public class CandidateRegistrationBean {

    private long id;
    private long moduleId;
    private long classId;
    private String candidateName;
    private String fatherName;
    private String motherName;
    private Date dob;
    private String nationality;
    private String permanentAddress;
    private String permanentCity;
    private String permanentPinno;
    private String permanentState;
    private String permanentCountry;
    private String correspondenceAddress;
    private String correspondenceCity;
    private String correspondencePinno;
    private String correspondenceState;
    private String correspondenceCountry;
    private String candidatePhonenumber;
    private String anotherCandidatePhonenumber;
    private String candidateEmail;
    private String fatherOccupation;
    private String motherOccupation;
    private String fatherContactNo;
    private String motherContactNo;
    private String gender;
    private String category;
    private String imageName;
    private int status;
//    private String candidateUsername;
    private String candidatePassword;
    private String createdBy;
    private Date createdDate;
    private String updatedBy;
    private Date updatedDate;
    private int internalCandidate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getModuleId() {
        return moduleId;
    }

    public void setModuleId(long moduleId) {
        this.moduleId = moduleId;
    }

    public long getClassId() {
        return classId;
    }

    public void setClassId(long classId) {
        this.classId = classId;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getMotherName() {
        return motherName;
    }

    public void setMotherName(String motherName) {
        this.motherName = motherName;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getPermanentAddress() {
        return permanentAddress;
    }

    public void setPermanentAddress(String permanentAddress) {
        this.permanentAddress = permanentAddress;
    }

    public String getPermanentCity() {
        return permanentCity;
    }

    public void setPermanentCity(String permanentCity) {
        this.permanentCity = permanentCity;
    }

    public String getPermanentPinno() {
        return permanentPinno;
    }

    public void setPermanentPinno(String permanentPinno) {
        this.permanentPinno = permanentPinno;
    }

    public String getPermanentState() {
        return permanentState;
    }

    public void setPermanentState(String permanentState) {
        this.permanentState = permanentState;
    }

    public String getPermanentCountry() {
        return permanentCountry;
    }

    public void setPermanentCountry(String permanentCountry) {
        this.permanentCountry = permanentCountry;
    }

    public String getCorrespondenceAddress() {
        return correspondenceAddress;
    }

    public void setCorrespondenceAddress(String correspondenceAddress) {
        this.correspondenceAddress = correspondenceAddress;
    }

    public String getCorrespondenceCity() {
        return correspondenceCity;
    }

    public void setCorrespondenceCity(String correspondenceCity) {
        this.correspondenceCity = correspondenceCity;
    }

    public String getCorrespondencePinno() {
        return correspondencePinno;
    }

    public void setCorrespondencePinno(String correspondencePinno) {
        this.correspondencePinno = correspondencePinno;
    }

    public String getCorrespondenceState() {
        return correspondenceState;
    }

    public void setCorrespondenceState(String correspondenceState) {
        this.correspondenceState = correspondenceState;
    }

    public String getCorrespondenceCountry() {
        return correspondenceCountry;
    }

    public void setCorrespondenceCountry(String correspondenceCountry) {
        this.correspondenceCountry = correspondenceCountry;
    }

    public String getCandidatePhonenumber() {
        return candidatePhonenumber;
    }

    public void setCandidatePhonenumber(String candidatePhonenumber) {
        this.candidatePhonenumber = candidatePhonenumber;
    }

    public String getAnotherCandidatePhonenumber() {
        return anotherCandidatePhonenumber;
    }

    public void setAnotherCandidatePhonenumber(String anotherCandidatePhonenumber) {
        this.anotherCandidatePhonenumber = anotherCandidatePhonenumber;
    }

    public String getCandidateEmail() {
        return candidateEmail;
    }

    public void setCandidateEmail(String candidateEmail) {
        this.candidateEmail = candidateEmail;
    }

    public String getFatherOccupation() {
        return fatherOccupation;
    }

    public void setFatherOccupation(String fatherOccupation) {
        this.fatherOccupation = fatherOccupation;
    }

    public String getMotherOccupation() {
        return motherOccupation;
    }

    public void setMotherOccupation(String motherOccupation) {
        this.motherOccupation = motherOccupation;
    }

    public String getFatherContactNo() {
        return fatherContactNo;
    }

    public void setFatherContactNo(String fatherContactNo) {
        this.fatherContactNo = fatherContactNo;
    }

    public String getMotherContactNo() {
        return motherContactNo;
    }

    public void setMotherContactNo(String motherContactNo) {
        this.motherContactNo = motherContactNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    /*public String getCandidateUsername() {
        return candidateUsername;
    }

    public void setCandidateUsername(String candidateUsername) {
        this.candidateUsername = candidateUsername;
    }*/

    public String getCandidatePassword() {
        return candidatePassword;
    }

    public void setCandidatePassword(String candidatePassword) {
        this.candidatePassword = candidatePassword;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getInternalCandidate() {
        return internalCandidate;
    }

    public void setInternalCandidate(int internalCandidate) {
        this.internalCandidate = internalCandidate;
    }

}
