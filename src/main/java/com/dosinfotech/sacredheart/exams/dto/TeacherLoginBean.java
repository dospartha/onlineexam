/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dosinfotech.sacredheart.exams.dto;

/**
 *
 * @author Monoj
 */
public class TeacherLoginBean {

    private long id;
    private String teacherLoginId;
    private String teacherLoginPassword;
    private String teacherOldPassword;
    private String teacherNewPassword;
    private String teacherNewConfirmPassword;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTeacherLoginId() {
        return teacherLoginId;
    }

    public void setTeacherLoginId(String teacherLoginId) {
        this.teacherLoginId = teacherLoginId;
    }

    public String getTeacherLoginPassword() {
        return teacherLoginPassword;
    }

    public void setTeacherLoginPassword(String teacherLoginPassword) {
        this.teacherLoginPassword = teacherLoginPassword;
    }

    public String getTeacherOldPassword() {
        return teacherOldPassword;
    }

    public void setTeacherOldPassword(String teacherOldPassword) {
        this.teacherOldPassword = teacherOldPassword;
    }

    public String getTeacherNewPassword() {
        return teacherNewPassword;
    }

    public void setTeacherNewPassword(String teacherNewPassword) {
        this.teacherNewPassword = teacherNewPassword;
    }

    public String getTeacherNewConfirmPassword() {
        return teacherNewConfirmPassword;
    }

    public void setTeacherNewConfirmPassword(String teacherNewConfirmPassword) {
        this.teacherNewConfirmPassword = teacherNewConfirmPassword;
    }

}
