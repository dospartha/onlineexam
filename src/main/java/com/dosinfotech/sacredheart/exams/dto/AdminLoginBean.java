/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dosinfotech.sacredheart.exams.dto;

/**
 *
 * @author Monoj
 */
public class AdminLoginBean {

    private long id;
    private String adminLoginId;
    private String adminLoginPassword;
    private String adminOldPassword;
    private String adminNewPassword;
    private String adminNewConfirmPassword;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAdminLoginId() {
        return adminLoginId;
    }

    public void setAdminLoginId(String adminLoginId) {
        this.adminLoginId = adminLoginId;
    }

    public String getAdminLoginPassword() {
        return adminLoginPassword;
    }

    public void setAdminLoginPassword(String adminLoginPassword) {
        this.adminLoginPassword = adminLoginPassword;
    }

    public String getAdminOldPassword() {
        return adminOldPassword;
    }

    public void setAdminOldPassword(String adminOldPassword) {
        this.adminOldPassword = adminOldPassword;
    }

    public String getAdminNewPassword() {
        return adminNewPassword;
    }

    public void setAdminNewPassword(String adminNewPassword) {
        this.adminNewPassword = adminNewPassword;
    }

    public String getAdminNewConfirmPassword() {
        return adminNewConfirmPassword;
    }

    public void setAdminNewConfirmPassword(String adminNewConfirmPassword) {
        this.adminNewConfirmPassword = adminNewConfirmPassword;
    }

}
