/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dosinfotech.sacredheart.exams.dto;

import java.util.Date;

/**
 *
 * @author Monoj
 */
public class FeedbackBean {

    private long id;
    private String fbName;
    private String fbEmail;
    private String fbNumber;
    private String fbComment;
    private Date fbDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFbName() {
        return fbName;
    }

    public void setFbName(String fbName) {
        this.fbName = fbName;
    }

    public String getFbEmail() {
        return fbEmail;
    }

    public void setFbEmail(String fbEmail) {
        this.fbEmail = fbEmail;
    }

    public String getFbNumber() {
        return fbNumber;
    }

    public void setFbNumber(String fbNumber) {
        this.fbNumber = fbNumber;
    }

    public String getFbComment() {
        return fbComment;
    }

    public void setFbComment(String fbComment) {
        this.fbComment = fbComment;
    }

    public Date getFbDate() {
        return fbDate;
    }

    public void setFbDate(Date fbDate) {
        this.fbDate = fbDate;
    }

}
