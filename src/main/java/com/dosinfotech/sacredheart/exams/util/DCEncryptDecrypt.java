package com.dosinfotech.sacredheart.exams.util;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Ganesh,Sayan DA
 */
public class DCEncryptDecrypt {

    String key = "Bar12345Bar12345";

    public String encrypt(String text) {
        try {
            Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            // encrypt the text
            cipher.init(Cipher.ENCRYPT_MODE, aesKey);
            byte[] encrypted = cipher.doFinal(text.getBytes());
            System.err.println("encrypted==" + new String(encrypted));
            return new String(encrypted);
        } catch (NoSuchAlgorithmException  ex) {
//            Logger.getLogger(DCEncryptDecrypt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        catch(NoSuchPaddingException ex)
        {
        	return null;
        }
        catch(InvalidKeyException ex){
        	return null;
        }
        catch(  IllegalBlockSizeException ex){
        	return null;
        }
        catch(BadPaddingException ex){
        	return null;
        }
    }

    public String decript(String text) {
        try {
            Key aesKey = new SecretKeySpec(key.getBytes(), "AES");
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, aesKey);
            byte[] encrypted = cipher.doFinal(text.getBytes());
            String decrypted = new String(cipher.doFinal(encrypted));
            System.err.println(decrypted);
            return decrypted;
        } catch (NoSuchAlgorithmException  ex) {
//          Logger.getLogger(DCEncryptDecrypt.class.getName()).log(Level.SEVERE, null, ex);
          return null;
      }
      catch(NoSuchPaddingException ex)
      {
      	return null;
      }
      catch(InvalidKeyException ex){
      	return null;
      }
      catch(  IllegalBlockSizeException ex){
      	return null;
      }
      catch(BadPaddingException ex){
      	return null;
      }
    }

    static Cipher cipher;

    public DCEncryptDecrypt() {
        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(128);
            SecretKey secretKey = keyGenerator.generateKey();
            cipher = Cipher.getInstance("AES");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(DCEncryptDecrypt.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NoSuchPaddingException ex) {
            Logger.getLogger(DCEncryptDecrypt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String encrypt(String plainText, SecretKey secretKey)
            throws Exception {
        byte[] plainTextByte = plainText.getBytes();
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] encryptedByte = cipher.doFinal(plainTextByte);
        Base64.Encoder encoder = Base64.getEncoder();
        String encryptedText = encoder.encodeToString(encryptedByte);
        return encryptedText;
    }

    public static String decrypt(String encryptedText, SecretKey secretKey)
            throws Exception {
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] encryptedTextByte = decoder.decode(encryptedText);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] decryptedByte = cipher.doFinal(encryptedTextByte);
        String decryptedText = new String(decryptedByte);
        return decryptedText;
    }

}
