-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 03, 2018 at 10:27 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 5.6.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shca_exam`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone_no` varchar(50) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` date DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `phone_no`, `email_id`, `username`, `password`, `role`, `status`, `created_date`, `created_by`, `updated_by`, `updated_date`) VALUES
(1, 'Monoj Majumder', '9874296556', 'dos.monoj@gmail.com', 'dosmonoj', 'vDeNN;', 'Admin', 1, '2018-10-23', NULL, NULL, NULL),
(2, 'Appi Majumdar', '8918724214', 'appimajumdar@yahoo.com', 'appimajumdar', 'Îr!øürð¬R¿<\"@M>¤', 'User', 1, '2018-10-23', 'dosmonoj', 'dosmonoj', NULL),
(3, 'Partha Sarothi', '78727242554', 'parthasarothidos@gmail.com', 'partha', 'vDeNN;', 'Admin', 1, '2018-11-13', 'partha', 'partha', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `candidates`
--

CREATE TABLE `candidates` (
  `id` int(10) NOT NULL,
  `candidate_name` varchar(255) NOT NULL,
  `father_name` varchar(255) DEFAULT NULL,
  `mother_name` varchar(255) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `nationality` varchar(50) DEFAULT NULL,
  `permanent_address` text,
  `permanent_city` varchar(50) DEFAULT NULL,
  `permanent_pinno` varchar(50) DEFAULT NULL,
  `permanent_state` varchar(50) DEFAULT NULL,
  `permanent_country` varchar(50) DEFAULT NULL,
  `correspondence_address` text,
  `correspondence_city` varchar(50) DEFAULT NULL,
  `correspondence_pinno` varchar(50) DEFAULT NULL,
  `correspondence_state` varchar(50) DEFAULT NULL,
  `correspondence_country` varchar(50) DEFAULT NULL,
  `candidate_phonenumber` varchar(20) NOT NULL,
  `another_candidate_phonenumber` varchar(20) DEFAULT NULL,
  `candidate_email` varchar(50) DEFAULT NULL,
  `father_occupation` varchar(50) DEFAULT NULL,
  `mother_occupation` varchar(50) DEFAULT NULL,
  `father_contact_no` varchar(20) DEFAULT NULL,
  `mother_contact_no` varchar(20) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `category` varchar(50) DEFAULT NULL,
  `image_name` varchar(255) DEFAULT NULL,
  `status` tinyint(5) NOT NULL DEFAULT '1',
  `candidate_password` varchar(255) NOT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `created_date` date NOT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `internal_candidate` tinyint(1) NOT NULL DEFAULT '0',
  `class_id` bigint(20) DEFAULT NULL,
  `module_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidates`
--

INSERT INTO `candidates` (`id`, `candidate_name`, `father_name`, `mother_name`, `dob`, `nationality`, `permanent_address`, `permanent_city`, `permanent_pinno`, `permanent_state`, `permanent_country`, `correspondence_address`, `correspondence_city`, `correspondence_pinno`, `correspondence_state`, `correspondence_country`, `candidate_phonenumber`, `another_candidate_phonenumber`, `candidate_email`, `father_occupation`, `mother_occupation`, `father_contact_no`, `mother_contact_no`, `gender`, `category`, `image_name`, `status`, `candidate_password`, `created_by`, `created_date`, `updated_by`, `updated_date`, `internal_candidate`, `class_id`, `module_id`) VALUES
(11, 'Partha Sarothi Banerjee', NULL, NULL, '2000-01-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7872724254', NULL, 'parthasarothidos@gmail.com', NULL, NULL, NULL, NULL, 'male', NULL, NULL, 1, 'S`/RG_/^', NULL, '2018-11-13', NULL, NULL, 0, 3, 2),
(20, 'Partha', NULL, NULL, '2000-01-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7873124589', NULL, 'fdgfd@gmail.com', NULL, NULL, NULL, NULL, 'male', NULL, NULL, 1, '?\'o:U\"s', NULL, '2018-11-22', NULL, NULL, 0, 2, 2),
(21, 'Partha Sarothi', NULL, NULL, '2000-01-08', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '7873124555', NULL, 'par@gmail.com', NULL, NULL, NULL, NULL, 'male', NULL, NULL, 1, 'rB;\'6\r<', NULL, '2018-12-03', NULL, NULL, 0, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `candidate_cheque_details`
--

CREATE TABLE `candidate_cheque_details` (
  `id` int(10) NOT NULL,
  `bank_name` varchar(255) DEFAULT NULL,
  `bank_branch_name` varchar(255) DEFAULT NULL,
  `cheque_no` varchar(50) DEFAULT NULL,
  `cheque_date` date DEFAULT NULL,
  `cheque_status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidate_cheque_details`
--

INSERT INTO `candidate_cheque_details` (`id`, `bank_name`, `bank_branch_name`, `cheque_no`, `cheque_date`, `cheque_status`) VALUES
(1, 'UBI', 'GS ROAD BRANCH GUWAHATI', '301701', '2018-10-25', 1),
(2, 'meghalaya coop apex bank', 'nongthymmai', '269719', '2018-10-25', 1),
(3, 'STATE BANK OF INDIA', 'MALKI', '892680', '2018-10-25', 1),
(4, 'SBI', 'SECTT BRANCH', '363697', '2018-10-25', 1),
(5, 'MEGHALAYA RURAL BANK', 'SHILLONG', '496635', '2018-10-25', 1),
(7, 'Axis Bank', 'Laitumkhrah', '029320', '2018-10-25', 1),
(8, 'SBI', 'Kenches Trace (Shillong)', '031940', '2018-10-25', 1),
(9, 'HDFC BANK', 'SHANGPUNG-MOOLIBANG', '000094', '2018-10-25', 1),
(10, 'sbi', 'rynjah bazar', '427958', '2018-10-25', 0),
(11, 'SBI', 'NONGMYNSONG', '128572', '2018-10-25', 0),
(12, 'Axis', 'Saltlake', '3', '2018-12-03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `candidate_payment_details`
--

CREATE TABLE `candidate_payment_details` (
  `id` int(10) NOT NULL,
  `invoice_id` varchar(255) DEFAULT NULL,
  `payment_for` varchar(255) DEFAULT NULL,
  `amount` double NOT NULL,
  `tax_amount` double NOT NULL,
  `payment_date` date NOT NULL,
  `due_amount` double NOT NULL DEFAULT '0',
  `due_date` date DEFAULT NULL,
  `discrepancy_amount` double NOT NULL DEFAULT '0',
  `extra_amount` double NOT NULL DEFAULT '0',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `cheque_id` bigint(20) DEFAULT NULL,
  `package_id` bigint(20) DEFAULT NULL,
  `candidate_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `candidate_payment_details`
--

INSERT INTO `candidate_payment_details` (`id`, `invoice_id`, `payment_for`, `amount`, `tax_amount`, `payment_date`, `due_amount`, `due_date`, `discrepancy_amount`, `extra_amount`, `create_date`, `update_date`, `status`, `cheque_id`, `package_id`, `candidate_id`) VALUES
(8, NULL, NULL, 100, 18, '2018-12-03', 0, NULL, 0, 0, '2018-12-03 09:23:09', NULL, 0, 12, NULL, 21);

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_date` date DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `name`, `created_date`, `update_date`, `status`, `updated_date`) VALUES
(1, 'UNITS AND MEASUREMENTS', '2018-10-23', NULL, 1, NULL),
(2, 'MOTION IN A STRAIGHT LINE', '2018-10-23', NULL, 1, NULL),
(3, 'MOTION IN A PLANE', '2018-10-23', NULL, 1, NULL),
(4, 'LAWS OF MOTION', '2018-10-23', NULL, 1, NULL),
(5, 'WORK, ENERGY AND POWER', '2018-10-23', NULL, 1, NULL),
(6, 'SYSTEM OF PARTICLES AND ROTATIONAL MOTION', '2018-10-23', NULL, 1, NULL),
(7, 'GRAVITATION', '2018-10-23', NULL, 1, NULL),
(8, 'MECHANICAL PROPERTIES OF SOLIDS', '2018-10-23', NULL, 1, NULL),
(9, 'MECHANICAL PROPERTIES OF FLUIDS', '2018-10-23', NULL, 1, NULL),
(10, 'THERMAL PROPERTIES OF MATTER', '2018-10-23', NULL, 1, NULL),
(11, 'THERMODYNAMICS', '2018-10-23', NULL, 1, NULL),
(12, 'KINETIC THEORY OF GASES', '2018-10-23', NULL, 1, NULL),
(13, 'OSCILLATIONS', '2018-10-23', NULL, 1, NULL),
(14, 'WAVES', '2018-10-23', NULL, 1, NULL),
(15, 'ELECTROSTATICS I', '2018-10-23', NULL, 1, NULL),
(16, 'ELECTROSTATICS II (CAPACITANCE)', '2018-10-23', NULL, 1, NULL),
(17, 'CURRENT ELECTRICITY', '2018-10-23', NULL, 1, NULL),
(18, 'MOVING CHARGES AND MAGNETISM', '2018-10-23', NULL, 1, NULL),
(19, 'MAGNETISM AND MATTER', '2018-10-23', NULL, 1, NULL),
(20, 'ELECTROMAGNETIC INDUCTION', '2018-10-23', NULL, 1, NULL),
(21, 'ALTERNATING CURRENT', '2018-10-23', NULL, 1, NULL),
(22, 'ELECTROMAGNETIC WAVES', '2018-10-23', NULL, 1, NULL),
(23, 'RAY OPTICS AND OPTICAL INSTRUMENTS', '2018-10-23', NULL, 1, NULL),
(24, 'WAVE OPTICS', '2018-10-23', NULL, 1, NULL),
(25, 'DUAL NATURE OF MATTER AND RADIATION', '2018-10-23', NULL, 1, NULL),
(26, 'ATOMS AND NUCLEI', '2018-10-23', NULL, 1, NULL),
(27, 'SEMICONDUCTORS ELECTRONICS: MATERIALS DEVICES AND SIMPLE CIRCUIT', '2018-10-23', NULL, 1, NULL),
(28, 'COMMUNICATION SYSTEM', '2018-10-23', NULL, 1, NULL),
(29, 'OTHERS', '2018-10-23', NULL, 1, NULL),
(30, 'Principles of Inheritance & Variation', '2018-10-30', NULL, 1, NULL),
(31, 'Biological Classification', '2018-10-30', NULL, 1, NULL),
(32, 'Basics of Organic Chemistry', '2018-10-30', NULL, 1, NULL),
(33, 'Coordination Compounds', '2018-10-30', NULL, 1, NULL),
(34, 'Equilibrium', NULL, NULL, 1, NULL),
(35, 'Human Reproduction', '2018-10-30', NULL, 1, NULL),
(36, 'Chemical Coordination & Integration', '2018-10-30', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_date` date DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `name`, `created_date`, `update_date`, `status`) VALUES
(1, 'XI', '2018-10-23', NULL, 1),
(2, 'XII', '2018-10-23', NULL, 1),
(3, 'XII PASSED', '2018-10-23', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `class_subject_mappings`
--

CREATE TABLE `class_subject_mappings` (
  `id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class_subject_mappings`
--

INSERT INTO `class_subject_mappings` (`id`, `class_id`, `subject_id`, `created_date`) VALUES
(1, 1, 1, '2018-10-23'),
(2, 1, 2, '2018-10-23'),
(3, 1, 3, '2018-10-23'),
(4, 1, 4, '2018-10-23'),
(5, 2, 1, '2018-10-23'),
(6, 2, 2, '2018-10-23'),
(7, 2, 3, '2018-10-23'),
(8, 2, 4, '2018-10-23'),
(9, 3, 1, '2018-10-23'),
(10, 3, 2, '2018-10-23'),
(11, 3, 3, '2018-10-23'),
(12, 3, 4, '2018-10-23');

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `id` int(11) NOT NULL,
  `exam_code` varchar(255) NOT NULL,
  `start_datetime` datetime NOT NULL,
  `end_datetime` datetime NOT NULL,
  `instruction` text,
  `correct_ans` int(10) NOT NULL DEFAULT '1',
  `wrong_ans` int(10) NOT NULL DEFAULT '0',
  `no_of_question` int(10) NOT NULL DEFAULT '0',
  `subject_id_combine` int(10) NOT NULL DEFAULT '0',
  `module_id` int(10) NOT NULL,
  `class_id` int(10) NOT NULL,
  `exam_type_id` int(10) NOT NULL,
  `chapter_id` int(10) NOT NULL DEFAULT '0',
  `subject_id` int(10) NOT NULL DEFAULT '0',
  `created_by` varchar(256) NOT NULL,
  `created_date` date DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `exam_code`, `start_datetime`, `end_datetime`, `instruction`, `correct_ans`, `wrong_ans`, `no_of_question`, `subject_id_combine`, `module_id`, `class_id`, `exam_type_id`, `chapter_id`, `subject_id`, `created_by`, `created_date`, `updated_date`, `updated_by`) VALUES
(1, 'XI-PHYSICS-UNITS AND MEASUREMENTS', '2018-10-30 03:30:00', '2018-10-30 04:00:00', '<p>&nbsp;</p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px; text-align: center;\"><strong>&nbsp;&nbsp;</strong><span style=\"text-decoration: underline;\"><strong> RULES FOR PARTICIPATION&nbsp; </strong><br /></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">There will be negative marking.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">+4 for correct answer</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">-1 for wrong answer</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">1.</span><span style=\"color: black; mso-themecolor: text1;\"> Only Individual Participation is allowed (No Teams).</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">2.</span><span style=\"color: black; mso-themecolor: text1;\"> The event will be held online as mentioned in the timeline.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp;&nbsp;&nbsp; </span>So, all the participants are requested to register on the Examshow website</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp;&nbsp;&nbsp; </span><a href=\"http://examshow.codeshare.in\">Register Here</a> for participation.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">3.</span><span style=\"color: black; mso-themecolor: text1;\"> The top two Participants will be awarded the goodies.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">4.</span><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp; </span>Only students who are studying in any collage or school eligible to take part.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">5.</span><span style=\"color: black; mso-themecolor: text1;\"> If any team(s) is found posting answers in any forum will be immediately disqualified without any prior notice.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">6.</span><span style=\"color: black; mso-themecolor: text1;\"> Any team found having fake information regarding qualification criteria or profile information shall be immediately disqualified.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">7.</span><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp; </span>Any team which attempts to hack/deliberately crash the judging system will be immediately disqualified.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">8.</span><span style=\"color: black; mso-themecolor: text1;\"> The decision of the organizers in declaring the results will be final and will be binding to all. No queries in this regard will be entertained</span></strong></span><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">.</span></strong></p>', 4, 1, 30, 0, 1, 1, 1, 1, 1, 'admin', '2018-10-23', NULL, NULL),
(2, 'XI-PHYSICS-MOTION IN A STRAIGHT LINE', '2018-10-31 04:00:00', '2018-10-31 05:00:00', '<p>&nbsp;</p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px; text-align: center;\"><strong>&nbsp;&nbsp;</strong><span style=\"text-decoration: underline;\"><strong> RULES FOR PARTICIPATION&nbsp; </strong><br /></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">There will be negative marking.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">+4 for correct answer</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">-1 for wrong answer</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">1.</span><span style=\"color: black; mso-themecolor: text1;\"> Only Individual Participation is allowed (No Teams).</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">2.</span><span style=\"color: black; mso-themecolor: text1;\"> The event will be held online as mentioned in the timeline.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp;&nbsp;&nbsp; </span>So, all the participants are requested to register on the Examshow website</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp;&nbsp;&nbsp; </span><a href=\"http://examshow.codeshare.in\">Register Here</a> for participation.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">3.</span><span style=\"color: black; mso-themecolor: text1;\"> The top two Participants will be awarded the goodies.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">4.</span><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp; </span>Only students who are studying in any collage or school eligible to take part.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">5.</span><span style=\"color: black; mso-themecolor: text1;\"> If any team(s) is found posting answers in any forum will be immediately disqualified without any prior notice.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">6.</span><span style=\"color: black; mso-themecolor: text1;\"> Any team found having fake information regarding qualification criteria or profile information shall be immediately disqualified.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">7.</span><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp; </span>Any team which attempts to hack/deliberately crash the judging system will be immediately disqualified.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">8.</span><span style=\"color: black; mso-themecolor: text1;\"> The decision of the organizers in declaring the results will be final and will be binding to all. No queries in this regard will be entertained</span></strong></span><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">.</span></strong></p>', 4, 1, 60, 0, 1, 1, 1, 2, 1, 'admin', '2018-10-23', NULL, NULL),
(5, 'XI-PHYSICS', '2018-10-31 04:00:00', '2018-10-31 05:00:00', '<p>&nbsp;</p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px; text-align: center;\"><strong>&nbsp;&nbsp;</strong><span style=\"text-decoration: underline;\"><strong> RULES FOR PARTICIPATION&nbsp; </strong><br /></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">There will be negative marking.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">+4 for correct answer</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">-1 for wrong answer</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">1.</span><span style=\"color: black; mso-themecolor: text1;\"> Only Individual Participation is allowed (No Teams).</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">2.</span><span style=\"color: black; mso-themecolor: text1;\"> The event will be held online as mentioned in the timeline.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp;&nbsp;&nbsp; </span>So, all the participants are requested to register on the Examshow website</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp;&nbsp;&nbsp; </span><a href=\"http://examshow.codeshare.in\">Register Here</a> for participation.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">3.</span><span style=\"color: black; mso-themecolor: text1;\"> The top two Participants will be awarded the goodies.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">4.</span><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp; </span>Only students who are studying in any collage or school eligible to take part.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">5.</span><span style=\"color: black; mso-themecolor: text1;\"> If any team(s) is found posting answers in any forum will be immediately disqualified without any prior notice.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">6.</span><span style=\"color: black; mso-themecolor: text1;\"> Any team found having fake information regarding qualification criteria or profile information shall be immediately disqualified.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">7.</span><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp; </span>Any team which attempts to hack/deliberately crash the judging system will be immediately disqualified.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">8.</span><span style=\"color: black; mso-themecolor: text1;\"> The decision of the organizers in declaring the results will be final and will be binding to all. No queries in this regard will be entertained</span></strong></span><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">.</span></strong></p>', 4, 1, 60, 0, 1, 1, 2, 0, 1, 'admin', '2018-10-23', NULL, NULL),
(6, 'XI-Full Mock Test', '2018-10-30 08:00:00', '2018-10-30 10:00:00', '<p>&nbsp;</p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px; text-align: center;\"><strong>&nbsp;&nbsp;</strong><span style=\"text-decoration: underline;\"><strong> RULES FOR PARTICIPATION&nbsp; </strong><br /></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">There will be negative marking.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">+4 for correct answer</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">-1 for wrong answer</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">1.</span><span style=\"color: black; mso-themecolor: text1;\"> Only Individual Participation is allowed (No Teams).</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">2.</span><span style=\"color: black; mso-themecolor: text1;\"> The event will be held online as mentioned in the timeline.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp;&nbsp;&nbsp; </span>So, all the participants are requested to register on the Examshow website</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp;&nbsp;&nbsp; </span><a href=\"http://examshow.codeshare.in\">Register Here</a> for participation.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">3.</span><span style=\"color: black; mso-themecolor: text1;\"> The top two Participants will be awarded the goodies.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">4.</span><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp; </span>Only students who are studying in any collage or school eligible to take part.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">5.</span><span style=\"color: black; mso-themecolor: text1;\"> If any team(s) is found posting answers in any forum will be immediately disqualified without any prior notice.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">6.</span><span style=\"color: black; mso-themecolor: text1;\"> Any team found having fake information regarding qualification criteria or profile information shall be immediately disqualified.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">7.</span><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp; </span>Any team which attempts to hack/deliberately crash the judging system will be immediately disqualified.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">8.</span><span style=\"color: black; mso-themecolor: text1;\"> The decision of the organizers in declaring the results will be final and will be binding to all. No queries in this regard will be entertained</span></strong></span><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">.</span></strong></p>', 4, 1, 100, 1, 1, 1, 3, 0, 0, 'admin', '2018-10-23', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exam_multiple_subject_mappings`
--

CREATE TABLE `exam_multiple_subject_mappings` (
  `id` int(10) NOT NULL,
  `exam_id` int(10) NOT NULL,
  `subject_id` int(10) NOT NULL,
  `no_of_question` int(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_multiple_subject_mappings`
--

INSERT INTO `exam_multiple_subject_mappings` (`id`, `exam_id`, `subject_id`, `no_of_question`) VALUES
(1, 6, 1, 40),
(2, 6, 2, 30),
(3, 6, 3, 30);

-- --------------------------------------------------------

--
-- Table structure for table `exam_packages`
--

CREATE TABLE `exam_packages` (
  `id` int(10) NOT NULL,
  `package_name` varchar(255) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `no_of_exam` int(10) NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `discrepancy_amount` double NOT NULL DEFAULT '0' COMMENT 'for internal candidate'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_packages`
--

INSERT INTO `exam_packages` (`id`, `package_name`, `created_date`, `update_date`, `status`, `no_of_exam`, `amount`, `discrepancy_amount`) VALUES
(1, 'Combo 1', '2018-10-25', NULL, 1, 50, 1000, 500),
(2, 'Combo 2', '2018-10-25', NULL, 1, 100, 2000, 1000);

-- --------------------------------------------------------

--
-- Table structure for table `exam_types`
--

CREATE TABLE `exam_types` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(255) NOT NULL,
  `updated_date` date DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_types`
--

INSERT INTO `exam_types` (`id`, `name`, `created_date`, `created_by`, `updated_date`, `updated_by`, `status`) VALUES
(1, 'Chapter Wise Exams', '2018-10-23', 'admin', NULL, NULL, 1),
(2, 'Subject Wise Exams', '2018-10-23', 'admin', NULL, NULL, 1),
(3, 'Full Mock Test', '2018-10-23', 'admin', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(10) NOT NULL,
  `fb_name` varchar(255) NOT NULL,
  `fb_email` varchar(50) NOT NULL,
  `fb_number` varchar(20) NOT NULL,
  `fb_comment` text NOT NULL,
  `fb_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `fb_name`, `fb_email`, `fb_number`, `fb_comment`, `fb_date`) VALUES
(1, 'Pankaj Chaudhary', 'monoj.majumder@gmail.com', '9874296556', '<p>&nbsp;</p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px; text-align: center;\"><strong>&nbsp;&nbsp;</strong><span style=\"text-decoration: underline;\"><strong> RULES FOR PARTICIPATION&nbsp; </strong><br /></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">There will be negative marking.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">+4 for correct answer</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">-1 for wrong answer</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">1.</span><span style=\"color: black; mso-themecolor: text1;\"> Only Individual Participation is allowed (No Teams).</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">2.</span><span style=\"color: black; mso-themecolor: text1;\"> The event will be held online as mentioned in the timeline.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp;&nbsp;&nbsp; </span>So, all the participants are requested to register on the Examshow website</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp;&nbsp;&nbsp; </span><a href=\"http://examshow.codeshare.in\">Register Here</a> for participation.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">3.</span><span style=\"color: black; mso-themecolor: text1;\"> The top two Participants will be awarded the goodies.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">4.</span><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp; </span>Only students who are studying in any collage or school eligible to take part.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">5.</span><span style=\"color: black; mso-themecolor: text1;\"> If any team(s) is found posting answers in any forum will be immediately disqualified without any prior notice.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">6.</span><span style=\"color: black; mso-themecolor: text1;\"> Any team found having fake information regarding qualification criteria or profile information shall be immediately disqualified.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">7.</span><span style=\"color: black; mso-themecolor: text1;\"><span style=\"mso-spacerun: yes;\">&nbsp; </span>Any team which attempts to hack/deliberately crash the judging system will be immediately disqualified.</span></strong></span></p>\r\n<p class=\"MsoNormal\" style=\"padding-left: 30px;\"><span style=\"mso-bookmark: _GoBack;\"><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: #c0504d; mso-themecolor: accent2;\">8.</span><span style=\"color: black; mso-themecolor: text1;\"> The decision of the organizers in declaring the results will be final and will be binding to all. No queries in this regard will be entertained</span></strong></span><strong style=\"mso-bidi-font-weight: normal;\"><span style=\"color: black; mso-themecolor: text1;\">.</span></strong></p>', '2018-10-23');

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `id` bigint(20) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_date` date DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT '1',
  `create_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `created_date`, `update_date`, `status`, `create_date`) VALUES
(1, 'NEET', '2018-10-22', NULL, 1, NULL),
(2, 'JEE', '2018-10-23', NULL, 1, NULL),
(3, 'NEET & JEE', '2018-10-23', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `module_class_mappings`
--

CREATE TABLE `module_class_mappings` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_class_mappings`
--

INSERT INTO `module_class_mappings` (`id`, `module_id`, `class_id`, `created_date`) VALUES
(1, 1, 1, '2018-10-23'),
(2, 1, 2, '2018-10-23'),
(3, 1, 3, '2018-10-23'),
(4, 2, 1, '2018-10-23'),
(5, 2, 2, '2018-10-23'),
(6, 2, 3, '2018-10-23'),
(7, 3, 1, '2018-10-23'),
(8, 3, 2, '2018-10-23'),
(9, 3, 3, '2018-10-23');

-- --------------------------------------------------------

--
-- Table structure for table `notice`
--

CREATE TABLE `notice` (
  `id` int(10) NOT NULL,
  `note` text,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `qno` int(10) NOT NULL,
  `question` text NOT NULL,
  `opt1` varchar(255) NOT NULL,
  `opt2` varchar(255) NOT NULL,
  `opt3` varchar(255) NOT NULL,
  `opt4` varchar(255) NOT NULL,
  `ans` varchar(255) NOT NULL,
  `created_date` date DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `teacher_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `module_id`, `class_id`, `subject_id`, `chapter_id`, `qno`, `question`, `opt1`, `opt2`, `opt3`, `opt4`, `ans`, `created_date`, `update_date`, `admin_id`, `status`, `teacher_id`) VALUES
(1, 1, 1, 1, 1, 1, '<p>Which port is used for ssh ?</p>', '22', '21', '80', '443', '22', '2018-10-23', NULL, 1, 1, NULL),
(2, 1, 1, 1, 2, 1, '<p>WWW stands for ?</p>', 'World Whole Web', 'Wide World Web', 'Web World Wide', 'World Wide Web', 'World Wide Web', '2018-10-23', NULL, 1, 1, NULL),
(3, 1, 1, 1, 1, 2, '<p>Which of the following are components of Central Processing Unit (CPU) ?</p>', 'Arithmetic logic unit, Mouse', 'Arithmetic logic unit, Control unit', 'Arithmetic logic unit, Integrated Circuits', 'Control Unit, Monitor', 'Arithmetic logic unit, Control unit', '2018-10-23', NULL, 1, 1, NULL),
(4, 1, 1, 1, 2, 2, '<pWhich among following first generation of computers had ?</p>', 'Vaccum Tubes and Magnetic Drum', 'Integrated Circuits', 'Magnetic Tape and Transistors', 'All of above', 'Vaccum Tubes and Magnetic Drum', '2018-10-23', NULL, 1, 1, NULL),
(44, 1, 1, 1, 2, 3, 'gdfgfg dfg??', '1', '2', '3', '4', '2', '2018-11-10', NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE `results` (
  `id` int(10) NOT NULL,
  `question_id` int(10) NOT NULL,
  `choose_ans` varchar(255) DEFAULT NULL,
  `score` int(10) NOT NULL,
  `candidate_id` int(10) NOT NULL,
  `exam_date` date NOT NULL,
  `exam_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_date` date DEFAULT NULL,
  `update_date` date DEFAULT NULL,
  `status` int(10) NOT NULL DEFAULT '1',
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `created_date`, `update_date`, `status`, `updated_date`) VALUES
(1, 'Physics', '2018-10-23', NULL, 1, NULL),
(2, 'Mathematics', '2018-10-23', NULL, 1, NULL),
(3, 'Chemistry', '2018-10-23', NULL, 1, NULL),
(4, 'Biology', '2018-10-23', NULL, 1, NULL),
(5, 'Botany', '2018-10-30', NULL, 1, NULL),
(6, 'Zoology', '2018-10-30', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subject_chapter_mappings`
--

CREATE TABLE `subject_chapter_mappings` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `chapter_id` int(11) NOT NULL,
  `created_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject_chapter_mappings`
--

INSERT INTO `subject_chapter_mappings` (`id`, `subject_id`, `chapter_id`, `created_date`) VALUES
(1, 1, 1, '2018-10-23'),
(2, 1, 2, '2018-10-23'),
(3, 1, 3, '2018-10-23'),
(4, 1, 4, '2018-10-23'),
(5, 1, 5, '2018-10-23'),
(6, 1, 6, '2018-10-23'),
(7, 1, 7, '2018-10-23'),
(8, 1, 8, '2018-10-23'),
(9, 1, 9, '2018-10-23'),
(10, 1, 10, '2018-10-23'),
(11, 1, 11, '2018-10-23'),
(12, 1, 12, '2018-10-23'),
(13, 1, 13, '2018-10-23'),
(14, 1, 14, '2018-10-23'),
(15, 1, 15, '2018-10-23'),
(16, 1, 16, '2018-10-23'),
(17, 1, 17, '2018-10-23'),
(18, 1, 18, '2018-10-23'),
(19, 1, 19, '2018-10-23'),
(20, 1, 20, '2018-10-23'),
(21, 1, 21, '2018-10-23'),
(22, 1, 22, '2018-10-23'),
(23, 1, 23, '2018-10-23'),
(24, 1, 24, '2018-10-23'),
(25, 1, 25, '2018-10-23'),
(26, 1, 26, '2018-10-23'),
(27, 1, 27, '2018-10-23'),
(28, 1, 28, '2018-10-23'),
(29, 1, 29, '2018-10-23'),
(30, 2, 29, '2018-10-23'),
(31, 3, 29, '2018-10-23'),
(32, 4, 29, '2018-10-23'),
(33, 5, 30, '2018-10-30'),
(34, 5, 31, '2018-10-30'),
(35, 6, 35, '2018-10-30'),
(36, 6, 36, '2018-10-30'),
(37, 3, 32, '2018-10-30'),
(38, 3, 33, '2018-10-30'),
(39, 3, 34, '2018-10-30');

-- --------------------------------------------------------

--
-- Table structure for table `tax_rate`
--

CREATE TABLE `tax_rate` (
  `id` int(11) NOT NULL,
  `rate` double NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `tax_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax_rate`
--

INSERT INTO `tax_rate` (`id`, `rate`, `start_date`, `end_date`, `tax_type`) VALUES
(1, 15, '2019-01-01', '2019-12-31', 'Service Tax'),
(2, 18, '2019-01-01', '2019-12-31', 'GST');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `teacher_name` varchar(255) NOT NULL,
  `teacher_email` varchar(50) DEFAULT NULL,
  `teacher_phonenumber` varchar(20) NOT NULL,
  `alternate_phone_no` varchar(20) DEFAULT NULL,
  `permanent_address` text,
  `permanent_city` varchar(50) DEFAULT NULL,
  `permanent_pinno` varchar(50) DEFAULT NULL,
  `permanent_state` varchar(50) DEFAULT NULL,
  `permanent_country` varchar(50) DEFAULT NULL,
  `correspondence_address` text,
  `correspondence_city` varchar(50) DEFAULT NULL,
  `correspondence_pinno` varchar(50) DEFAULT NULL,
  `correspondence_state` varchar(50) DEFAULT NULL,
  `correspondence_country` varchar(50) DEFAULT NULL,
  `status` tinyint(5) NOT NULL DEFAULT '1',
  `image_name` varchar(255) DEFAULT NULL,
  `teacher_username` varchar(255) NOT NULL,
  `teacher_password` varchar(255) NOT NULL,
  `created_by` varchar(20) DEFAULT NULL,
  `created_date` date NOT NULL,
  `updated_by` varchar(20) DEFAULT NULL,
  `updated_date` date DEFAULT NULL,
  `internal_teacher` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `teacher_name`, `teacher_email`, `teacher_phonenumber`, `alternate_phone_no`, `permanent_address`, `permanent_city`, `permanent_pinno`, `permanent_state`, `permanent_country`, `correspondence_address`, `correspondence_city`, `correspondence_pinno`, `correspondence_state`, `correspondence_country`, `status`, `image_name`, `teacher_username`, `teacher_password`, `created_by`, `created_date`, `updated_by`, `updated_date`, `internal_teacher`) VALUES
(1, 'Tuhin Majumder', 'dos.tuhin@gmail.com', '9800000001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 'dostuhin', 'vDeNN;', 'admin', '2018-10-29', NULL, NULL, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email_id` (`email_id`),
  ADD UNIQUE KEY `phone_no` (`phone_no`);

--
-- Indexes for table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone_no` (`candidate_phonenumber`),
  ADD UNIQUE KEY `email_id` (`candidate_email`);

--
-- Indexes for table `candidate_cheque_details`
--
ALTER TABLE `candidate_cheque_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `candidate_payment_details`
--
ALTER TABLE `candidate_payment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_subject_mappings`
--
ALTER TABLE `class_subject_mappings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_class_chapter_mappings_1` (`class_id`),
  ADD KEY `FK_class_chapter_mappings_2` (`subject_id`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_exams_1` (`module_id`),
  ADD KEY `FK_exams_2` (`class_id`),
  ADD KEY `FK_exams_3` (`exam_type_id`);

--
-- Indexes for table `exam_multiple_subject_mappings`
--
ALTER TABLE `exam_multiple_subject_mappings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_exam_multiple_subject_mappings_1` (`exam_id`),
  ADD KEY `FK_exam_multiple_subject_mappings_2` (`subject_id`);

--
-- Indexes for table `exam_packages`
--
ALTER TABLE `exam_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_types`
--
ALTER TABLE `exam_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module_class_mappings`
--
ALTER TABLE `module_class_mappings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_module_subject_mappings_1` (`module_id`),
  ADD KEY `FK_module_subject_mappings_2` (`class_id`);

--
-- Indexes for table `notice`
--
ALTER TABLE `notice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_question_1` (`module_id`),
  ADD KEY `FK_question_2` (`class_id`),
  ADD KEY `FK_question_3` (`subject_id`),
  ADD KEY `FK_question_4` (`chapter_id`),
  ADD KEY `FK5enfjaku1w7g6luokbw6lp9y4` (`admin_id`);

--
-- Indexes for table `results`
--
ALTER TABLE `results`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_results_1` (`question_id`),
  ADD KEY `FK_results_2` (`candidate_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subject_chapter_mappings`
--
ALTER TABLE `subject_chapter_mappings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_subject_class_mappings_1` (`subject_id`),
  ADD KEY `FK_subject_class_mappings_2` (`chapter_id`);

--
-- Indexes for table `tax_rate`
--
ALTER TABLE `tax_rate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`teacher_username`),
  ADD UNIQUE KEY `phone_no` (`teacher_phonenumber`),
  ADD UNIQUE KEY `email_id` (`teacher_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `candidate_cheque_details`
--
ALTER TABLE `candidate_cheque_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `candidate_payment_details`
--
ALTER TABLE `candidate_payment_details`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `class_subject_mappings`
--
ALTER TABLE `class_subject_mappings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `exam_multiple_subject_mappings`
--
ALTER TABLE `exam_multiple_subject_mappings`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `exam_packages`
--
ALTER TABLE `exam_packages`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exam_types`
--
ALTER TABLE `exam_types`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `module_class_mappings`
--
ALTER TABLE `module_class_mappings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `notice`
--
ALTER TABLE `notice`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `results`
--
ALTER TABLE `results`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `subject_chapter_mappings`
--
ALTER TABLE `subject_chapter_mappings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tax_rate`
--
ALTER TABLE `tax_rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `class_subject_mappings`
--
ALTER TABLE `class_subject_mappings`
  ADD CONSTRAINT `FK_class_chapter_mappings_1` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_class_chapter_mappings_2` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exams`
--
ALTER TABLE `exams`
  ADD CONSTRAINT `FK_exams_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_exams_2` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_exams_3` FOREIGN KEY (`exam_type_id`) REFERENCES `exam_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exam_multiple_subject_mappings`
--
ALTER TABLE `exam_multiple_subject_mappings`
  ADD CONSTRAINT `FK_exam_multiple_subject_mappings_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_exam_multiple_subject_mappings_2` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`);

--
-- Constraints for table `module_class_mappings`
--
ALTER TABLE `module_class_mappings`
  ADD CONSTRAINT `FK_module_subject_mappings_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_module_subject_mappings_2` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `FK5enfjaku1w7g6luokbw6lp9y4` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`),
  ADD CONSTRAINT `FK_question_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_question_2` FOREIGN KEY (`class_id`) REFERENCES `classes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_question_3` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_question_4` FOREIGN KEY (`chapter_id`) REFERENCES `chapters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `results`
--
ALTER TABLE `results`
  ADD CONSTRAINT `FK_results_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_results_2` FOREIGN KEY (`candidate_id`) REFERENCES `candidates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subject_chapter_mappings`
--
ALTER TABLE `subject_chapter_mappings`
  ADD CONSTRAINT `FK_subject_class_mappings_1` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_subject_class_mappings_2` FOREIGN KEY (`chapter_id`) REFERENCES `chapters` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
